import { AppConstants } from '../@core/common/constants';

export class UirConstants {
    public static EVENTS = {
        'COMPLAINT_REFFERAL_SUBMITTED': 'PRCM'
    };

    public static NEW_COMPLAINT_ACCESS = [
        AppConstants.ROLES.LICENSING_ADMINISTRATOR,
        AppConstants.ROLES.QUALITY_ASSURANCE
    ];

    public static monthDropDown = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    public static yearDropDown = [];
    public static periodicDropDown = ['Monthly', 'Quarterly', 'Yearly', 'Initial', 'Periodic', 'Mid-Year', 'Re-Licensure'];
    public static timeOfVisit = ['Night', 'Weekend', 'Regular'];
    public static visitSchedule = ['Announced', 'Unannounced'];

    // public static COMPLAINT_STATUS = {
    //     pending: 'pending',
    //     refferalsubmit: 'refferalsubmit', // PRCM
    //     refferel_rejected: 'refferel_rejected',
    //     refferalApproved: 'refferal_approved',
    //     complaint_rejected: 'complaint_rejected',
    //     complaint_submited: 'complaint_submited',
    //     complaint_approved: 'complaint_approved',
    //     closed: 'closed'

    // };

    public static UIR_TABS =
        [
            {
                id: 'program',
                path: 'program',
                name: 'Program',
                roles: [
                    AppConstants.ROLES.LICENSING_ADMINISTRATOR,
                    AppConstants.ROLES.QUALITY_ASSURANCE,
                    AppConstants.ROLES.PROGRAM_MANAGER,
                    AppConstants.ROLES.PROVIDER_STAFF_ADMIN
                ]
            },{
                id: 'person',
                path: 'person',
                name: 'Person',
                roles: [
                        AppConstants.ROLES.LICENSING_ADMINISTRATOR,
                        AppConstants.ROLES.QUALITY_ASSURANCE,
                        AppConstants.ROLES.PROGRAM_MANAGER,
                        AppConstants.ROLES.PROVIDER_STAFF_ADMIN
                    ]
            },
            // {
            //     id: 'incident',
            //     path: 'incident',
            //     name: 'Incident',
            //     roles: [
            //         AppConstants.ROLES.LICENSING_ADMINISTRATOR,
            //         AppConstants.ROLES.QUALITY_ASSURANCE,
            //         AppConstants.ROLES.PROGRAM_MANAGER,
            //         AppConstants.ROLES.PROVIDER_STAFF_ADMIN
            //     ]
            // },
            {
                id: 'notification',
                path: 'notification',
                name: 'Notification',
                roles: [
                    AppConstants.ROLES.LICENSING_ADMINISTRATOR,
                    AppConstants.ROLES.QUALITY_ASSURANCE,
                    AppConstants.ROLES.PROGRAM_MANAGER,
                    AppConstants.ROLES.PROVIDER_STAFF_ADMIN
                ]
            },
            {
                id: 'narrative',
                path: 'narrative',
                name: 'Narrative',
                roles: [
                    AppConstants.ROLES.LICENSING_ADMINISTRATOR,
                    AppConstants.ROLES.QUALITY_ASSURANCE,
                    AppConstants.ROLES.PROGRAM_MANAGER,
                    AppConstants.ROLES.PROVIDER_STAFF_ADMIN
                ]
            },
            {
                id: 'staff',
                path: 'staff',
                name: 'Staff Details',
                roles: [
                    AppConstants.ROLES.LICENSING_ADMINISTRATOR,
                    AppConstants.ROLES.QUALITY_ASSURANCE,
                    AppConstants.ROLES.PROGRAM_MANAGER,
                    AppConstants.ROLES.PROVIDER_STAFF_ADMIN
                ]
            },
            {
                id: 'youth-involved',
                path: 'youth-involved',
                name: 'Youth Involved',
                roles: [
                    AppConstants.ROLES.LICENSING_ADMINISTRATOR,
                    AppConstants.ROLES.QUALITY_ASSURANCE,
                    AppConstants.ROLES.PROGRAM_MANAGER,
                    AppConstants.ROLES.PROVIDER_STAFF_ADMIN
                ]
            },
            {
                id: 'gang',
                path: 'gang',
                name: 'Gang Details',
                roles: [
                    AppConstants.ROLES.LICENSING_ADMINISTRATOR,
                    AppConstants.ROLES.QUALITY_ASSURANCE,
                    AppConstants.ROLES.PROGRAM_MANAGER,
                    AppConstants.ROLES.PROVIDER_STAFF_ADMIN
                ]
            },
            {
                id: 'supervisory',
                path: 'supervisory',
                name: 'Supervisory',
                roles: [
                    AppConstants.ROLES.LICENSING_ADMINISTRATOR,
                    AppConstants.ROLES.QUALITY_ASSURANCE,
                    AppConstants.ROLES.PROGRAM_MANAGER,
                    AppConstants.ROLES.PROVIDER_STAFF_ADMIN
                ]
            },
            {
                id: 'Witness',
                path: 'witness',
                name: 'Witness',
                roles: [
                    AppConstants.ROLES.LICENSING_ADMINISTRATOR,
                    AppConstants.ROLES.QUALITY_ASSURANCE,
                    AppConstants.ROLES.PROGRAM_MANAGER,
                    AppConstants.ROLES.PROVIDER_STAFF_ADMIN
                ]
            },
            {
                id: 'documents',
                path: 'documents',
                name: 'Documents',
                roles: [
                    AppConstants.ROLES.LICENSING_ADMINISTRATOR,
                    AppConstants.ROLES.QUALITY_ASSURANCE,
                    AppConstants.ROLES.PROGRAM_MANAGER,
                    AppConstants.ROLES.PROVIDER_STAFF_ADMIN
                ]
            },
            {
                id: 'decision',
                path: 'decision',
                name: 'Decision',
                roles: [
                    AppConstants.ROLES.LICENSING_ADMINISTRATOR,
                    AppConstants.ROLES.QUALITY_ASSURANCE,
                    AppConstants.ROLES.PROGRAM_MANAGER,
                    AppConstants.ROLES.PROVIDER_STAFF_ADMIN
                ]
            }
        ];


    // public static COMPLAINT_DETIAL_TABS =
    //     [
    //         {
    //             id: 'refferal',
    //             path: 'refferal',
    //             name: 'Complaint Referral',
    //             roles: [
    //                 AppConstants.ROLES.LICENSING_ADMINISTRATOR,
    //                 AppConstants.ROLES.QUALITY_ASSURANCE,
    //                 AppConstants.ROLES.PROGRAM_MANAGER,
    //             ],
    //             statuses: [
    //                 ProviderConstants.COMPLAINT_STATUS.pending,
    //                 ProviderConstants.COMPLAINT_STATUS.refferalApproved,
    //                 ProviderConstants.COMPLAINT_STATUS.refferel_rejected,
    //                 ProviderConstants.COMPLAINT_STATUS.refferalsubmit,
    //                 ProviderConstants.COMPLAINT_STATUS.complaint_submited,
    //                 ProviderConstants.COMPLAINT_STATUS.closed
    //             ]
    //         },
    //         {
    //             id: 'refferal',
    //             path: 'contacts',
    //             name: 'Contacts',
    //             roles: [
    //                 AppConstants.ROLES.LICENSING_ADMINISTRATOR,
    //                 AppConstants.ROLES.QUALITY_ASSURANCE,
    //                 AppConstants.ROLES.PROGRAM_MANAGER,
    //             ],
    //             statuses: [
    //                 ProviderConstants.COMPLAINT_STATUS.refferalApproved,
    //                 ProviderConstants.COMPLAINT_STATUS.complaint_rejected,
    //                 ProviderConstants.COMPLAINT_STATUS.complaint_submited,
    //                 ProviderConstants.COMPLAINT_STATUS.closed
    //             ]
    //         },
    //         {
    //             id: 'monitor',
    //             path: 'monitor-activity',
    //             name: 'Monitor Activity',
    //             roles: [
    //                 AppConstants.ROLES.LICENSING_ADMINISTRATOR,
    //                 AppConstants.ROLES.QUALITY_ASSURANCE,
    //                 AppConstants.ROLES.PROGRAM_MANAGER,
    //             ],
    //             statuses: [

    //                 ProviderConstants.COMPLAINT_STATUS.refferalApproved,
    //                 ProviderConstants.COMPLAINT_STATUS.complaint_rejected,
    //                 ProviderConstants.COMPLAINT_STATUS.complaint_submited,
    //                 ProviderConstants.COMPLAINT_STATUS.closed
    //             ]
    //         },
    //         {
    //             id: 'deficiency-violation',
    //             path: 'deficiency-violation',
    //             name: 'Deficiency/Violation ',
    //             roles: [
    //                 AppConstants.ROLES.LICENSING_ADMINISTRATOR,
    //                 AppConstants.ROLES.QUALITY_ASSURANCE,
    //                 AppConstants.ROLES.PROGRAM_MANAGER,
    //             ],
    //             statuses: [

    //                 ProviderConstants.COMPLAINT_STATUS.refferalApproved,
    //                 ProviderConstants.COMPLAINT_STATUS.complaint_rejected,
    //                 ProviderConstants.COMPLAINT_STATUS.complaint_submited,
    //                 ProviderConstants.COMPLAINT_STATUS.closed
    //             ]
    //         },
    //         {
    //             id: 'summary',
    //             path: 'summary',
    //             name: 'Summary',
    //             roles: [
    //                 AppConstants.ROLES.LICENSING_ADMINISTRATOR,
    //                 AppConstants.ROLES.QUALITY_ASSURANCE,
    //                 AppConstants.ROLES.PROGRAM_MANAGER,
    //             ],
    //             statuses: [

    //                 ProviderConstants.COMPLAINT_STATUS.refferalApproved,
    //                 ProviderConstants.COMPLAINT_STATUS.complaint_rejected,
    //                 ProviderConstants.COMPLAINT_STATUS.complaint_submited,
    //                 ProviderConstants.COMPLAINT_STATUS.closed
    //             ]
    //         },
    //         {
    //             id: 'outcomes',
    //             path: 'outcomes',
    //             name: 'outcomes',
    //             roles: [
    //                 AppConstants.ROLES.LICENSING_ADMINISTRATOR,
    //                 AppConstants.ROLES.QUALITY_ASSURANCE,
    //                 AppConstants.ROLES.PROGRAM_MANAGER,
    //             ],
    //             statuses: [

    //                 ProviderConstants.COMPLAINT_STATUS.refferalApproved,
    //                 ProviderConstants.COMPLAINT_STATUS.complaint_rejected,
    //                 ProviderConstants.COMPLAINT_STATUS.complaint_submited,
    //                 ProviderConstants.COMPLAINT_STATUS.closed
    //             ]
    //         },
    //         {
    //             id: 'documents',
    //             path: 'documents',
    //             name: 'documents',
    //             roles: [
    //                 AppConstants.ROLES.LICENSING_ADMINISTRATOR,
    //                 AppConstants.ROLES.QUALITY_ASSURANCE,
    //                 AppConstants.ROLES.PROGRAM_MANAGER,
    //             ],
    //             statuses: [

    //                 ProviderConstants.COMPLAINT_STATUS.refferalApproved,
    //                 ProviderConstants.COMPLAINT_STATUS.complaint_rejected,
    //                 ProviderConstants.COMPLAINT_STATUS.complaint_submited,
    //                 ProviderConstants.COMPLAINT_STATUS.closed
    //             ]
    //         },
    //         {
    //             id: 'decision',
    //             path: 'decision',
    //             name: 'decision',
    //             roles: [
    //                 AppConstants.ROLES.LICENSING_ADMINISTRATOR,
    //                 AppConstants.ROLES.QUALITY_ASSURANCE,
    //                 AppConstants.ROLES.PROGRAM_MANAGER,
    //             ],
    //             statuses: [

    //                 ProviderConstants.COMPLAINT_STATUS.refferalApproved,
    //                 ProviderConstants.COMPLAINT_STATUS.complaint_rejected,
    //                 ProviderConstants.COMPLAINT_STATUS.refferalsubmit,
    //                 ProviderConstants.COMPLAINT_STATUS.complaint_submited,
    //                 ProviderConstants.COMPLAINT_STATUS.closed
    //             ]
    //         }


    //     ];

    //  1. Approve and assigns to worker
    //  2. Denies and closes
    //  3. Return to worker
    //  4. Refers to another licensing agency
    // public static COMPLAINT_DECISONS = [
    //     {
    //         displayName: 'Approve and Assign',
    //         eventcode: ProviderConstants.COMPLAINT_STATUS.refferalApproved,
    //         roles: [
    //             AppConstants.ROLES.PROGRAM_MANAGER,
    //         ],
    //         statuses: [
    //             ProviderConstants.COMPLAINT_STATUS.refferalsubmit,
    //         ]
    //     },
    //     {
    //         displayName: 'Deny and close',
    //         eventcode: ProviderConstants.COMPLAINT_STATUS.closed,
    //         roles: [
    //             AppConstants.ROLES.PROGRAM_MANAGER,
    //         ],
    //         statuses: [
    //             ProviderConstants.COMPLAINT_STATUS.refferalsubmit,
    //         ]
    //     },
    //     {
    //         displayName: 'Return to worker',
    //         eventcode: ProviderConstants.COMPLAINT_STATUS.refferel_rejected,
    //         roles: [
    //             AppConstants.ROLES.PROGRAM_MANAGER,
    //         ],
    //         statuses: [
    //             ProviderConstants.COMPLAINT_STATUS.refferalsubmit,
    //         ]
    //     },
    //     {
    //         displayName: 'Refers to another licensing agency',
    //         eventcode: ProviderConstants.COMPLAINT_STATUS.closed,
    //         roles: [
    //             AppConstants.ROLES.PROGRAM_MANAGER,
    //         ],
    //         statuses: [
    //             ProviderConstants.COMPLAINT_STATUS.refferalsubmit,
    //         ]
    //     },
    //     {
    //         displayName: 'Submit the complaint review ',
    //         eventcode: ProviderConstants.COMPLAINT_STATUS.complaint_submited,
    //         roles: [
    //             AppConstants.ROLES.QUALITY_ASSURANCE,
    //             AppConstants.ROLES.LICENSING_ADMINISTRATOR
    //         ],
    //         statuses: [
    //             ProviderConstants.COMPLAINT_STATUS.refferalsubmit,
    //         ]
    //     }
    // ];

}
