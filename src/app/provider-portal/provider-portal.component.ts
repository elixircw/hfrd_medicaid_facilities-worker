import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProviderPortalService } from './provider-portal.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'provider-portal',
  templateUrl: './provider-portal.component.html',
  styleUrls: ['./provider-portal.component.scss']
})
export class ProviderPortalComponent implements OnInit {

  constructor(private route: ActivatedRoute,
    private _service: ProviderPortalService) {
    this.route.data.subscribe(data => {
      console.log('configurations', data.configurations);
      this._service.setApplications(data.configurations);
    });
  }

  ngOnInit() {
  }

}
