import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { DynamicObject, PaginationInfo } from '../../../../@core/entities/common.entities';
import { ProviderIncidentReportService } from '../../provider-incident-report.service';
import { IncidentUirService } from '../incident-uir.service';
import { CommonDropdownsService, AlertService, DataStoreService, CommonHttpService, AuthService } from '../../../../@core/services';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { MatRadioButton, MatRadioChange } from '@angular/material';
import { UirDetail } from '../_entities/uir-model';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
import { AppUser } from '../../../../@core/entities/authDataModel';


@Component({
  selector: 'uir-incident',
  templateUrl: './uir-incident.component.html',
  styleUrls: ['./uir-incident.component.scss']
})
export class UirIncidentComponent implements OnInit {

  incidentForm: FormGroup;
  locationSource$: any;
  locationSource1$: any;
  locationSource2$: any;
  class1IncidentSource$: any;
  class2IncidentSource$: any;
  class3IncidentSource$: any;
  class1Selected: any = [];
  class2Selected: any = [];
  class3Selected: any = [];
  class3Other = false;
  classMerge: any = [];
  precipitate = '';
  providerUirId: string;
  uirNo: string;
  providerUirDetails: UirDetail;
  providerClassIncident: any = [];
  paginationInfo: PaginationInfo = new PaginationInfo();
  user: AppUser;
  role: string;
  statusId: any;
  isDisabled: boolean;

  constructor(private formBuilder: FormBuilder,
    private _service: ProviderIncidentReportService,
    private _incidentService: IncidentUirService,
    private commonDropdownService: CommonDropdownsService,
    private _dataStore: DataStoreService,
    private _commonHttpService: CommonHttpService,
    private _providerIncident: ProviderIncidentReportService,
    private _alertService: AlertService,
    private _router: Router,
    private _authService: AuthService) {
      const id = this._incidentService.provideruirid;
      this.providerUirId = (id !== 'create') ? id : '';
      this.uirNo = this._incidentService.uirid;
      this.user = this._authService.getCurrentUser();
      this.role = this.user.role.name;
      this.statusId = this._dataStore.getData('uir_status_type_id');
      if ((this.role === 'Provider_Staff_Admin' && this.statusId === 49) ||
          (this.role !== 'Provider_Staff_Admin' && this.statusId !== 49)) {
        this.isDisabled = true;
      } else  {
        this.isDisabled = false;
      }
  }

  ngOnInit() {
    if (this.providerUirId) {
      this.getIncidentUirDetails(this.providerUirId);
    }
    this.addIncidentForm();
    this.loadDropDowns();
  }

  loadFormValues() {
    const class1_typekey = this.providerClassIncident.filter((list) => list.class1_typekey != null).map((list) => list.class1_typekey);
    const class2_typekey = this.providerClassIncident.filter((list) => list.class2_typekey != null).map((list) => list.class2_typekey);
    const class3_typekey = this.providerClassIncident.filter((list) => list.class3_typekey != null).map((list) => list.class3_typekey);
    console.log(class3_typekey);
    const isOther = class3_typekey.findIndex(x => x === 'Other');
    console.log(isOther);
    this.class3Other = (isOther !== -1) ? true : false;
    console.log(this.class3Other);
    const other_class3 = this.providerClassIncident.filter(x => x.class3_typekey === 'Other').map(x => x.other_class3);
    const other_class3_value = (other_class3) ? other_class3[0] : '';

    this.incidentForm.patchValue({
      level_supervision: this.providerUirDetails.level_supervision,
      location_incident: this.providerUirDetails.location_incident,
      location_area: this.providerUirDetails.location_area,
      location_area_other: this.providerUirDetails.location_area_other,
      incident_date: this.providerUirDetails.incident_datetime,
      incident_time: moment(this.providerUirDetails.incident_datetime, 'HH:mm:ss').format('HH:mm:ss'),
      discovered_date: this.providerUirDetails.discovered_datetime,
      discovered_time: moment(this.providerUirDetails.discovered_datetime, 'HH:mm:ss').format('HH:mm:ss'),
      class1_typekey: class1_typekey,
      class2_typekey: class2_typekey,
      class3_typekey: class3_typekey,
      other_class3: other_class3_value
    });
    if (this.isDisabled) {
      this.incidentForm.disable();
    } else {
      this.incidentForm.enable();
    }
  }

  addIncidentForm() {
    this.incidentForm = this.formBuilder.group({
      level_supervision: [null],
      location_incident: [null],
      location_area: [null],
      location_area_other: [null],
      incident_date: [null],
      incident_time: [null],
      discovered_date: [null, Validators.required],
      discovered_time: [null, Validators.required],
      other_class3: [null],
      class1_typekey: [null],
      class2_typekey: [null],
      class3_typekey: [null],
    });
  }

  onChangeLocationType($event: MatRadioChange) {
    this.locationSource$ = ($event.value === '1') ? this.locationSource1$ : this.locationSource2$;
  }

  loadDropDowns() {
    Observable.forkJoin([
      this.commonDropdownService.getDropownsByTable('onsite_location_area', 'OLM'),
      this.commonDropdownService.getDropownsByTable('offsite_location_area', 'OLM'),
      this.commonDropdownService.getDropownsByTable('Class_I_Incidents', 'OLM'),
      this.commonDropdownService.getDropownsByTable('Class_II_Incidents', 'OLM'),
      this.commonDropdownService.getDropownsByTable('Class_III_Incidents', 'OLM'),
    ]).subscribe(
      ([locationSource1, locationSource2, class1IncidentSource, class2IncidentSource, class3IncidentSource]) => {
        this.locationSource1$ = locationSource1;
        this.locationSource2$ = locationSource2;
        this.locationSource$ = (this.providerUirDetails) ? (this.providerUirDetails.location_incident === '1') ? this.locationSource1$ : this.locationSource2$ : this.locationSource1$;
        this.class1IncidentSource$ = class1IncidentSource;
        this.class2IncidentSource$ = class2IncidentSource;
        this.class3IncidentSource$ = class3IncidentSource;

        const class1 = this.providerClassIncident.filter((list) => list.class1_typekey != null).map((list) => list.class1_typekey);
        const class2 = this.providerClassIncident.filter((list) => list.class2_typekey != null).map((list) => list.class2_typekey);
        const class3 = this.providerClassIncident.filter((list) => list.class3_typekey != null).map((list) => list.class3_typekey);

        this.class1Selected = (class1) ? this.class1IncidentSource$.filter(item => (class1.indexOf(item.ref_key) !== -1)) : [];
        this.class2Selected = (class1) ? this.class2IncidentSource$.filter(item => (class2.indexOf(item.ref_key) !== -1)) : [];
        this.class3Selected = (class1) ? this.class3IncidentSource$.filter(item => (class3.indexOf(item.ref_key) !== -1)) : [];
        this.classMerge = [...this.class1Selected, ...this.class2Selected, ...this.class3Selected];

        const isOther = this.classMerge.findIndex(x => x.class3_typekey === 'Other');
        this.class3Other = (isOther !== -1) ? true : false;
      });

  }

  onChange(event, classType, selected) {

    const class1 = this.incidentForm.getRawValue().class1_typekey;
    const class2 = this.incidentForm.getRawValue().class2_typekey;
    const class3 = this.incidentForm.getRawValue().class3_typekey;
    console.log(this.incidentForm.getRawValue());

    switch (classType) {
      case 'class1':
        if (event.option.selected) {
          this.class1Selected = (class1) ? this.class1IncidentSource$.filter(item => (class1.indexOf(item.ref_key) !== -1)) : [];
        } else {
          const itemIndex = this.class1Selected.findIndex(x => x.class1_typekey === event.option.value);
          this.class1Selected.splice(itemIndex, 1);
        }
        break;
      case 'class2':
        if (event.option.selected) {
          this.class2Selected = (class2) ? this.class2IncidentSource$.filter(item => (class2.indexOf(item.ref_key) !== -1)) : [];
        } else {
          const itemIndex = this.class2Selected.findIndex(x => x.class2_typekey === event.option.value);
          this.class2Selected.splice(itemIndex, 1);
        }
        break;
      case 'class3':
        if (event.option.selected) {
          this.class3Selected = (class3) ? this.class3IncidentSource$.filter(item => (class3.indexOf(item.ref_key) !== -1)) : [];
        } else {
          const itemIndex = this.class3Selected.findIndex(x => x.class3_typekey === event.option.value);
          this.class3Selected.splice(itemIndex, 1);
        }
    }
    this.classMerge = [...this.class1Selected, ...this.class2Selected, ...this.class3Selected];
    const isOther = this.classMerge.findIndex(x => x.ref_key === 'Other');
    this.class3Other = (isOther !== -1) ? true : false;
  }

  onChangePrecipitate(item) {
    this.precipitate = item.ref_key;
  }

  selectPrecipitaeEvent() {
    (<any>$('#precipitate-event')).modal('show');
  }

  closeDeleteModal() {
    (<any>$('#precipitate-event')).modal('hide');
  }

  submitIncident() {
    if (this.incidentForm.valid) {
      const uirDetails = this.incidentForm.getRawValue();

      const class1 = [];
      const class2 = [];
      const class3 = [];

      if (this.incidentForm.getRawValue().class1_typekey) {
        this.incidentForm.getRawValue().class1_typekey.forEach(function (item, index) {
          class1.push({ class1_typekey: item });
        });
      }
      if (this.incidentForm.getRawValue().class2_typekey) {
        this.incidentForm.getRawValue().class2_typekey.forEach(function (item, index) {
          class2.push({ class2_typekey: item });
        });
      }
      if (this.incidentForm.getRawValue().class3_typekey) {
        this.incidentForm.getRawValue().class3_typekey.forEach(function (item, index) {
           class3.push({ class3_typekey: item });
        });
      }

      const date1 = moment(this.incidentForm.getRawValue().discovered_date).format('MM/DD/YYYY') + ' ' + this.incidentForm.getRawValue().discovered_time;
      const date2 = moment(this.incidentForm.getRawValue().incident_date).format('MM/DD/YYYY') + ' ' + this.incidentForm.getRawValue().incident_time;
      uirDetails.discovered_datetime = moment(date1).format();
      uirDetails.incident_datetime = moment(date2).format();
      uirDetails.uir_no = this._incidentService.uirid;
      uirDetails.class1_typekey = class1;
      uirDetails.class2_typekey = class2;
      uirDetails.class3_typekey = class3;
      if (this.providerUirId) {
        uirDetails.provider_uir_id = this.providerUirId;
      }
      uirDetails.precipitate_event = this.precipitate;

      this._service.saveIncidentUirDetails(uirDetails).subscribe(response => {
        (<any>$('#precipitate-event')).modal('hide');
        if (response) {
          if (!this.providerUirId && response[0].provider_uir_id) {
            this.providerUirId = response[0].provider_uir_id;
            this._providerIncident.navigateTo(this.uirNo, this.providerUirId, 'incident');
          }
          this.getIncidentUirDetails(this.providerUirId);
          this._alertService.success('Information saved successfully!');
        }
      });
    }
  }

  getIncidentUirDetails(providerUirId) {
    const providerUirWhere = (providerUirId) ? { provider_uir_id: providerUirId } : {};
    const providerParam = {
          where: providerUirWhere,
          method: 'get',
          count: -1 };
    this._providerIncident.getProviderUirDetails(providerParam).subscribe(response => {
      if (response) {
        this.providerUirDetails = response[0];
        this.getClassIncidentDetails(this.providerUirId);
      }
    });
  }

  getClassIncidentDetails(providerUirId) {
    const classParam = {
              where: { provider_uir_id: providerUirId },
              method: 'get',
              count: -1 };
    this._providerIncident.getClassIncidentDetails(classParam).subscribe((response) => {
      if (response) {
        this.providerClassIncident = response;
        setTimeout(() => { this.loadFormValues(); }, 800);
      }
    });
  }
}
