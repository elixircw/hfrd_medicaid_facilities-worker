import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UirIncidentRoutingModule } from './uir-incident-routing.module';
import { UirIncidentComponent } from './uir-incident.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { ProviderIncidentReportService } from '../../provider-incident-report.service';
import { Routes, RouterModule } from '@angular/router';


@NgModule({
  imports: [
    CommonModule,
    UirIncidentRoutingModule,
    FormMaterialModule,
    RouterModule
  ],
  declarations: [UirIncidentComponent],
  providers:[ProviderIncidentReportService]
})
export class UirIncidentModule { }
