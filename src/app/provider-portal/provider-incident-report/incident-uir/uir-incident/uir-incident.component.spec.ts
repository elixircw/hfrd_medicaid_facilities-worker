import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UirIncidentComponent } from './uir-incident.component';

describe('UirIncidentComponent', () => {
  let component: UirIncidentComponent;
  let fixture: ComponentFixture<UirIncidentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UirIncidentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UirIncidentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
