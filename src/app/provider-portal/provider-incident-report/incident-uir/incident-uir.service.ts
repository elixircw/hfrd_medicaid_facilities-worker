import { Injectable } from '@angular/core';
import { ProviderPortalUrlConfig  } from '../../provider-portal-url.config';
import { CommonHttpService, AuthService, GenericService } from '../../../@core/services';
import { Router } from '@angular/router';
import { UirDetail, PersonSearchResponse } from './_entities/uir-model';
import { ProviderPortalService } from '../../provider-portal.service';
import { PaginationInfo } from '../../../@core/entities/common.entities';
import { ObjectUtils } from '../../../@core/common/initializer';
import * as moment from 'moment';


@Injectable()
export class IncidentUirService {
  uirid: string;
  provideruirid: string;
  uirDetail: UirDetail;
  providerUirId: string;

  constructor(private _router: Router, private _commonHttpService: CommonHttpService,
    private authService: AuthService,
    private _providerPortalService: ProviderPortalService,
    private _involvedPersonSearchService: GenericService<PersonSearchResponse>) { }

  openIncidentReport() {
    this._commonHttpService.getArrayList({}, ProviderPortalUrlConfig.EndPoint.Uir.GetNextNumberUrl).subscribe(result => {
      const url = '/provider-portal/incident-report/uir/' + result['nextNumber'] + '/create/program';
      this._router.navigate([url]);
    });
  }

  getIncidentUirDetails(uir_id: string) {
    return this._commonHttpService.getArrayList({
      method: 'get',
      where: { uir_id: uir_id }
    }, ProviderPortalUrlConfig.EndPoint.Uir.DETAIL_URL);
  }

  getMetaData(uirid: string): any {
    const uirDetatail = {
      uir_number: uirid,
      inserted_on: new Date(),
      created_by: this.authService.getCurrentUser().user.securityusersid,
      displayname: this.authService.getCurrentUser().user.userprofile.fullname
    };
    return uirDetatail;
  }

  getProgramType() {
   return this._commonHttpService.getArrayList(
    {
      method: 'post',
      nolimit: true,
      providertype: 'public'
    },
    ProviderPortalUrlConfig.EndPoint.Uir.LIST_PROGRAMMES_URL );
  }

  getProgramNames() {
    return this._commonHttpService.getArrayList(
      {
          method: 'post',
          nolimit: true,
          filter: {},
          where:
          {
           provider_id: this._providerPortalService.getProviderId()
          }
      },
      ProviderPortalUrlConfig.EndPoint.Uir.PROVIDER_PROGRAM_NAMES_URL );
  }

  getProgramDetails() {
    return this._commonHttpService.getArrayList(
      {
          method: 'post',
          nolimit: true,
          filter: {
            where:
            {
             provider_id: this._providerPortalService.getProviderId(),
             uir_no: this.uirid
            }
          }
      }, ProviderPortalUrlConfig.EndPoint.Uir.GET_PROGRAM_DETAILS );
  }

  getSelectedProgramInformation(selectedSiteId: string) {
    return this._commonHttpService.create(
      {
        method: 'post',
        where:
        {
          objectid: selectedSiteId
        }
      },
      ProviderPortalUrlConfig.EndPoint.Uir.GET_SELECTED_PROGRAM_INFO );
  }
  getSelectedSiteInformation(selectedSiteId: string) {
    return this._commonHttpService.create(
      {
        method: 'post',
        where:
        {
          provider_id: this._providerPortalService.getProviderId(),
          site_id: selectedSiteId
        }
      },
      ProviderPortalUrlConfig.EndPoint.Uir.GET_SELECTED_SITE_INFO);
  }
  searchPerson(searchCriteria: any, pageNumber: number, paginationInfo: PaginationInfo) {
    console.log(searchCriteria);
    if (searchCriteria.dob) {
      searchCriteria.dob = moment(searchCriteria.dob).format('MM/DD/YYYY');
    }
    ObjectUtils.removeEmptyProperties(searchCriteria);
    return this._involvedPersonSearchService
      .getPagedArrayList(
        {
          limit: paginationInfo.pageSize,
          order: paginationInfo.sortBy,
          page: pageNumber,
          count: paginationInfo.total,
          where: searchCriteria,
          method: 'post'
        },
        'globalpersonsearches/getPersonSearchData'
      );
  }
}
