import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UirStaffRoutingModule } from './uir-staff-routing.module';
import { UirStaffComponent } from './uir-staff.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';

@NgModule({
  imports: [
    CommonModule,
    UirStaffRoutingModule,
    FormMaterialModule
  ],
  declarations: [UirStaffComponent]
})
export class UirStaffModule { }
