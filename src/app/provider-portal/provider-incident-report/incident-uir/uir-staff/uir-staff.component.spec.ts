import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UirStaffComponent } from './uir-staff.component';

describe('UirStaffComponent', () => {
  let component: UirStaffComponent;
  let fixture: ComponentFixture<UirStaffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UirStaffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UirStaffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
