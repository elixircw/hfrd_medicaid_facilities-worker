import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CommonHttpService, AlertService, DataStoreService, AuthService } from '../../../../@core/services';
import { Router, ActivatedRoute } from '@angular/router';
import { IncidentUirService } from '../incident-uir.service';
import { ProviderIncidentReportService } from '../../provider-incident-report.service';
import { AppUser } from '../../../../@core/entities/authDataModel';

@Component({
  selector: 'uir-staff',
  templateUrl: './uir-staff.component.html',
  styleUrls: ['./uir-staff.component.scss']
})
export class UirStaffComponent implements OnInit {
  additionalStaffInfo= [];
  uirNo: string;
  availableStaffs= [];
  providerUirId: string;
  isPrimaryStaffSelected: boolean;
  isEditStaff: boolean;
  user: AppUser;
  role: string;
  statusId: any;
  isDisabled: boolean;

  staffForm: FormGroup;
  constructor(private formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _service: IncidentUirService,
    private _incidentService: IncidentUirService,
    private _router: Router, private route: ActivatedRoute,
    private _dataStore: DataStoreService,
    private _providerIncident: ProviderIncidentReportService,
    private _authService: AuthService) {
      this.uirNo = this._incidentService.uirid;
      const id = this._incidentService.provideruirid;

      this.providerUirId = (id !== 'create') ? id : '';
      this.user = this._authService.getCurrentUser();
      this.role = this.user.role.name;
      this.statusId = this._dataStore.getData('uir_status_type_id');
      if ((this.role === 'Provider_Staff_Admin' && this.statusId === 49) ||
          (this.role !== 'Provider_Staff_Admin' && this.statusId !== 49)) {
        this.isDisabled = true;
      } else  {
        this.isDisabled = false;
      }
    }

    ngOnInit() {
    this.addStaffForm();
    this.getassignedstaff();
    this.getAdditionalStaffInfo();
    this.isPrimaryStaffSelected = false;
    this.isEditStaff = false;
  }
  addStaffForm() {
    this.staffForm = this.formBuilder.group({
      is_staff_assaulted: [null],
      is_injury_sustained: [null],
      injury_severity_rating: [null],
      is_primary_staff_involved: [null],
      provider_staff_id: [''],
      // provider_primary_staff:[''],
      provider_uir_actor_involved_id: [null],
      provider_uir_id: this.providerUirId

    });
    if (this.isDisabled) {
      this.staffForm.disable();
    } else {
      this.staffForm.enable();
    }
  }
  addStaffInfo() {
    this.isEditStaff = false;
  }
  getassignedstaff() {
    this._commonHttpService.create(
      {
        // where: { provider_applicant_id: this.applicantId },
        where: { provider_applicant_id: this.uirNo },
        method: 'post'
        // applicant_id:this.applicantNumber
      },
      'providerstaff/getassignedstaff'
    ).subscribe(response => {
      this.availableStaffs = response.map(item => {
        return {
          key: item.first_nm + ' ' + item.last_nm,
          value: item.provider_staff_id
        };
      });
      console.log(' this.availableStaffs', this.availableStaffs);
    },
      (error) => {
        this._alertService.error('Unable to get assigned staffs, please try again.');
        console.log('get contact Error', error);
        return false;
      }
    );
  }
  saveAdditionalStaffInfo() {
    // if(this.isPrimaryStaffSelected){
    //   this.staffForm.value.is_primary_staff_involved= true;
    // }
    console.log('Saved response', this.staffForm.value);
    this._providerIncident.saveYouthInvolvedDetails(this.staffForm.value).subscribe(response => {
      if (response) {
       this.getAdditionalStaffInfo();
        console.log('Saved response', response);
      }
      (<any>$('#staff-details')).modal('hide');
      this._alertService.success('Information saved successfully!');
    });
  }

  getAdditionalStaffInfo() {
    const providerIdKeyVal = (this.providerUirId) ? { provider_uir_id: this.providerUirId } : {};
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        where: providerIdKeyVal,
      },
      'provider_uir_actor_involved/list?filter'
    ).subscribe(response => {
      this.additionalStaffInfo = response;
      console.log(' this.availableStaffs', this.additionalStaffInfo);
    },
      (error) => {
        this._alertService.error('Unable to get assigned staffs, please try again.');
        console.log('get contact Error', error);
        return false;
      }
    );
  }
  // savePrimaryStaff(){
  //   console.log('option selected')
  //   this.isPrimaryStaffSelected=true;
  //   this.saveAdditionalStaffInfo();
  // }

  editStaffForm(editStaff) {
    (<any>$('#staff-details')).modal('show');
    this.isEditStaff = true;
    this.staffForm.patchValue(editStaff);
  }
}
