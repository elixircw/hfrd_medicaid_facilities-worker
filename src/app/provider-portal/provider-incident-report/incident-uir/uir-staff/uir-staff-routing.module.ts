import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UirStaffComponent } from './uir-staff.component';

const routes: Routes = [
  {
    path:'',
    component:UirStaffComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UirStaffRoutingModule { }
