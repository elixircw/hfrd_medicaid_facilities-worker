import { Component, OnInit } from '@angular/core';
//import { ComplaintDetailsService } from '../complaint-details.service';
import { AttachmentService } from '../../../provider-portal-utils/attachment/attachment.service';
import { AttachmentConfig } from '../../../provider-portal-utils/attachment/_entities/attachment.data.models';
import { Router, ActivatedRoute } from '@angular/router';
import { IncidentUirService } from '../incident-uir.service';
import { DataStoreService, AuthService } from '../../../../@core/services';
import { AppUser } from '../../../../@core/entities/authDataModel';



@Component({
  selector: 'uir-documents',
  templateUrl: './uir-documents.component.html',
  styleUrls: ['./uir-documents.component.scss']
})
export class UirDocumentsComponent implements OnInit {
  uir_number: string;
  user: AppUser;
  role: string;
  statusId: any;
  isDisabled: boolean;
  constructor(private attachService: AttachmentService,
    private _router: Router, private route: ActivatedRoute,
    private _service: IncidentUirService,
    private _dataStore: DataStoreService,
    private _authService: AuthService
    ) {
      this.uir_number = _service.uirid; //route.snapshot.params['id'];
      const attachmentConfig: AttachmentConfig = new AttachmentConfig();      
      attachmentConfig.uniqueNumber = this.uir_number;
      this.attachService.setAttachmentConfig(attachmentConfig);
      this.user = this._authService.getCurrentUser();
      this.role = this.user.role.name;
      this.statusId = this._dataStore.getData('uir_status_type_id');
      if ((this.role === 'Provider_Staff_Admin' && this.statusId === 49) ||
          (this.role !== 'Provider_Staff_Admin' && this.statusId !== 49)) {
        this.isDisabled = true;
      } else  {
        this.isDisabled = false;
      }
    }

  ngOnInit() {
    console.log('this.uir_number --1',this.uir_number);
    }

}
