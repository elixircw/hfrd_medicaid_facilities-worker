import { async, ComponentFixture, TestBed } from '@angular/core/testing';

  import { UirNarrativeComponent } from './uir-narrative.component';

describe('UriNarrativeComponent', () => {
  let component: UirNarrativeComponent;
  let fixture: ComponentFixture<UirNarrativeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UirNarrativeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UirNarrativeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
