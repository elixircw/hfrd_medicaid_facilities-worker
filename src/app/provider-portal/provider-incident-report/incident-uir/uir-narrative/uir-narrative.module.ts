import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UirNarrativeRoutingModule } from './uir-narrative-routing.module';
import { UirNarrativeComponent } from './uir-narrative.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';

@NgModule({
  imports: [
    CommonModule,
    UirNarrativeRoutingModule,
    FormMaterialModule
  ],
  declarations: [UirNarrativeComponent]
})
export class UirNarrativeModule { }
