import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonHttpService, AlertService, DataStoreService, AuthService } from '../../../../@core/services';
import { IncidentUirService } from '../incident-uir.service';
import { ProviderIncidentReportService } from '../../provider-incident-report.service';
import { UirDetail } from '../_entities/uir-model';
import { AppUser } from '../../../../@core/entities/authDataModel';

@Component({
  selector: 'uir-narrative',
  templateUrl: './uir-narrative.component.html',
  styleUrls: ['./uir-narrative.component.scss']
})
export class UirNarrativeComponent implements OnInit, OnDestroy {

  narrativeForm: FormGroup;
  providerUirId: string;
  uirNo: string;
  providerUirDetails: UirDetail;
  narrativeInformation = [];
  user: AppUser;
  role: string;
  statusId: number;
  isDisabled: boolean;
  url: any;
  id: any;
  constructor(private formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _router: Router, private route: ActivatedRoute,
    private _incidentService: IncidentUirService,
    private _providerIncident: ProviderIncidentReportService,
    private _dataStore: DataStoreService,
    private _authService: AuthService) {
      this.id = this._incidentService.provideruirid;
      this.providerUirId = (this.id !== 'create') ? this.id : '';
      this.uirNo = this._incidentService.uirid;
      this.user = this._authService.getCurrentUser();
      this.role = this.user.role.name;
      this.statusId = this._dataStore.getData('uir_status_type_id');
      if ((this.role === 'Provider_Staff_Admin' && this.statusId === 49) ||
          (this.role !== 'Provider_Staff_Admin' && this.statusId !== 49)) {
        this.isDisabled = true;
      } else  {
        this.isDisabled = false;
      }
      this.url = this._router.url;
      this.url = this.url.substring(0, this.url.lastIndexOf('/') + 1);
    }

  ngOnInit() {
    this.initNarrativeForm();
    if (this.providerUirId) {
      this.getNarrativeInformation();
    }
  }

  loadNarrativeForm() {
    if (this.narrativeInformation) {
      this.narrativeForm.patchValue({
        narrative_before_incident: this.narrativeInformation['narrative_before_incident'],
        narrative_incident_occur: this.narrativeInformation['narrative_incident_occur'],
        narrative_during_incident: this.narrativeInformation['narrative_during_incident'],
        narrative_after_incident: this.narrativeInformation['narrative_after_incident']
      });
    }
  }

  initNarrativeForm() {
    this.narrativeForm = this.formBuilder.group({
      narrative_before_incident: [null],
      narrative_incident_occur: [null],
      narrative_during_incident: [null],
      narrative_after_incident: [null],
    });
    if (this.isDisabled) {
      this.narrativeForm.disable();
    } else {
      this.narrativeForm.enable();
    }
  }

  patchNarrative(event?) {
    const narrativeData = this.narrativeForm.value;
    narrativeData.uir_no = this._incidentService.uirid;
    if (this.providerUirId) {
      narrativeData.provider_uir_id = this.providerUirId;
    }

    this._providerIncident.saveUirDetails(narrativeData).subscribe(response => {
      if (!this.providerUirId && response.provider_uir_id) {
        this.providerUirId = response.provider_uir_id;
        this.getNarrativeInformation();
        // this._providerIncident.navigateTo(this.uirNo, this.providerUirId, 'narrative');
      } else {
        // this.resetForm();
        this.providerUirDetails = response;
      }
      if (event === 'save') {
        if (this.id === 'create') {
          this.url = this.url.substr(0, this.url.length - 1);
          this.url = this.url.substring(0, this.url.lastIndexOf('/') + 1) + this.id + '/';
          this._alertService.warn('Please Fill the program tab First');
          setTimeout(() => {
            this._router.navigate([this.url + 'program']);
          }, 3000);
          this.getNarrativeInformation();
        } else {
          this._alertService.success('Information saved successfully!');
          // this._router.navigate([this.url + 'youth-involved']);
          this.getNarrativeInformation();
        }

      } else {
        if (this.id === 'create') {
          this.url = this.url.substr(0, this.url.length - 1);
          this.url = this.url.substring(0, this.url.lastIndexOf('/') + 1) + this.id + '/';
          this._router.navigate([this.url + 'program']);
        }
      }
    });
  }

  getNarrativeInformation() {
    const providerParam = {
      where: { provider_uir_id: this.providerUirId },
      method: 'get',
      count: -1 };
    this._providerIncident.getProviderUirDetails(providerParam).subscribe(response => {
      if (response) {
        this.narrativeInformation = response[0];
        this.loadNarrativeForm();
      }
    });
  }

  resetForm() {
    this.narrativeForm.reset();
    this.initNarrativeForm();
  }
  ngOnDestroy () {
    if (this.narrativeForm.getRawValue().narrative_before_incident || this.narrativeForm.getRawValue().narrative_during_incident
     || this.narrativeForm.getRawValue().narrative_incident_occur || this.narrativeForm.getRawValue().narrative_after_incident) {
      this.patchNarrative();
  }

}
}
