import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IncidentUirRoutingModule } from './incident-uir-routing.module';
import { IncidentUirComponent } from './incident-uir.component';
import { ProviderPortalUtilsModule } from '../../provider-portal-utils/provider-portal-utils.module';
import { UirHeaderComponent } from './uir-header/uir-header.component';
import { FormMaterialModule } from '../../../@core/form-material.module';
import { IncidentUirService } from './incident-uir.service';
import { IncidentUirResolverService } from './incident-uir-resolver.service';
import { AttachmentModule } from '../../provider-portal-utils/attachment/attachment.module';

@NgModule({
  imports: [
    CommonModule,
    IncidentUirRoutingModule,
    FormMaterialModule,
    ProviderPortalUtilsModule,
    AttachmentModule
  ],
  declarations: [IncidentUirComponent, UirHeaderComponent],
  providers: [IncidentUirService, IncidentUirResolverService],
})
export class IncidentUirModule { }
