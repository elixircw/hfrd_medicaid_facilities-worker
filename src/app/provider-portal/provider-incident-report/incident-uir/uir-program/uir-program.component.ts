import { Component, OnInit, OnDestroy } from '@angular/core';
import { CommonHttpService, AlertService, DataStoreService, CommonDropdownsService, AuthService } from '../../../../@core/services';
// import { ProviderPortalUrlConfig } from '../../../provider-portal-url.config';
import { IncidentUirService } from '../incident-uir.service';
import { DropdownModel } from '../../../../@core/entities/common.entities';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProviderIncidentReportService } from '../../provider-incident-report.service';
import { AppUser } from '../../../../@core/entities/authDataModel';
import * as moment from 'moment';
import { ProviderPortalService } from '../../../provider-portal.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AppErrorConfig } from '../../../../app.constants';

@Component({
  selector: 'uir-program',
  templateUrl: './uir-program.component.html',
  styleUrls: ['./uir-program.component.scss']
})
export class UirProgramComponent implements OnInit, OnDestroy {

  programNames$: Observable<DropdownModel[]>;
  providerId$: Observable<DropdownModel[]>;
  programNamesDropdown: any[];
  selectedSiteId: string;
  selectedLicenseNo: string;
  licenseInformation: any;
  selectedSiteLicenseInformation: any;
  selectedSiteInformation: any;
  uirProgramForm: FormGroup;
  class3IncidentSource$: any;
  user: AppUser;
  role: string;
  statusId: any;
  isDisabled: boolean;
  selectedProviderId: string;
  selectedProviderName: string;
  selectedProviderEmail: string;
  selectedProviderPhone: string;
  selectedProgramAddress: string;
  selectedProgramProviderName: string;
  selectedProgram: string;
  selectedProgramType: string;
  selectedProgramSiteId: string;
  selectedProgramEmail: string;
  selectedProgramPhone: string;
  selectedProgramContactName: string;
  selectedProgramContactPhone: string;
  selectedProgramContactEmail: string;
  isClass3Incident: boolean;
  providerUirId: any;
  url: any;
  isProgramSaved: Boolean = false;
  constructor(private _commonHttpService: CommonHttpService,
    private _incidentService: IncidentUirService,
    private _alertService: AlertService,
    private commonDropdownService: CommonDropdownsService,
    private formBuilder: FormBuilder,
    private _providerIncidentService: ProviderIncidentReportService,
    private _dataStore: DataStoreService,
    private _authService: AuthService,
    private _router: Router,
    private _providerPortalService: ProviderPortalService,
    private _route: ActivatedRoute) {

    this.providerUirId = this._incidentService.provideruirid;
    this.user = this._authService.getCurrentUser();
    this.role = this.user.role.name;
    this.statusId = this._dataStore.getData('uir_status_type_id');
    if ((this.role === 'Provider_Staff_Admin' && this.statusId === 49) ||
        (this.role !== 'Provider_Staff_Admin' && this.statusId !== 49)) {
      this.isDisabled = true;
    } else  {
      this.isDisabled = false;
    }
    this.url = this._router.url;
    this.url = this.url.substring(0, this.url.lastIndexOf('/') + 1);
}

  ngOnInit() {
    this.initForms();
    this.loadDropdowns();
    this.getClassIIIEvents();
    this.getProgramDetails();
  }

  loadDropdowns() {
    const source = forkJoin([
      this._incidentService.getProgramNames()
    ])
      .map((result) => {
        return {
          programNames: result[0].map(
            (res) =>
              new DropdownModel({
                text: res.license_type,
                value: res.site_id,
                parent_provider_name: res.parent_provider_nm
              })
          )
        };
      })
      .share();

    this.programNames$ = source.pluck('programNames');
  }



  initForms() {
    this.uirProgramForm = this.formBuilder.group({
      // incident_no: [''],
      // provider_nm: [''],
      program_nm: [''],
      // program: [''],
      // program_type: [''],
      license_no: [''],
      site_id: [null],
      // phone_no: [null],
      // uir_adr_street_no: [''],
      // uir_adr_street_nm: [''],
      // uir_adr_city_nm: [''],
      // uir_adr_state_cd: [''],
      // uir_adr_zip_no: [''],
      // formatted_address: [null],
      // incident_date: ['', Validators.required],
      // incident_time: ['', Validators.required],
      is_classthreeincident: [null],
      // selectedOptions:[''],
      // parent_provider_nm:[''],
      additional_youth_info: [null],
      class3_brief_desc: [null]
    });
    if (this.isDisabled) {
      this.uirProgramForm.disable();
    } else {
      this.uirProgramForm.enable();
    }
  }

  getProgramDetails() {
    this._incidentService.getProgramDetails().subscribe(response => {
      if (response && response.length) {
        const programData = response[response.length - 1];
        this.uirProgramForm.patchValue({
          program_nm: parseInt(programData.program_nm, 10),
          // incident_date: moment(programData.incident_date).format('YYYY-MM-DD'),
          // incident_time: programData.incident_time, // moment.utc(programData.incident_time).format('HH:mm'),
          is_classthreeincident: programData.classthree_incident,
          additional_youth_info: programData.additional_youth_info,
          class3_brief_desc: programData.class3_brief_desc,
        });

        this.isClass3Incident = (programData.classthree_incident === 1) ? true : false;

        if (programData.program_nm) {
          this.selectedSiteId = programData.program_nm;
          this.getSiteLicenseInformation();
          this.getSiteInformation();
        }
      }
    });
  }

  changeSiteId(event) {
    this.selectedSiteId = event.value;
    this.getSiteLicenseInformation();
    this.getSiteInformation();
  }
  getClassIIIEvents() {
    Observable.forkJoin([
      this.commonDropdownService.getDropownsByTable('Class_III_Incidents', 'OLM')
    ]).subscribe(
      ([ class3IncidentSource]) => {
        this.class3IncidentSource$ = class3IncidentSource;
    // cconsole.log(class3IncidentSource);
      });
  }

  onNgModelChange(event) {
    this._providerIncidentService.selectClassIncident();
    if (this.providerUirId === 'create' && !this.isProgramSaved) {
      this.submitProgram(event.value);
    } else {
      if (event.value === 1) {
        this.isClass3Incident = true;
       setTimeout(() => {
        const el = document.getElementById('personTab');
        el.scrollIntoView({ behavior: 'smooth', block: 'nearest' });
       }, 500);
      } else {
        this.isClass3Incident = false;
      }
    }
  }

  getSiteInformation() {
    this.selectedSiteInformation = {};
    this._incidentService.getSelectedSiteInformation(this.selectedSiteId)
      .subscribe(response => {
        if (response && response.length) {
          this.selectedSiteInformation = response;
          this.selectedSiteInformation = this.selectedSiteInformation[0];
          this.selectedProviderId = this.selectedSiteInformation.provider_id;
          this.selectedProviderName = this.selectedSiteInformation.providername;
          this.selectedProviderEmail = this.selectedSiteInformation.email;
          this.selectedProviderPhone = this.selectedSiteInformation.phone;
          this.selectedProgramAddress = this.selectedSiteInformation.prgm_app_address;
          this.selectedProgram = this.selectedSiteInformation.prgm_app_programtype;
          this.selectedProgramType = this.selectedSiteInformation.programname;
          this.selectedProgramSiteId = this.selectedSiteInformation.site_id;
          this.selectedProgramEmail = this.selectedSiteInformation.prgm_app_mail;
        }
      },
      (error) => {
        this._alertService.error('Unable to get site information, please try again.');
          console.log('get site information Error', error);
          return false;
      }
      );
  }

  getSiteLicenseInformation() {
    this.selectedSiteLicenseInformation = {};
    this._incidentService.getSelectedProgramInformation(this.selectedSiteId)
      .subscribe(response => {
        if (response && response.length) {
          this.selectedSiteLicenseInformation = response;
          this.selectedSiteLicenseInformation = this.selectedSiteLicenseInformation.data[0];
          this.selectedLicenseNo = this.selectedSiteLicenseInformation.license_no;
          this.patchSiteInformation();
        }
      },
        (error) => {
          this._alertService.error('Unable to get license information, please try again.');
          console.log('get license information Error', error);
          return false;
        }
      );
  }

  patchSiteInformation() {
    this.uirProgramForm.patchValue({
      site_id: this.selectedSiteId,
      formatted_address: this.selectedSiteLicenseInformation.formatted_address,
      // uir_adr_street_no: this.selectedSiteLicenseInformation.adr_street_no,
      // uir_adr_street_nm: this.selectedSiteLicenseInformation.adr_street_nm,
      // uir_adr_city_nm: this.selectedSiteLicenseInformation.adr_city_nm,
      // uir_adr_state_cd: this.selectedSiteLicenseInformation.adr_state_cd,
      // uir_adr_zip_no: this.selectedSiteLicenseInformation.adr_zip5_no,
      program: this.selectedSiteLicenseInformation.license_level,
      phone_no: this.selectedSiteLicenseInformation.adr_work_phone_tx,
      program_type: this.selectedSiteLicenseInformation.license_type,
      license_no: this.selectedSiteLicenseInformation.license_no
    });
  }

  submitProgram(event?) {
    if (this.uirProgramForm.valid) {
      const uirDetails = this.uirProgramForm.getRawValue();
      uirDetails.license_type = this.uirProgramForm.getRawValue().program_nm;
      uirDetails.site_id = this.uirProgramForm.getRawValue().site_id ? this.uirProgramForm.getRawValue().site_id : this.selectedSiteId;
     // uirDetails.incident_time = moment(uirDetails.incident_date).format('YYYY/MM/DD') + ' ' + uirDetails.incident_time;
      console.log(this.providerUirId);
      uirDetails.provider_uir_id = (this.providerUirId === 'create' || !this.providerUirId ) ? null : this.providerUirId;
      uirDetails.uir_no = this._incidentService.uirid;
      uirDetails.provider_id = this._providerPortalService.getProviderId(),
      this._providerIncidentService.saveUirDetails(uirDetails).subscribe(response => {
        if (response) {
          const programDetails = response;
          this.isProgramSaved = true;
          if (this.providerUirId === 'create') {
            this.url = this.url.substr(0, this.url.length - 1);
            this.url = this.url.substring(0, this.url.lastIndexOf('/') + 1) + programDetails.provider_uir_id + '/';
          }
          this.programSaveChanges(event, programDetails.provider_uir_id);
        }
      });
    }
  }

  programSaveChanges(event, provider_id) {
    if (event === 'save') {
      this._alertService.success('Program details saved successfully');
      this.providerUirId = provider_id;
      if (this.isClass3Incident) {
          // this._router.navigate([this.url + 'notification']);
      } else {
          // this._router.navigate([this.url + 'person']);
      }
    } else if ( event === 1) {
      this.isClass3Incident = true;
      if (this.providerUirId === 'create') {
        this.url = this.url + 'program';
        this.providerUirId = provider_id;
        this._router.navigateByUrl('/', { skipLocationChange: true }).then(() => this._router.navigate([this.url]));
      }
    } else if ( event === 0) {
      this.isClass3Incident = false;
      if (this.providerUirId === 'create') {
        this.url = this.url + '/program';
        this.providerUirId = provider_id;
        this._router.navigateByUrl('/', { skipLocationChange: true }).then(() => this._router.navigate([this.url]));
      }
    }
  }

  ngOnDestroy() {
    if (this.uirProgramForm.getRawValue().program_nm || this.uirProgramForm.getRawValue().is_classthreeincident) {
      const isClass3Incident = this.uirProgramForm.getRawValue().is_classthreeincident;
      if (isClass3Incident === 1) {
        this.submitProgram(1);
      } else {
        this.submitProgram(0);
      }
    }
  }

  sendNotification() {
    const requestObj = {
      email: 'nkumar@dminc.com',
      where: {
        provider_id: this.selectedProviderId,
        site_id: this.selectedSiteId,
        uir_no: this._incidentService.uirid
      }
    };
    this._commonHttpService.create(requestObj, 'Provider_uir/sendonehournotification').subscribe(
      (result) => {
        if (result.status === AppErrorConfig.SUCCESS) {
          this._alertService.success('Email Sent successfully!');
        }
      },
      (error) => {
        console.log('error');
      }
    );
  }
}
