import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UirNotificationRoutingModule } from './uir-notification-routing.module';
import { UirNotificationComponent } from './uir-notification.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';

@NgModule({
  imports: [
    CommonModule,
    UirNotificationRoutingModule,
    FormMaterialModule
  ],
  declarations: [UirNotificationComponent]
})
export class UirNotificationModule { }
