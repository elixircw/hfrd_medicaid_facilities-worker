import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UirNotificationComponent } from './uir-notification.component';

describe('UirNotificationComponent', () => {
  let component: UirNotificationComponent;
  let fixture: ComponentFixture<UirNotificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UirNotificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UirNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
