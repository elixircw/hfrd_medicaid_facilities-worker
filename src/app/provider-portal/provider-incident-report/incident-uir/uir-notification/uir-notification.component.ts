import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { CommonHttpService, DataStoreService, AlertService, AuthService } from '../../../../@core/services';
import { IncidentUirService } from '../incident-uir.service';
import { ProviderIncidentReportService } from '../../provider-incident-report.service';
import * as moment from 'moment';
import { Notification } from '../_entities/uir-model';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { Router } from '@angular/router';


@Component({
  selector: 'uir-notification',
  templateUrl: './uir-notification.component.html',
  styleUrls: ['./uir-notification.component.scss']
})
export class UirNotificationComponent implements OnInit, OnDestroy {

  notificationForm: FormGroup;
  providerUirId: string;
  uirNo: string;
  methodType: any[];
  notificationInfo: Notification;
  isDisabled: boolean;
  user: AppUser;
  role: string;
  statusId: any;
  url: any;

  id: any;
  constructor(private fb: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _dataStore: DataStoreService,
    private _incidentService: IncidentUirService,
    private _providerIncident: ProviderIncidentReportService,
    private _authService: AuthService,
    private _router: Router,
    ) {
      this.id = this._incidentService.provideruirid;
      this.providerUirId = (this.id !== 'create') ? this.id : '';
      this.uirNo = this._incidentService.uirid;
      this.user = this._authService.getCurrentUser();
      this.role = this.user.role.name;
      this.statusId = this._dataStore.getData('uir_status_type_id');
      if ((this.role === 'Provider_Staff_Admin' && this.statusId === 49) ||
          (this.role !== 'Provider_Staff_Admin' && this.statusId !== 49)) {
        this.isDisabled = true;
      } else  {
        this.isDisabled = false;
      }

      console.log(this.isDisabled);

      this.notificationForm = this.fb.group({
        notification: this.fb.array([]),
        notifiy_is_attach: [null],
        notifiy_attach_other: [null],
        notifiy_staff_member: [null],
        notifiy_signdatetime: [null],
        notification_comments: [null]
      });
      if (this.isDisabled) {
        this.notificationForm.disable();
      } else {
        this.notificationForm.enable();
      }

      this.url = this._router.url;
      this.url = this.url.substring(0, this.url.lastIndexOf('/') + 1);
  }

  ngOnInit() {
    this.getMethodList();
    this.formFieldGroup();
    if (this.providerUirId) {
      this.getNotificationInfo();
    }
  }

  formFieldGroup() {
    const rows = this.notificationForm.get('notification') as FormArray;
    rows.push(this.fb.group({
      services_name: new FormControl({ value: 'Program Administrator', disabled: true }), notifcation_name: [{ value: null, disabled: this.isDisabled }],
      notifcation_datetime: [null], notifcation_received: [null], notifcation_comments: [{ value: null, disabled: this.isDisabled }], method_type: [null],
      provider_uir_notification_id: [{ value: null, disabled: this.isDisabled }]
    }));
    const rows1 = this.notificationForm.get('notification') as FormArray;
    rows1.push(this.fb.group({
      services_name: new FormControl({ value: 'Licensing Agency', disabled: true }), notifcation_name: [{ value: null, disabled: this.isDisabled }],
      notifcation_datetime: [null], notifcation_received: [null], notifcation_comments: [{ value: null, disabled: this.isDisabled }], method_type: [null],
      provider_uir_notification_id: [{ value: null, disabled: this.isDisabled }]
    }));
    const rows2 = this.notificationForm.get('notification') as FormArray;
    rows2.push(this.fb.group({
      services_name: new FormControl({ value: 'Child Protective Services', disabled: true }), notifcation_name: [{ value: null, disabled: this.isDisabled }],
      notifcation_datetime: [null], notifcation_received: [null], notifcation_comments: [{ value: null, disabled: this.isDisabled }], method_type: [null],
      provider_uir_notification_id: [{ value: null, disabled: this.isDisabled }]
    }));
    const rows3 = this.notificationForm.get('notification') as FormArray;
    rows3.push(this.fb.group({
      services_name: new FormControl({ value: 'Placing Agency', disabled: true }), notifcation_name: [{ value: null, disabled: this.isDisabled }],
      notifcation_datetime: [null], notifcation_received: [null], notifcation_comments: [{ value: null, disabled: this.isDisabled }], method_type: [null],
      provider_uir_notification_id: [{ value: null, disabled: this.isDisabled }]
    }));
    const rows4 = this.notificationForm.get('notification') as FormArray;
    rows4.push(this.fb.group({
      services_name: new FormControl({ value: 'Law enforcement', disabled: true }), notifcation_name: [{ value: null, disabled: this.isDisabled }],
      notifcation_datetime: [null], notifcation_received: [null], notifcation_comments: [{ value: null, disabled: this.isDisabled }], method_type: [null],
      provider_uir_notification_id: [{ value: null, disabled: this.isDisabled }]
    }));
    const rows5 = this.notificationForm.get('notification') as FormArray;
    rows5.push(this.fb.group({
      services_name: new FormControl({ value: 'OHCQ/BHA/CSA', disabled: true }), notifcation_name: [{ value: null, disabled: this.isDisabled }],
      notifcation_datetime: [null], notifcation_received: [null], notifcation_comments: [{ value: null, disabled: this.isDisabled }], method_type: [null],
      provider_uir_notification_id: [{ value: null, disabled: this.isDisabled }]
    }));
    const rows6 = this.notificationForm.get('notification') as FormArray;
    rows6.push(this.fb.group({
      services_name: new FormControl({ value: 'OHCQ/DDA', disabled: true }), notifcation_name: [{ value: null, disabled: this.isDisabled }],
      notifcation_datetime: [null], notifcation_received: [null], notifcation_comments: [{ value: null, disabled: this.isDisabled }], method_type: [null],
      provider_uir_notification_id: [{ value: null, disabled: this.isDisabled }]
    }));
    const rows7 = this.notificationForm.get('notification') as FormArray;
    rows7.push(this.fb.group({
      services_name: new FormControl({ value: 'Other', disabled: true }), notifcation_name: [{ value: null, disabled: this.isDisabled }],
      notifcation_datetime: [null], notifcation_received: [null], notifcation_comments: [{ value: null, disabled: this.isDisabled }], method_type: [null],
      provider_uir_notification_id: [{ value: null, disabled: this.isDisabled }]
    }));
    const rows8 = this.notificationForm.get('notification') as FormArray;
    rows8.push(this.fb.group({
      services_name: new FormControl({ value: 'Placing Agency Case Worker', disabled: true }), notifcation_name: [{ value: null, disabled: this.isDisabled }],
      notifcation_datetime: [null], notifcation_received: [null], notifcation_comments: [{ value: null, disabled: this.isDisabled }], method_type: [null],
      provider_uir_notification_id: [{ value: null, disabled: this.isDisabled }]
    }));
    const rows9 = this.notificationForm.get('notification') as FormArray;
    rows9.push(this.fb.group({
      services_name: new FormControl({ value: 'Parent/Guardian (if appropriate)', disabled: true }), notifcation_name: [{ value: null, disabled: this.isDisabled }],
      notifcation_datetime: [null], notifcation_received: [null], notifcation_comments: [{ value: null, disabled: this.isDisabled }], method_type: [null],
      provider_uir_notification_id: [{ value: null, disabled: this.isDisabled }]
    }));
    const rows10 = this.notificationForm.get('notification') as FormArray;
    rows10.push(this.fb.group({
      services_name: new FormControl({ value: 'Legal Representative', disabled: true }), notifcation_name: [{ value: null, disabled: this.isDisabled }],
      notifcation_datetime: [null], notifcation_received: [null], notifcation_comments: [{ value: null, disabled: this.isDisabled }], method_type: [null],
      provider_uir_notification_id: [{ value: null, disabled: this.isDisabled }]
    }));
  }

  patchNotification() {
    if (this.notificationInfo) {
      this.notificationForm.patchValue({
       // notification: this.fb.array([this.notificationInfo.notification]),
        notifiy_is_attach: this.notificationInfo.notifiy_is_attach,
        notifiy_attach_other: this.notificationInfo.notifiy_attach_other,
        notifiy_staff_member: this.notificationInfo.notifiy_staff_member,
        notifiy_signdatetime: this.notificationInfo.notifiy_signdatetime,
        notification_comments: this.notificationInfo.notification_comments
      });
      // if (this.notificationInfo.notification) {
      //   this.notificationForm.setControl('notification', this.fb.array(this.notificationInfo.notification || []));
      // }

      if (this.notificationInfo.notification) {
        const control = <FormArray>this.notificationForm.controls['notification'];
        const notificationList = this.notificationInfo.notification;
        for (const item of control.controls) {
          if (item instanceof FormGroup) {
            const obj = item.getRawValue();
            const notification = notificationList.find(ele => ele.services_name === obj.services_name);
            if (notification) {
              item.patchValue(notification);
            }
          }
       }
      }
    }
  }

  private buildResourceListForm(x): FormGroup {
    const resourceList = this.fb.group({
      method_type: x.method_type,
      notifcation_comments: x.notifcation_comments,
      notifcation_datetime: x.notifcation_datetime,
      notifcation_name: x.notifcation_name,
      notifcation_received: x.notifcation_received,
      notification_type: x.notification_type,
      provider_uir_id: x.provider_uir_id,
      provider_uir_notification_id: x.provider_uir_notification_id,
      services_name: x.services_name
    });
    return resourceList;
}

  getMethodList() {
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        where: {'tablename': 'UIR_Notification_Information', 'teamtypekey': 'OLM'},
      },
      'referencetype/gettypes?filter'
    ).subscribe(
      (response) => {
        if (response) {
          this.methodType = response;
        }
      },
      (error) => {
        this._alertService.error('Unable to retrieve information');
      }
    );
  }

  getNotificationInfo() {
    const providerParam = {
              where: { provider_uir_id: this.providerUirId },
              method: 'get',
              count: -1
    };

    this._providerIncident.getUirNotificationDetails(providerParam).subscribe(response => {
      if (response) {
          this.notificationInfo = response[0];
          this.patchNotification();
      }
    });
    // const providerIdKeyVal = (this.providerUirId) ? { provider_uir_id: this.providerUirId} : {};
    // this._commonHttpService.getArrayList(
    //   {
    //     method: 'get',
    //     nolimit: true,
    //     where : providerIdKeyVal,
    //   },
    //   'provider_uir_notification/list?filter'
    // ).subscribe(
    //   (response) => {
    //     if (response) {
    //       this.notificationInfo = response[0];
    //       console.log(response);
    //       this.patchNotification();
    //     }
    //   },
    //   (error) => {
    //     this._alertService.error('Unable to retrieve information');
    //   }
    // );
  }

  submitNotification(event?) {
    const notificationInfo = this.notificationForm.getRawValue();
    notificationInfo.uir_no = this._incidentService.uirid;
    notificationInfo.notifiy_signdatetime = (this.notificationForm.value.notifiy_signdatetime) ? moment(this.notificationForm.value.notifiy_signdatetime).format('YYYY-M-DD hh:mm:ss') : null;
    if (this.providerUirId) {
      notificationInfo.provider_uir_id = this.providerUirId;
    }

    this._providerIncident.saveNotificationDetails(notificationInfo).subscribe(response => {
      if (response) {
        if (!this.providerUirId && response.provider_uir_id) {
          this.providerUirId = response.provider_uir_id;
          // this._providerIncident.navigateTo(this.uirNo, this.providerUirId, 'notification');
          this.getNotificationInfo();
        }

        if (event === 'save') {
          if (this.id === 'create') {
            this.url = this.url.substr(0, this.url.length - 1);
            this.url = this.url.substring(0, this.url.lastIndexOf('/') + 1) + this.id + '/';
            this._alertService.warn('Please Fill the program tab');
            setTimeout(() => {
              this._router.navigate([this.url + 'program']);
            }, 3000);

          } else {
            this._alertService.info('Information saved successfully!');
            // this._router.navigate([this.url + 'narrative']);
            this.getNotificationInfo();
          }
        } else {
          if (this.id === 'create') {
            this.url = this.url.substr(0, this.url.length - 1);
            this.url = this.url.substring(0, this.url.lastIndexOf('/') + 1) + this.id + '/';
            this._router.navigate([this.url + 'program']);
          }
        }

      }
    });

  }
  ngOnDestroy() {
    if (this.notificationForm.getRawValue().notifiy_staff_member || this.notificationForm.getRawValue().notifiy_signdatetime
    || this.notificationForm.getRawValue().notifiy_attach_other) {
      this.submitNotification();
    }
  }
}
