import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CommonHttpService, AlertService, DataStoreService, AuthService } from '../../../../@core/services';
import { IncidentUirService } from '../incident-uir.service';
import { ProviderIncidentReportService } from '../../provider-incident-report.service';
import * as jsPDF from 'jspdf';
import { AppUser } from '../../../../@core/entities/authDataModel';


@Component({
  selector: 'uir-witness',
  templateUrl: './uir-witness.component.html',
  styleUrls: ['./uir-witness.component.scss']
})
export class UirWitnessComponent implements OnInit {
  witnessForm: FormGroup;
  witnessInformation = [];
  currentSelectedWitnessId: string;
  providerUirId: string;
  uirNo: string;
  isEditWitness: boolean;

  pdfFiles: { fileName: string; images: { image: string; height: any; name: string }[] }[] = [];
  downloadInProgress: boolean;
  downloadWitnessData = [];
  user: AppUser;
  role: string;
  statusId: any;
  isDisabled: boolean;
  constructor(private formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _router: Router, private route: ActivatedRoute,
    private _incidentService: IncidentUirService,
    private _providerIncident: ProviderIncidentReportService,
    private _dataStore: DataStoreService,
    private _authService: AuthService) {
      const id = this._incidentService.provideruirid;
      this.providerUirId = (id !== 'create') ? id : '';
      this.uirNo = this._incidentService.uirid;
      this.user = this._authService.getCurrentUser();
      this.role = this.user.role.name;
      this.statusId = this._dataStore.getData('uir_status_type_id');
      if ((this.role === 'Provider_Staff_Admin' && this.statusId === 49) ||
          (this.role !== 'Provider_Staff_Admin' && this.statusId !== 49)) {
        this.isDisabled = true;
      } else  {
        this.isDisabled = false;
      }
    }

  ngOnInit() {
    this.initWitnessForm();
    if (this.providerUirId) {
      this.getWitnessInformation();
    }
  }

  initWitnessForm() {
    this.witnessForm = this.formBuilder.group({
      first_name: [null],
      last_name: [null],
      actor_type_id: [null],
      other_actor_type: [null],
      witness_statement: [null]
    });
    if (this.isDisabled) {
      this.witnessForm.disable();
    } else {
      this.witnessForm.enable();
    }
  }

  getWitnessInformation() {
    const narrativeParam = {
      where: { provider_uir_id: this.providerUirId },
      method: 'get',
      count: -1 };
    this._providerIncident.getNarrativeDetails(narrativeParam).subscribe(response => {
      if (response) {
        this.witnessInformation = response;
      }
    });
  }
  resetWitnessForm() {
    this.witnessForm.reset();
    this.initWitnessForm();
  }

  patchWitness() {
    const witnessData = this.witnessForm.value;
    witnessData.uir_no = this._incidentService.uirid;
    if (this.providerUirId) {
      witnessData.provider_uir_id = this.providerUirId;
    }

    if (this.isEditWitness) {
      witnessData.provider_uir_witness_id = this.currentSelectedWitnessId;
    }

    this._providerIncident.saveWitnessDetails(witnessData).subscribe(response => {
        if (response) {
          if (!this.providerUirId && response.provider_uir_id) {
            this.providerUirId = response.provider_uir_id;
            // this._providerIncident.navigateTo(this.uirNo, this.providerUirId, 'witness');
          }
          this.resetWitnessForm();
          this.getWitnessInformation();
        }
        this._alertService.success('Information saved successfully!');
    });
    (<any>$('#add-witness')).modal('hide');
  }

  navigateTo() {
    const redirectUrl = '/provider-portal/incident-report/uir/' + this.uirNo + '/' + this.providerUirId + '/witness';
    this._router.navigate([redirectUrl]);
  }

  deleteWitness(witnessInfo) {
    this.currentSelectedWitnessId = witnessInfo.provider_uir_witness_id;
    (<any>$('#delet-witness')).modal('show');
  }

  editWitness(witnessInfo) {
    this.isEditWitness = true;
    this.currentSelectedWitnessId = witnessInfo.provider_uir_witness_id;
    (<any>$('#add-witness')).modal('show');
    this.witnessForm.patchValue({
      provider_uir_id: this.providerUirId,
      first_name: witnessInfo.first_name,
      last_name: witnessInfo.last_name,
      actor_type_id: witnessInfo.actor_type_id,
      other_actor_type: (witnessInfo.actor_type_id === '3') ? witnessInfo.other_actor_type : '',
      witness_statement: witnessInfo.witness_statement
    });
  }

  openAddWitness() {
    this.isEditWitness = false;
    this.currentSelectedWitnessId = '';
  }

  confirmDelete() {
    this._commonHttpService.deleteByPost(
      '',
      {
       provider_uir_witness_id: this.currentSelectedWitnessId
      },
      'provider_uir_witness/deletewitness').subscribe(
      (response) => {
        this.getWitnessInformation();
      },
      (error) => {
        this._alertService.error('Unable to delete Witness');
        return false;
      }
    );
  (<any>$('#delet-witness')).modal('hide');
  }

  /****  Download witness ****/

  download_witness(witness) {
    (<any>$('#generate_witness_form')).modal('show');
    this.downloadWitnessData = (witness) ? witness : [];
  }

  collectivePdfCreator(element: string) {
      this.downloadCasePdf(element);
  }

  async downloadCasePdf(element: string) {
      const source = document.getElementById(element);
      const pages = source.getElementsByClassName('pdf-page');
      let pageImages = [];
      for (let i = 0; i < pages.length; i++) {
          // console.log(pages.item(i).getAttribute('data-page-name'));
          const pageName = pages.item(i).getAttribute('data-page-name');
          const isPageEnd = pages.item(i).getAttribute('data-page-end');
          await html2canvas(<HTMLElement>pages.item(i)).then((canvas) => {
              const img = canvas.toDataURL('image/png');
              pageImages.push(img);
              if (isPageEnd === 'true') {
                  this.pdfFiles.push({ fileName: pageName, images: pageImages });
                  pageImages = [];
              }
          });
      }
      this.convertImageToPdf();
  }

  convertImageToPdf() {
      this.pdfFiles.forEach((pdfFile) => {
          const doc = new jsPDF();
          pdfFile.images.forEach((image, index) => {
              doc.addImage(image, 'JPEG', 0, 0);
              if (pdfFile.images.length > index + 1) {
                  doc.addPage();
              }
          });
          doc.save(pdfFile.fileName);
      });
      this.goBack();
      this.pdfFiles = [];
      this.downloadInProgress = false;
  }
  goBack(): void {
      (<any>$('#payment-slip-document')).modal('hide');
      this._router.navigate(['../'], { relativeTo: this.route });
  }
}
