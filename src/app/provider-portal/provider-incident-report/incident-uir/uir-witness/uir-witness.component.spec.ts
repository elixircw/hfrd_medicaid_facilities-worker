import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UirWitnessComponent } from './uir-witness.component';

describe('UirWidnessComponent', () => {
  let component: UirWitnessComponent;
  let fixture: ComponentFixture<UirWitnessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UirWitnessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UirWitnessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
