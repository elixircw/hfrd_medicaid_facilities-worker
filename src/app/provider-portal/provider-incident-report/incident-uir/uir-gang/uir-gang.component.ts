import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonHttpService, AlertService, DataStoreService, AuthService } from '../../../../@core/services';
import { IncidentUirService } from '../incident-uir.service';
import { UirDetail } from '../_entities/uir-model';
import { ProviderIncidentReportService } from '../../provider-incident-report.service';
import { AppUser } from '../../../../@core/entities/authDataModel';

@Component({
  selector: 'uir-gang',
  templateUrl: './uir-gang.component.html',
  styleUrls: ['./uir-gang.component.scss']
})
export class UirGangComponent implements OnInit, OnDestroy {

  gangForm: FormGroup;
  gangInfo = [];

  providerUirDetails: UirDetail;
  providerUirId: string;
  uirNo: string;
  user: AppUser;
  role: string;
  statusId: any;
  isDisabled: boolean;
  url: any;
  id: any;

  constructor(private formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _router: Router, private route: ActivatedRoute,
    private _incidentService: IncidentUirService,
    private _providerIncident: ProviderIncidentReportService,
    private _authService: AuthService,
    private _dataStore: DataStoreService) {
    this.id = this._incidentService.provideruirid;
    this.providerUirId = (this.id !== 'create') ? this.id : '';
    this.uirNo = this._incidentService.uirid;
    this.user = this._authService.getCurrentUser();
    this.role = this.user.role.name;
    this.statusId = this._dataStore.getData('uir_status_type_id');
    if ((this.role === 'Provider_Staff_Admin' && this.statusId === 49) ||
      (this.role !== 'Provider_Staff_Admin' && this.statusId !== 49)) {
      this.isDisabled = true;
    } else {
      this.isDisabled = false;
    }
    this.url = this._router.url;
    this.url = this.url.substring(0, this.url.lastIndexOf('/') + 1);
  }

  ngOnInit() {
    this.initGangForm();
    if (this.providerUirId) {
      this.getGangInformation(this.providerUirId);
    }
  }

  loadGangForm(gangInfo) {
    this.gangForm.patchValue({
      gang_related_explain: gangInfo.gang_related_explain,
      gang_incident_videotapped: gangInfo.gang_incident_videotapped,
      gang_support_evidence: gangInfo.gang_support_evidence,
    });
  }

  initGangForm() {
    this.gangForm = this.formBuilder.group({
      gang_related_explain: [null],
      gang_incident_videotapped: [null, Validators.required],
      gang_support_evidence: [null],
    });
    if (this.isDisabled) {
      this.gangForm.disable();
    } else {
      this.gangForm.enable();
    }
  }

  patchGang(event?) {
    const gangData = this.gangForm.value;
    gangData.uir_no = this._incidentService.uirid;
    if (this.providerUirId) {
      gangData.provider_uir_id = this.providerUirId;
    }

    this._providerIncident.saveUirDetails(gangData).subscribe(response => {
      if (response) {
        if (!this.providerUirId && response.provider_uir_id) {
          this.providerUirId = response.provider_uir_id;
          // this._providerIncident.navigateTo(this.uirNo, this.providerUirId, 'gang');
        } else {
          this.resetForm();
          this.providerUirDetails = response;
          this.loadGangForm(response);
        }
      }
      if (event === 'save') {
        if (this.id === 'create') {
          this.url = this.url.substr(0, this.url.length - 1);
          this.url = this.url.substring(0, this.url.lastIndexOf('/') + 1) + this.id + '/';
          this._alertService.warn('Please Fill the program tab First');
          setTimeout(() => {
            this._router.navigate([this.url + 'program']);
          }, 3000);

        } else {
          this._alertService.info('Information saved successfully!');
          this._router.navigate([this.url + 'supervisory']);
        }
      } else {
        if (this.id === 'create') {
          this.url = this.url.substr(0, this.url.length - 1);
          this.url = this.url.substring(0, this.url.lastIndexOf('/') + 1) + this.id + '/';
          this._router.navigate([this.url + 'program']);
        }
      }
      // this._alertService.success('Information saved successfully!');
      // this._router.navigate([this.url + 'supervisory']);
    });

  }

  getGangInformation(providerUirId) {
    const providerParam = {
      where: { provider_uir_id: providerUirId },
      method: 'get',
      count: -1
    };
    this._providerIncident.getProviderUirDetails(providerParam).subscribe(response => {
      if (response) {
        this.gangInfo = response[0];
        this.loadGangForm(this.gangInfo);
      }
    });
  }

  resetForm() {
    this.gangForm.reset();
    this.initGangForm();
  }
  ngOnDestroy() {
    if (this.gangForm.getRawValue().gang_related_explain || this.gangForm.getRawValue().gang_incident_videotapped
      || this.gangForm.getRawValue().gang_support_evidence) {
      this.patchGang();
    }
  }

}
