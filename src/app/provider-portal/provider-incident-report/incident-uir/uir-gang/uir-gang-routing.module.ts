import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UirGangComponent } from './uir-gang.component';

const routes: Routes = [
  {
    path: '',
    component: UirGangComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UirGangRoutingModule { }
