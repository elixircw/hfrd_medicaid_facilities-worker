import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IncidentUirService } from './incident-uir.service';

@Component({
  selector: 'incident-uir',
  templateUrl: './incident-uir.component.html',
  styleUrls: ['./incident-uir.component.scss']
})
export class IncidentUirComponent implements OnInit {

  constructor(private route: ActivatedRoute,
    private _service: IncidentUirService,
  ) {
    this.route.data.subscribe(data => {
      if (data && data.uirDetail && data.uirDetail.length) {
        this._service.uirDetail = data.uirDetail[0];
      } else {
        this._service.uirDetail = this._service.getMetaData(this._service.uirid);
      }
    });
  }

  ngOnInit() {
  }

}
