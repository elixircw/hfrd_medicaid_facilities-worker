import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UirPersonRoutingModule } from './uir-person-routing.module';
import { UirPersonComponent } from './uir-person.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { IncidentUirService } from '../incident-uir.service';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { SortTableModule } from '../../../../shared/modules/sortable-table/sortable-table.module';
import { NgxMaskModule } from 'ngx-mask';
@NgModule({
  imports: [
    CommonModule,
    UirPersonRoutingModule,
    FormMaterialModule,
    PaginationModule,
    SortTableModule,
    NgxMaskModule.forRoot(),
  ],
  declarations: [UirPersonComponent],
  exports: [UirPersonComponent]
  //providers: [IncidentUirService]
})
export class UirPersonModule { }
