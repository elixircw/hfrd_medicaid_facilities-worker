import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { CommonHttpService, AlertService, DataStoreService, AuthService, CommonDropdownsService, ValidationService } from '../../../../@core/services';
import { MatTableDataSource, MatRadioChange } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { IncidentUirService } from '../incident-uir.service';
import { ProviderIncidentReportService } from '../../provider-incident-report.service';
import { ProviderPortalService } from '../../../provider-portal.service';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { Observable } from 'rxjs/Observable';
import { PaginationInfo, DropdownModel } from '../../../../@core/entities/common.entities';
import { ColumnSortedEvent } from '../../../../shared/modules/sortable-table/sort.service';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { ProviderPortalUrlConfig } from '../../../provider-portal-url.config';
export interface StaffInfoTable {
  firstName: string;
  lastName: string;
  affiliationType: string;
  jobTitle: string;
  employeeType: string;
  // provider_staff_id:string;

}
export interface YouthInfoTable {
  firstName: string;
  lastName: string;
  dob: string;
  identifierNo: string;
  admittingCharge: string;
  placing_agency: string;
  // provider_staff_id:string;

}
@Component({
  selector: 'uir-person',
  templateUrl: './uir-person.component.html',
  styleUrls: ['./uir-person.component.scss']
})
export class UirPersonComponent implements OnInit {
  PrecipitatingEventForm: FormGroup;
  incidentForm: FormGroup;
  youthForm: FormGroup;
  staffForm: FormGroup;
  fosterForm: FormGroup;
  affiliationType = [];
  employeeType = [];
  reasonForLeaving = [];
  rccCertificateType = [];
  showCertification: boolean;
  currentSelectedProviderStaffId: string;
  currentSelectedYouthId: string;
  staffInformation = [];
  youthInformation = [];
  assignedYouth = [];
  incidentInformation = [];
  fosterInformation = [];
  isEditStaff: boolean;
  isEndDate: boolean;
  isPhoneValid: boolean;
  currenbtStaffInfo = [];
  dataSource: any;
  youthDataSource: any;
  maxDate = new Date();
  displayedColumns: string[] = ['select', 'firstName', 'lastName', 'affiliationType', 'jobTitle', 'employeeType'];
  selection = new SelectionModel<StaffInfoTable>(true, []);
  staffArray: any[];
  assignedStaff: any[];
  displayedYouthColumns: string[] = ['select', 'firstName', 'lastName', 'placing_agency', 'dob', 'county', 'identifierNo', 'admittingCharge'];
  youthSelection = new SelectionModel<YouthInfoTable>(true, []);
  youthArray: any[];
  providerUirId: string;
  uirNo: string;
  staffFormInfo = [];
  providerId: string;
  user: AppUser;
  statusId: number;
  isDisabled: boolean;
  role: string;
  classType: string[] = ['Class I Incidents', 'Class II Incidents'];
  locationSource: any;
  locationSource1$: any;
  locationSource2$: any;
  class1IncidentSource: any[];
  class2IncidentSource: any[];
  class3IncidentSource: any[];
  providerClassIncident: any = [];
  class3Other: boolean;
  classIncidents: any[];
  @Input() isClass3Incident: Boolean = false;
  contactForm: FormGroup;
  lawEnforcementForm: FormGroup;
  contactList: any;
  lawEnforcementList: any;
  paginationInfo: PaginationInfo = new PaginationInfo();
  canDisplayPager$: Observable<boolean>;
  totalRecords$: Observable<number>;
  personSearchResult: any[];
  totalRecords: number;
  selectedCaseInfo: any;
  selectedPerson: any;
  searchCriteria: any;
  suggestedAddress$: Observable<any[]>;
  suggestedFosterAddress$: Observable<any[]>;
  stateList$: Observable<any[]>;
  countyList$: Observable<DropdownModel[]>;
  class3Type: string;
  precipitate: any;
  classMerge: any[] = [];
  class1Selected: any[] = [];
  class2Selected: any[] = [];
  class3Selected: any[] = [];
  class1IncidentSource$: any;
  class2IncidentSource$: any;
  isViewIncident: Boolean = false;
  isViewYouth: Boolean = false;
  isViewNotification: Boolean = false;
  isViewLawEnforcement: Boolean = false;
  isViewStaff: Boolean = false;
  isviewFoster: Boolean = false;
  isUpdate: Boolean = true;
  selectedStaffs: any;
  isEditable: Boolean = false;
  isIncidentExists: Boolean = true;
  constructor(private formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _router: Router, private route: ActivatedRoute,
    private _dataStore: DataStoreService,
    private _service: IncidentUirService,
    private _incidentService: IncidentUirService,
    private _providerIncident: ProviderIncidentReportService,
    private _providerPortalService: ProviderPortalService,
    private _authService: AuthService,
    private commonDropdownService: CommonDropdownsService,
    private _commonDropDownService: CommonDropdownsService,
  ) {

    const id = this._incidentService.provideruirid;
    const provider_uir_id = this._dataStore.getData('provider_uir_id');
    this.providerUirId = (id !== 'create') ? id : '';
    console.log(this._dataStore.getData('provider_uir_id'));
    console.log(this.providerUirId);
    if (this.providerUirId === '') {
      this.providerUirId = (provider_uir_id) ? provider_uir_id : '';
    }

    console.log(this.providerUirId);

    this.uirNo = this._incidentService.uirid;
    this.providerId = this._providerPortalService.getProviderId();
    this.user = this._authService.getCurrentUser();
    this.role = this.user.role.name;
    this.statusId = this._dataStore.getData('uir_status_type_id') ? parseInt(this._dataStore.getData('uir_status_type_id')) : null;
    if ((this.role === 'Provider_Staff_Admin' && this.statusId === 49) ||
      (this.role !== 'Provider_Staff_Admin' && this.statusId !== 49)) {
      this.isDisabled = true;
      this.isEditable = false;
    } else {
      this.isDisabled = false;
      this.isEditable = true;
    }
    //   if ((this.role === 'Provider_Staff_Admin' && this.statusId === 49) ) {
    // this.isEditable = true;
    // } else  {
    //   this.isEditable = false;
    // }
  }

  ngOnInit() {

    console.log(this.isClass3Incident);

    this.loadDropDowns();
    this.loadDropDownAddress();
    this.initializeIncidentForm();
    this.initializePrecipitatingForm();
    this.initializeYouthForm();
    this.initializeStaffForm();
    this.getYouthList();
    this.getClassIncidentDetails();
    this.getStaffInformation();
    if (this.providerUirId) {
      this.getYouthFosterInformation();
    }
    this.initializeContactForm();
    this.initializeLawEnforcementForm();
    this.affiliationType = ['Employee', 'Board Member', 'Volunteer', 'Intern Information', 'Temp Employee'];
    this.employeeType = ['Program Administrator', 'Residential Child and youth care practitioner', 'Other Employees' , 'N/A'];
    this.reasonForLeaving = ['Resigned', 'Retired', 'Death', 'Terminated'];
    this.rccCertificateType = ['Certified Program Administrator', 'Residential Child and youth care practitioner certification'];
    this.initializeFosterForm();
    this.getassignedstaff();
    this.getContactInformation();
    this.getLawEnforcementInformation();
  }

  private initializePrecipitatingForm(){
    this.PrecipitatingEventForm = this.formBuilder.group({
      precipitating_event: [null]
    });
  }

  private initializeIncidentForm() {
    this.incidentForm = this.formBuilder.group({
      incident_type: [null],
      incident_level_of_supervision: [null],
      incident_date: [null, Validators.required],
      incident_time: [null, Validators.required],
      discovered_date: [null],
      discovered_time: [null],
      incident_location: [null],
      incident_area: [null],
      incident_precipitating_event: [null],
      precipitating_event: [null],
      other_class3: [null],
      provider_uir_incident_detail_id: [null],
      isdeletable: null
    });
    this.classType = ['Class I Incidents', 'Class II Incidents'];
    if (this.isClass3Incident) {
      this.classType = ['Class I Incidents', 'Class II Incidents', 'Class III Incidents'];
      this.incidentForm.patchValue({ incident_type: 'Class III Incidents' });
      this.incidentForm.get('incident_type').disable();
    }
  }

  private initializeYouthForm() {
    this.youthForm = this.formBuilder.group({
      first_name: [null],
      last_name: [null],
      address1: [null],
      stateid: [''],
      address2: [''],
      zipcode: [''],
      city: [''],
      county: [''],
      // phone_no: [null],
      // gender: [null],
      dob: [null],
      placing_agency: [null],
      // youth_identifier: [null],
      // placing_agency: [null]
      identifier_no: [null],
      admitting_charge: [null],
      provider_uir_actor_detail_id: [null],
      isdeletable: null
    }, { validator: this.atLeastOne(Validators.required) });
  }
  atLeastOne = (validator: ValidatorFn) => (
    group: FormGroup,
  ): ValidationErrors | null => {
    const hasAtLeastOne = group && group.controls && Object.keys(group.controls)
      .some(k => !validator(group.controls[k]));

    return hasAtLeastOne ? null : {
      atLeastOne: true,
    };
  }

  private initializeStaffForm() {
    this.staffForm = this.formBuilder.group({
      object_id: this.providerId,
      first_nm: [''],
      middle_nm: [''],
      last_nm: [''],
      affiliation_type: [''],
      job_title: [''],
      employee_type: null,
      start_date: null,
      end_date: null,
      reason_for_leaving: [''],
      behavioral_interventions_training: null,
      cps_clearance_request_date: null,
      cps_clearance_result_date: null,
      cps_rcc_compliant: null,
      cps_cpa_compliant: null,
      federal_clearance_request_date: null,
      federal_clearance_result_date: null,
      federal_rcc_compliant: null,
      federal_cpa_compliant: null,
      state_clearance_request_date: null,
      state_clearance_result_date: null,
      state_rcc_compliant: null,
      state_cpa_compliant: null,
      rcc_certificate_type: null,
      rcc_certificate_due_date: null,
      rcc_date_application_mailed: null,
      rcc_date_tested: null,
      rcc_certification_effective_date: null,
      rcc_certification_number: [''],
      rcc_certification_renewal_date: null,
      staff_comment: [''],
      isdeletable: null
    });
  }

  private initializeContactForm() {
    this.contactForm = this.formBuilder.group({
      first_name: null,
      last_name: null,
      email: ['', [ValidationService.mailFormat]],
      phone_no: null,
      isdeletable: null,
      provider_uir_contact_detail_id: null
    });
  }

  private initializeLawEnforcementForm() {
    this.lawEnforcementForm = this.formBuilder.group({
      report_number: null,
      date: null,
      time: null,
      contact_first_name: null,
      contact_last_name: null,
      phone_no: null,
      isdeletable: null,
      provider_uir_id: null
    });
  }

  private initializeFosterForm() {
    this.fosterForm = this.formBuilder.group({
      first_name: null,
      last_name: null,
      address: null,
      phone_no: null,
      isdeletable: null,
      provider_uir_actor_detail_id: null
      // stateid: null,
      // address2: null,
      // zip: null,
      // city: null,
      // county: null
    });
  }

  getStaffInformation() {
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        where: { object_id: this.providerId },
        order: 'provider_staff_id desc'
      },
      'providerstaff?filter'
    ).subscribe(
      (response) => {
        this.staffInformation = response;
        this.dataSource = new MatTableDataSource<StaffInfoTable>(this.staffInformation);
      },
      (error) => {
        this._alertService.error('Unable to retrieve information');
      }
    );
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  assignStaff() {
    this.staffArray = this.selection.selected;
    const selectedStaffs = [];
    for (let i = 0; i < this.staffArray.length; i++) {
      selectedStaffs.push({ provider_staff_id: this.staffArray[i].provider_staff_id });
    }
    const payload = {};
    payload['applicant_id'] = this.uirNo; // saving uir number in applicant id column
    payload['staff'] = selectedStaffs;
    console.log('payload staff', payload);
    this._commonHttpService.create(
      payload,
      'providerstaff/assignstaff'
    ).subscribe(
      (response) => {
        this._alertService.success('Staffs assigned successfully!');
        this.getassignedstaff();
        (<any>$('#assign-staff')).modal('hide');
      },
      (error) => {
        this._alertService.error('Unable to assign staffs');
      });

    this._alertService.success('Staffs assigned successfully!');

  }

  assignAddedStaff(providerStaffId) {
    const payload = {};
    payload['applicant_id'] = this.uirNo; // saving uir number in applicant id column
    this.staffFormInfo[0].provider_staff_id = providerStaffId;
    console.log('this.staffFormInfo', this.staffFormInfo);
    payload['staff'] = this.staffFormInfo;
    console.log('payload staff', payload);
    this._commonHttpService.create(
      payload,
      'providerstaff/addapplicantstaff'
    ).subscribe(
      (response) => {
        this._alertService.success('Staffs assigned successfully!');
        this.getassignedstaff();
        this.resetStaffForm();
        (<any>$('#assign-staff')).modal('hide');
      },
      (error) => {
        this._alertService.error('Unable to assign staffs');
      });
    this._alertService.success('Staffs assigned successfully!');
  }

  getassignedstaff() {
    this._commonHttpService.create(
      {
        // where: { provider_applicant_id: this.applicantId },
        where: { provider_applicant_id: this.uirNo },
        method: 'post'
        // applicant_id:this.applicantNumber
      },
      'providerstaff/getassignedstaff'
    ).subscribe(response => {
      this.assignedStaff = response;
      console.log(JSON.stringify('assigned staff', this.assignedStaff));
      // console.log('rahul');
    },
      (error) => {
        this._alertService.error('Unable to get assigned staffs, please try again.');
        console.log('get contact Error', error);
        return false;
      }
    );
  }

  openAddStaffMembers() {
    this.isEditStaff = false;
    this.showCertification = false;
    this.isEndDate = false;
  }

  isEmployeeType(employeeType) {
    if (employeeType === 'Residential Child and youth care practitioner' || employeeType === 'Program Administrator') {
      this.showCertification = true;
    } else if (employeeType === 'N/A') {
      this.showCertification = false;
    }
  }

  editStaffForm(editStaff) {
    console.log(editStaff);
    this.currentSelectedProviderStaffId = editStaff.provider_staff_id;
    (<any>$('#add-staff')).modal('show');
    this.isEditStaff = true;
    this.staffForm.patchValue({
      object_id: this.providerId,
      first_nm: editStaff.first_nm,
      last_nm: editStaff.last_nm,
      affiliation_type: editStaff.affiliation_type,
      job_title: editStaff.job_title,
      employee_type: editStaff.employee_type,
      start_date: editStaff.start_date,
      end_date: editStaff.end_date,
      reason_for_leaving: editStaff.reason_for_leaving,
      behavioral_interventions_training: editStaff.behavioral_interventions_training,
      cps_clearance_request_date: editStaff.cps_clearance_request_date,
      cps_clearance_result_date: editStaff.cps_clearance_result_date,
      cps_rcc_compliant: editStaff.cps_rcc_compliant,
      cps_cpa_compliant: editStaff.cps_cpa_compliant,
      federal_clearance_request_date: editStaff.federal_clearance_request_date,
      federal_clearance_result_date: editStaff.federal_clearance_result_date,
      federal_rcc_compliant: editStaff.federal_rcc_compliant,
      federal_cpa_compliant: editStaff.federal_cpa_compliant,
      state_clearance_request_date: editStaff.state_clearance_request_date,
      state_clearance_result_date: editStaff.state_clearance_result_date,
      state_rcc_compliant: editStaff.state_rcc_compliant,
      state_cpa_compliant: editStaff.state_cpa_compliant,
      rcc_certificate_type: editStaff.rcc_certificate_type,
      rcc_certificate_due_date: editStaff.rcc_certificate_due_date,
      rcc_date_application_mailed: editStaff.rcc_date_application_mailed,
      rcc_date_tested: editStaff.rcc_date_tested,
      rcc_certification_effective_date: editStaff.rcc_certification_effective_date,
      rcc_certification_number: editStaff.rcc_certification_number,
      rcc_certification_renewal_date: editStaff.rcc_certification_renewal_date,
      staff_comment: editStaff.staff_comment

    });
  }

  resetStaffForm() {
    this.staffForm.reset();
  }

  resetYouthForm() {
    this.youthForm.reset();
  }

  resetIncidentForm() {
    this.incidentForm.reset();
    this.classMerge = [];
    if (this.isClass3Incident) {
      this.incidentForm.patchValue({ incident_type: 'Class III Incidents' });
      this.incidentForm.get('incident_type').disable();
    }
  }

  personnelEndDate() {
    this.isEndDate = true;
  }


  createProviderStaff() {
    console.log(this.staffForm.value);
    this._commonHttpService.create(
      this.staffForm.value,
      'providerstaff'
    ).subscribe(
      (response) => {
        this._alertService.success('Information saved, Please add clearance info!');
        this.currentSelectedProviderStaffId = response.provider_staff_id;
      },
      (error) => {
        this._alertService.error('Unable to save information');
      }
    );
  }

  patchProviderStaff() {
    // this.staffFormInfo= this.staffForm.value;
    // selectedstaffs.push({provider_staff_id:this.staffArray[i].provider_staff_id});
    this.staffFormInfo.push(this.staffForm.value);
    console.log('this.staffForm.value', this.staffForm.value);
    this._commonHttpService.patch(
      this.currentSelectedProviderStaffId,
      this.staffForm.value,
      'providerstaff'
    ).subscribe(
      (response) => {
        console.log('response staff', response.provider_staff_id);
        this.getStaffInformation();
        this.assignAddedStaff(response.provider_staff_id);
        this.resetStaffForm();
        this._alertService.success('Information saved successfully!');
      },
      (error) => {
        this._alertService.error('Unable to save information');
      }
    );
    (<any>$('#add-staff')).modal('hide');
  }

  confirmDeleteIncidentForm() {


    const incidentInfo = this.incidentForm.value;
    incidentInfo.uir_actor_type = 'incident';
    incidentInfo.uir_no = this._incidentService.uirid;
    incidentInfo.isdeletable = true;
    incidentInfo.precipitating_event = this.precipitate;
    incidentInfo.incident_type = (this.isClass3Incident) ? 'Class III Incidents' : incidentInfo.incident_type;
    if (this.providerUirId) {
      incidentInfo.provider_uir_id = this.providerUirId;
    }

    this._providerIncident.saveClass3IncidentDetails(incidentInfo).subscribe(response => {
      // this.getYouthFosterInformation();
      // this.resetFosterForm();
      // this._alertService.success('Information saved successfully!');
      if (response) {
        this._alertService.success(response);
        (<any>$('#delete-incident')).modal('hide');
        if (!this.providerUirId && response.provider_uir_id) {
          this.providerUirId = response.provider_uir_id;
          this._incidentService.provideruirid = this.providerUirId;

          // this._providerIncident.navigateTo(this.uirNo, this.providerUirId, 'person');
        } else {
          this.resetIncidentForm();
        }
        // this._alertService.success('Incident information deleted successfully!');
        this.getClassIncidentDetails();
      }
    });

  }

  patchIncidentForm() {
    if (!this.precipitate && !this.isClass3Incident) {
      this._alertService.error('Please Fill required fields');
      return;
    }
    const incidentInfo = this.incidentForm.value;
    incidentInfo.uir_actor_type = 'incident';
    incidentInfo.uir_no = this._incidentService.uirid;
    incidentInfo.precipitating_event = this.precipitate;
    incidentInfo.incident_type = (this.isClass3Incident) ? 'Class III Incidents' : incidentInfo.incident_type;
    if (this.providerUirId) {
      incidentInfo.provider_uir_id = this.providerUirId;
    }

    this._providerIncident.saveClass3IncidentDetails(incidentInfo).subscribe(response => {
      // this.getYouthFosterInformation();
      // this.resetFosterForm();
      // this._alertService.success('Information saved successfully!');
      if (response) {
        if (!this.providerUirId && response.provider_uir_id) {
          this.providerUirId = response.provider_uir_id;
          this._incidentService.provideruirid = this.providerUirId;
          // this._providerIncident.navigateTo(this.uirNo, this.providerUirId, 'person');
        } else {
          this.resetIncidentForm();
        }
        this._alertService.success('Incident information saved successfully!');
        this.getClassIncidentDetails();
      }
    });
    this.classMerge = [];
    (<any>$('#precipitate-event')).modal('hide');
    (<any>$('#add-incident')).modal('hide');
  }

  onChangeLocationType($event: MatRadioChange) {
    this.locationSource = ($event.value === '1') ? this.locationSource1$ : this.locationSource2$;
  }

  changeClass3Event(events) {
    if (events) {
      // const isOther = events.find(x => x === 'Other');
      this.class3Other = (events === 'Other') ? true : false;
    }
  }

  loadDropDowns() {
    Observable.forkJoin([
      this.commonDropdownService.getDropownsByTable('onsite_location_area', 'OLM'),
      this.commonDropdownService.getDropownsByTable('offsite_location_area', 'OLM'),
      this.commonDropdownService.getDropownsByTable('Class_I_Incidents', 'OLM'),
      this.commonDropdownService.getDropownsByTable('Class_II_Incidents', 'OLM'),
      this.commonDropdownService.getDropownsByTable('Class_III_Incidents', 'OLM'),
    ]).subscribe(
      ([locationSource1, locationSource2, class1IncidentSource, class2IncidentSource, class3IncidentSource]) => {
        this.locationSource1$ = locationSource1;
        this.locationSource2$ = locationSource2;
        // this.locationSource = (this.providerUirDetails) ? (this.providerUirDetails.location_incident === '1') ? this.locationSource1$ : this.locationSource2$ : this.locationSource1$;
        this.class1IncidentSource = class1IncidentSource;
        this.class2IncidentSource = class2IncidentSource;
        this.class3IncidentSource = class3IncidentSource;
        if (this.isClass3Incident) {
          this.classIncidents = this.class3IncidentSource;
        }

        const class1 = this.providerClassIncident && this.providerClassIncident.length ?
          this.providerClassIncident.filter((list) => list.class1_typekey != null).map((list) => list.class1_typekey) : [];
        const class2 = this.providerClassIncident && this.providerClassIncident.length ?
          this.providerClassIncident.filter((list) => list.class2_typekey != null).map((list) => list.class2_typekey) : [];
        const class3 = this.providerClassIncident && this.providerClassIncident.length ?
          this.providerClassIncident.filter((list) => list.class3_typekey != null).map((list) => list.class3_typekey) : [];
        // const class3 = this.providerClassIncident.filter((list) => list.class3_typekey != null).map((list) => list.class3_typekey);

        const class1Selected = (class1) ? this.providerClassIncident && this.providerClassIncident.length ? this.class1IncidentSource.filter(item => (class1.indexOf(item.ref_key) !== -1)) : [] : [];
        const class2Selected = (class2) ? this.providerClassIncident && this.providerClassIncident.length ? this.class2IncidentSource.filter(item => (class2.indexOf(item.ref_key) !== -1)) : [] : [];
        const class3Selected = (class2) ? this.providerClassIncident && this.providerClassIncident.length ? this.class3IncidentSource.filter(item => (class3.indexOf(item.ref_key) !== -1)) : [] : [];
        // const class3Selected = (class1) ? this.class3IncidentSource.filter(item => (class3.indexOf(item.ref_key) !== -1)) : [];
        const classMerge = [...class1Selected, ...class2Selected, ...class3Selected];
        console.log('Class merge array----->', classMerge);
        const isOther = classMerge.findIndex(x => x.class3_typekey === 'Other');
        this.class3Other = (isOther !== -1) ? true : false;
      });
  }

  // onChange(event) {
  //   console.log(event);
  //   const class3 = this.incidentForm.getRawValue().class3_typekey;
  //   let class3Selected = [];
  //   if (event.option.selected) {
  //     class3Selected = (class3) ? this.class3IncidentSource.filter(item => (class3.indexOf(item.ref_key) !== -1)) : [];
  //   } else {
  //     const itemIndex = class3Selected.findIndex(x => x.class3_typekey === event.option.value);
  //     class3Selected.splice(itemIndex, 1);
  //   }
  //   const isOther = class3Selected.findIndex(x => x.ref_key === 'Other');
  //   this.class3Other = (isOther !== -1) ? true : false;
  // }

  onChange(event) {
    const classType = this.incidentForm.getRawValue().incident_type;
    if (event.isUserInput) {
      this.class1Selected = [];
      this.class2Selected = [];
      switch (classType) {
        case 'Class I Incidents':
          if (event.source.selected) {
            this.class1Selected = this.class1IncidentSource.find(item => (item.ref_key === event.source.value));
            this.classMerge.push(this.class1Selected);
          } else {
            const itemIndex = (this.classMerge.length) ? this.classMerge.findIndex(x => x.class1_typekey === event.source.value) : '';
            if (itemIndex) {
              this.classMerge.splice(itemIndex, 1);
            }
          }
          break;
        case 'Class II Incidents':
          if (event.source.selected) {
            this.class2Selected = this.class2IncidentSource.find(item => (item.ref_key === event.source.value));
            this.classMerge.push(this.class2Selected);
          } else {
            const itemIndex = (this.classMerge.length) ? this.classMerge.findIndex(x => x.class2_typekey === event.source.value) : '';
            if (itemIndex) {
              this.classMerge.splice(itemIndex, 1);
            }
          }
          break;
        case 'Class III Incidents':
          if (event.source.selected) {
            this.class3Selected = this.class3IncidentSource.find(item => (item.ref_key === event.source.value));
            this.classMerge.push(this.class3Selected);
          } else {
            const itemIndex = (this.classMerge.length) ? this.classMerge.findIndex(x => x.class3_typekey === event.source.value) : '';
            if (itemIndex) {
              this.classMerge.splice(itemIndex, 1);
            }
          }
          if (event.source.selected) {
            this.class3Other = (event.source.value === 'Other') ? true : false;
          }
          break;
      }
      if (classType !== 'Class III Incidents') {
        const isOther = this.classMerge.findIndex(x => x.ref_key === 'Other');
        this.class3Other = (isOther !== -1) ? true : false;
      }
    }
  }

  getClassIncidentDetails() {
    const classParam = {
      where: { provider_uir_id: this.providerUirId },
      method: 'get',
      count: -1
    };
    this._providerIncident.getClass3IncidentDetails(classParam).subscribe((response) => {
      // if (response) {
      this.providerClassIncident = response;
        this.isIncidentExists = ( response && response.length ) ? true : false;
      // setTimeout(() => { this.loadFormValues(); }, 800);
      // }
    });
  }

  getClassIncidentByType(type) {
    if (type === 'Class I Incidents') {
      this.classIncidents = this.class1IncidentSource;
    } else if (type === 'Class II Incidents') {
      this.classIncidents = this.class2IncidentSource;
    } else {
      this.classIncidents = this.class3IncidentSource;
    }
  }

  patchYouthForm() {
    const youthInfo = this.youthForm.value;
    youthInfo.uir_actor_type = 'youth';
    console.log(this._incidentService.uirid);
    youthInfo.uir_no = this._incidentService.uirid;
    youthInfo.object_id = this.providerId;
    if (this.providerUirId) {
      youthInfo.provider_uir_id = this.providerUirId;
    }

    // if (this.isEditWitness) {
    //   witnessData.provider_uir_witness_id = this.currentSelectedWitnessId;
    // }

    this._providerIncident.saveYouthFosterDetails(youthInfo).subscribe(response => {
      if (response) {
        console.log(response.provider_uir_id);
        this._dataStore.setData('provider_uir_id', response.provider_uir_id);
        console.log(this._dataStore.getData('provider_uir_id'));
        if (!this.providerUirId) {
          this.providerUirId = response.provider_uir_id;
          this._incidentService.provideruirid = this.providerUirId;
          // this._providerIncident.navigateTo(this.uirNo, this.providerUirId, 'person');
        } else {
          this.resetYouthForm();
        }
        this.getYouthList();
      }
      this._alertService.success('Information saved successfully!');
      this.searchCriteria = {};
      this.paginationInfo.pageNumber = 1;
    });
    (<any>$('#add-youth')).modal('hide');
  }

  patchFosterForm() {
    const fosterInfo = this.fosterForm.value;
    fosterInfo.uir_actor_type = 'foster';
    fosterInfo.uir_no = this._incidentService.uirid;
    if (this.providerUirId) {
      fosterInfo.provider_uir_id = this.providerUirId;
    }

    this._providerIncident.saveYouthFosterDetails(fosterInfo).subscribe(response => {
      // this.getYouthFosterInformation();
      // this.resetFosterForm();
      // this._alertService.success('Information saved successfully!');
      if (response) {
        if (!this.providerUirId && response.provider_uir_id) {
          this.providerUirId = response.provider_uir_id;
          this._incidentService.provideruirid = this.providerUirId;
          // this._providerIncident.navigateTo(this.uirNo, this.providerUirId, 'person');
        } else {
          this.resetFosterForm();
        }
        this._alertService.success('Information saved successfully!');
        this.getYouthFosterInformation();
      }
    });
    (<any>$('#add-foster')).modal('hide');
  }

  addYouthshow() {
    this.searchCriteria = {};
    this.isUpdate = false;
    this.paginationInfo.pageNumber = 1;
    (<any>$('#add-youth')).modal('show');
  }

  getYouthFosterInformation() {
    const youthInvolvedParam = {
      where: { provider_uir_id: this.providerUirId },
      method: 'get',
      count: -1
    };
    this._providerIncident.getYouthFosterData(youthInvolvedParam).subscribe(response => {
      // if (response) {
      // this.incidentInformation = response.filter(person => person.uir_actor_type === 'incident');
      // this.youthInformation = response.filter(person => person.uir_actor_type === 'youth');
      this.fosterInformation = response && response.length ? response.filter(person => person.uir_actor_type === 'foster') : [];
      // }
    });

    // const providerIdKeyVal = (this.providerUirId) ? { provider_uir_id: this.providerUirId } : {};
    // this._commonHttpService.getArrayList(
    //   {
    //     method: 'get',
    //     nolimit: true,
    //     where: providerIdKeyVal,
    //   },
    //   'provider_uir_actor_detail/list?filter'
    // ).subscribe(
    //   (response) => {
    //     if (response) {
    //       this.youthInformation = response.filter(person => person.uir_actor_type === 'youth');
    //       this.fosterInformation = response.filter(person => person.uir_actor_type === 'foster');
    //     }
    //   },
    //   (error) => {
    //     this._alertService.error('Unable to retrieve information');
    //   });
  }

  getYouthList() {
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        where: { uir_no: this.uirNo }
      },
      ProviderPortalUrlConfig.EndPoint.Uir.GET_YOUTH_LIST
    ).subscribe(
      (response) => {
        this.youthInformation = response;
      },
      (error) => {
        this._alertService.error('Unable to retrieve information');
      }
    );
  }


  getAssignedYouthList() {
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        where: { object_id: this.providerId }
      },
      ProviderPortalUrlConfig.EndPoint.Uir.GET_ASSIGED_YOUTH_LIST
    ).subscribe(
      (response) => {
        this.assignedYouth = response;
        this.youthDataSource = new MatTableDataSource<StaffInfoTable>(this.assignedYouth);
      },
      (error) => {
        this._alertService.error('Unable to retrieve information');
      }
    );
  }

  isAllYouthSelected() {
    const numSelected = this.youthSelection.selected.length;
    const numRows = this.youthDataSource.data.length;
    return numSelected === numRows;
  }

  masterToggleYouth() {
    this.isAllYouthSelected() ?
      this.youthSelection.clear() :
      this.youthDataSource.data.forEach(row => this.youthSelection.select(row));
  }

  assignYouth() {
    this.youthArray = this.youthSelection.selected;
    const selectedYouths = [];
    // for (let i = 0; i < this.youthArray.length; i++) {
    this.youthArray.forEach((item) => {
      selectedYouths.push({ provider_youth_id: item.client_id });
    });
    const payload = {};
    payload['provider_uir_id'] = this.providerUirId;
    payload['provider_id'] = this.providerId;
    payload['uir_no'] = this.uirNo;
    payload['youth'] = selectedYouths;
    console.log('payload youth', payload);
    this._commonHttpService.create(
      payload,
      ProviderPortalUrlConfig.EndPoint.Uir.ASSIGN_YOUTH_LIST
    ).subscribe(
      (response) => {
        this._alertService.success('Youth assigned successfully!');
        this.getYouthList();
        (<any>$('#assign-youth')).modal('hide');
      },
      (error) => {
        this._alertService.error('Unable to assign youth');
      });

    this._alertService.success('Youth assigned successfully!');

  }


  deleteStaffMember(staffInfo) {
    this.selectedStaffs = staffInfo;
    (<any>$('#delete-staff')).modal('show');
  }

  confirmDeleteStaffMember() {

    const payload = {};
    payload['applicant_id'] = this.uirNo; // saving uir number in applicant id column
    payload['isdeletable'] = true;
    payload['provider_staff_config_id'] = this.selectedStaffs ? this.selectedStaffs.provider_staff_config_id : null;
    payload['staff'] = [this.selectedStaffs];
    console.log('payload staff', payload);
    this._commonHttpService.create(
      payload,
      'providerstaff/assignstaff'
    ).subscribe(
      (response) => {
        this._alertService.success(response);
        this.getassignedstaff();
        (<any>$('#delete-staff')).modal('hide');
      },
      (error) => {
        this._alertService.error('Unable to delete staffs');
      });



  }


  assignAddedYouth(providerStaffId) {
    const payload = {};
    payload['applicant_id'] = this.uirNo; // saving uir number in applicant id column
    this.staffFormInfo[0].provider_staff_id = providerStaffId;
    console.log('this.staffFormInfo', this.staffFormInfo);
    payload['staff'] = this.staffFormInfo;
    console.log('payload staff', payload);
    this._commonHttpService.create(
      payload,
      'providerstaff/addapplicantstaff'
    ).subscribe(
      (response) => {
        this._alertService.success('Staffs assigned successfully!');
        this.getassignedstaff();
        this.resetStaffForm();
        (<any>$('#assign-staff')).modal('hide');
      },
      (error) => {
        this._alertService.error('Unable to assign staffs');
      });
    this._alertService.success('Staffs assigned successfully!');
  }
  deleteStaffForm(staffInfo) {
    this.currentSelectedProviderStaffId = staffInfo.provider_staff_id;
    (<any>$('#delet-staff')).modal('show');
  }

  confirmDelete() {
    this._commonHttpService.remove(
      this.currentSelectedProviderStaffId,
      {
        where: { provider_staff_id: this.currentSelectedProviderStaffId }
      },
      'providerstaff').subscribe(
        (response) => {
          this.getStaffInformation();
        },
        (error) => {
          this._alertService.error('Unable to delete Staff');
          return false;
        }
      );
    (<any>$('#delet-staff')).modal('hide');
  }

  phoneValidation(event: any) {
    this.isPhoneValid = false;
    const pattern = /^\d{10}$/;
    if (!pattern.test(event.target.value)) {
      this.isPhoneValid = false;
    } else {
      this.isPhoneValid = true;
    }
  }

  resetFosterForm() {
    this.fosterForm.reset();
  }

  patchContactForm() {
    const contactInfo = this.contactForm.value;
    contactInfo.uir_no = this._incidentService.uirid;

    if (this.providerUirId) {
      contactInfo.provider_uir_id = this.providerUirId;
    }

    this._providerIncident.saveContactDetails(contactInfo).subscribe(response => {
      if (response) {
        if (!this.providerUirId) {
          this.providerUirId = response.provider_uir_id;
          this._incidentService.provideruirid = this.providerUirId;
        } else {
          this.resetContactForm();
        }
        this.getContactInformation();
      }
      this._alertService.success('Contact Saved Successfully!');
    });
    (<any>$('#add-contact')).modal('hide');
  }

  resetContactForm() {
    this.contactForm.reset();
  }

  getContactInformation() {
    const contactParam = {
      where: { provider_uir_id: this.providerUirId },
      method: 'get',
      count: -1
    };
    this._providerIncident.getContactData(contactParam).subscribe(response => {
      if (response && response.length) {
        this.contactList = response[0].provider_uir_contact_detail;
      }
    });
  }

  patchLawEnforcementForm() {
    const lawEnforcementInfo = this.lawEnforcementForm.value;
    lawEnforcementInfo.uir_no = this._incidentService.uirid;

    if (this.providerUirId) {
      lawEnforcementInfo.provider_uir_id = this.providerUirId;
    }

    this._providerIncident.saveLawEnforcementDetails(lawEnforcementInfo).subscribe(response => {
      if (response) {
        if (!this.providerUirId) {
          this.providerUirId = response.provider_uir_id;
          this._incidentService.provideruirid = this.providerUirId;
        } else {
          this.resetLawEnforcementForm();
        }
        this.getLawEnforcementInformation();
      }
      this._alertService.success('Law Enforcement details Saved Successfully!');
    });
    (<any>$('#add-law-enforcement')).modal('hide');
  }

  resetLawEnforcementForm() {
    this.lawEnforcementForm.reset();
  }

  getLawEnforcementInformation() {
    const lawEnforcementParam = {
      where: { provider_uir_id: this.providerUirId },
      method: 'get',
      count: -1
    };
    this._providerIncident.getLawEnforcementData(lawEnforcementParam).subscribe(response => {
      // if (response && response.length) {
      this.lawEnforcementList = response;
      // }
    });
  }
  searchPerson() {
    this.searchCriteria = {};
    if (this.youthForm.valid) {
      if (this.youthForm.value.identifier_no || this.youthForm.value.admitting_charge) {
        this.personSearchResult = [];
      } else {
        if (this.youthForm.value.first_name) {
          this.searchCriteria['firstname'] = this.youthForm.value.first_name;
        }
        if (this.youthForm.value.last_name) {
          this.searchCriteria['lastname'] = this.youthForm.value.last_name;
        }
        if (this.youthForm.value.address1) {
          this.searchCriteria['address1'] = this.youthForm.value.address1;
        }
        if (this.youthForm.value.address2) {
          this.searchCriteria['address2'] = this.youthForm.value.address2;
        }
        if (this.youthForm.value.stateid) {
          this.searchCriteria['stateid'] = this.youthForm.value.stateid;
        }
        if (this.youthForm.value.zipcode) {
          this.searchCriteria['zip'] = this.youthForm.value.zipcode;
        }
        if (this.youthForm.value.city) {
          this.searchCriteria['city'] = this.youthForm.value.city;
        }
        if (this.youthForm.value.county) {
          this.searchCriteria['county'] = this.youthForm.value.county;
        }
        if (this.youthForm.value.dob) {
          this.searchCriteria['dob'] = this.youthForm.value.dob;
        }
        // this.searchCriteria['phone'] = this.youthForm.value.phone_no;
        // this.searchCriteria['gender'] = this.youthForm.value.gender;
        this.searchCriteria['sortorder'] = 'asc';
        this.searchCriteria['sortcolumn'] = null;
        this.getPage(1);
      }
      (<any>$('#add-youth')).modal('hide');
      (<any>$('#youth-search')).modal('show');
    }
  }
  onSortedPerson($event: ColumnSortedEvent) {
    this.searchCriteria['sortorder'] = $event.sortDirection;
    this.searchCriteria['sortcolumn'] = $event.sortColumn;
    this.paginationInfo.pageNumber = 1;
    this.getPage(1);
  }
  getPage(pageNumber: number) {
    this.selectedPerson = null;
    const source = this._service.searchPerson(this.searchCriteria, pageNumber, this.paginationInfo)
      .map((result) => {
        return {
          data: result.data,
          count: result.count,
          canDisplayPager: result.count > this.paginationInfo.pageSize
        };
      }).subscribe(response => {
        this.personSearchResult = response.data;
        if (pageNumber === 1) {
          this.totalRecords = response.count;
        }
      });
  }
  pageChanged(event: any) {
    this.paginationInfo.pageNumber = event.page;
    this.getPage(this.paginationInfo.pageNumber);
  }
  selectPerson(row) {
    this.selectedPerson = row;
  }
  addNewYouth() {
    this.youthForm.reset();
    this.searchCriteria = {};
    (<any>$('#youth-search')).modal('hide');
    (<any>$('#add-youth')).modal('show');
    this.paginationInfo.pageNumber = 1;
  }
  editSelectedPerson() {
    console.log(this.selectedPerson);
    if (this.selectedPerson) {
      (<any>$('#youth-search')).modal('hide');
      (<any>$('#add-youth')).modal('show');
      this.youthForm.patchValue({
        first_name: this.selectedPerson.firstname,
        last_name: this.selectedPerson.lastname,
        address1: this.selectedPerson.address,
        stateid: this.selectedPerson.state,
        address2: this.selectedPerson.address2,
        zipcode: this.selectedPerson.zipcode,
        city: this.selectedPerson.city,
        county: this.selectedPerson.county,
        dob: this.selectedPerson.dob,
        provider_uir_actor_detail_id: this.selectedPerson.dob.provider_uir_actor_detail_id,
      });
    }
  }
  goBack() {
    this.youthForm.reset();
    this.searchCriteria = {};
    (<any>$('#youth-search')).modal('hide');
    (<any>$('#add-youth')).modal('show');
    this.paginationInfo.pageNumber = 1;
  }
  getSuggestedAddress() {
    this.suggestAddress();
  }
  suggestAddress() {
    this._commonHttpService
      .getArrayListWithNullCheck(
        {
          method: 'post',
          where: {
            prefix: this.youthForm.value.address1,
            cityFilter: '',
            stateFilter: '',
            geolocate: '',
            geolocate_precision: '',
            prefer_ratio: 0.66,
            suggestions: 25,
            prefer: 'MD'
          }
        },
        'People/suggestaddress'
      ).subscribe(
        (result: any) => {
          if (result.length > 0) {
            this.suggestedAddress$ = result;
          }
        }
      );
  }
  selectedAddress(model) {
    this.youthForm.patchValue({
      address1: model.streetLine ? model.streetLine : '',
      city: model.city ? model.city : '',
      stateid: model.state ? model.state : ''
    });
    const addressInput = {
      street: model.streetLine ? model.streetLine : '',
      street2: '',
      city: model.city ? model.city : '',
      state: model.state ? model.state : '',
      zipcode: '',
      match: 'invalid'
    };
    this._commonHttpService
      .getSingle(
        {
          method: 'post',
          where: addressInput
        },
        'People/validateaddress'
      )
      .subscribe(
        (result) => {
          if (result[0].analysis) {
            this.youthForm.patchValue({
              zipcode: result[0].components.zipcode ? result[0].components.zipcode : ''
            });
            if (result[0].metadata.countyName) {
              this._commonHttpService.getArrayList(
                {
                  nolimit: true,
                  where: { referencetypeid: 306, mdmcode: this.youthForm.value.stateid, description: result[0].metadata.countyName }, method: 'get'
                },
                'referencevalues?filter'
              ).subscribe(
                (resultresp) => {
                  if (resultresp && resultresp.length) {
                    this.youthForm.patchValue({
                      county: resultresp[0].ref_key
                    });
                  }
                }
              );
            }
          }
        },
        (error) => {
          console.log(error);
        }
      );
  }
  getFosterSuggestedAddress() {
    this.suggestFosterAddress();
  }
  suggestFosterAddress() {
    this._commonHttpService
      .getArrayListWithNullCheck(
        {
          method: 'post',
          where: {
            prefix: this.fosterForm.value.address,
            cityFilter: '',
            stateFilter: '',
            geolocate: '',
            geolocate_precision: '',
            prefer_ratio: 0.66,
            suggestions: 25,
            prefer: 'MD'
          }
        },
        'People/suggestaddress'
      ).subscribe(
        (result: any) => {
          if (result.length > 0) {
            this.suggestedFosterAddress$ = result;
          }
        }
      );
  }
  selectedFosterAddress(model) {
    this.fosterForm.patchValue({
      address: model.streetLine ? model.streetLine : '',
      city: model.city ? model.city : '',
      stateid: model.state ? model.state : ''
    });
    const addressInput = {
      street: model.streetLine ? model.streetLine : '',
      street2: '',
      city: model.city ? model.city : '',
      state: model.state ? model.state : '',
      zipcode: '',
      match: 'invalid'
    };
    this._commonHttpService
      .getSingle(
        {
          method: 'post',
          where: addressInput
        },
        'People/validateaddress'
      )
      .subscribe(
        (result) => {
          if (result[0].analysis) {
            this.fosterForm.patchValue({
              zip: result[0].components.zipcode ? result[0].components.zipcode : ''
            });
            if (result[0].metadata.countyName) {
              this._commonHttpService.getArrayList(
                {
                  nolimit: true,
                  where: { referencetypeid: 306, mdmcode: this.fosterForm.value.stateid, description: result[0].metadata.countyName }, method: 'get'
                },
                'referencevalues?filter'
              ).subscribe(
                (resultresp) => {
                  if (resultresp && resultresp.length) {
                    this.fosterForm.patchValue({
                      county: resultresp[0].ref_key
                    });
                  }
                }
              );
            }
          }
        },
        (error) => {
          console.log(error);
        }
      );
  }

  loadDropDownAddress() {
    // const source = forkJoin([
    //    this._commonHttpService.getArrayList(
    //      {
    //          method: 'get',
    //          nolimit: true
    //      },
    //      'States?filter'
    //  ),
    //  this._commonHttpService.getArrayList('get', 'providerreferral/listcountycodes'),
    // ])
    //   .map((result) => {
    //     return {
    //       stateList: result[0].map(
    //         (res) =>
    //             new DropdownModel({
    //                 text: res.statename,
    //                 value: res.stateabbr
    //             })
    //     ),
    //     countyListitems: result[1].map(
    //       (res) =>
    //           new DropdownModel({
    //               text: res.value_tx,
    //               value: res.picklist_value_cd
    //           })
    //   ),

    //     };
    //   })
    //   .share();
    this.stateList$ = this.commonDropdownService.getPickListByName('state');
    this.countyList$ = this.commonDropdownService.getPickList('328');
  }

  formatPhoneNumber(phoneNumber: string) {
    return this._commonDropDownService.formatPhoneNumber(phoneNumber);
  }

  selectPrecipitaeEvent() {
    (<any>$('#add-incident')).modal('hide');
    (<any>$('#precipitate-event')).modal('show');
  }

  closeDeleteModal() {
    (<any>$('#precipitate-event')).modal('hide');
  }

  onChangePrecipitate(item) {
    this.precipitate = item.ref_key;
  }

  getEventMultiple(incident_type, event, index) {
    /*if (incident_type === 'Class III Incidents') {
      const class3Incident = (this.class3IncidentSource) ? this.class3IncidentSource.find(item => item.ref_key === event) : '';
      return (class3Incident) ? class3Incident.description : '';
    }*/
    const events = (event) ? event.split(',') : '';
    let incident = [];
    if (incident_type === 'Class I Incidents') {
      incident = (this.class1IncidentSource) ? this.class1IncidentSource.filter(item => events.includes(item.ref_key)) : [];
    } else if (incident_type === 'Class II Incidents') {
      incident = (this.class2IncidentSource) ? this.class2IncidentSource.filter(item => events.includes(item.ref_key)) : [];
    } else if (incident_type === 'Class III Incidents') {
      incident = (this.class3IncidentSource) ? this.class3IncidentSource.filter(item => events.includes(item.ref_key)) : [];
    }
    const incidents = (incident.length) ? incident.map(item => item.description) : [];
    const incidentItem = document.getElementById('incident' + index);
    if (incidentItem.hasChildNodes()) {
      while (incidentItem.firstChild) {
        incidentItem.removeChild(incidentItem.firstChild);
      }
    }
    incidents.forEach((element) => {
      const container = document.createElement('p');
      container.innerHTML = element;
      const node = container.cloneNode(true);
      document.getElementById('incident' + index).appendChild(node);
    });
    return true;
  }

  getEvent(incident_type, event) {
    if (incident_type === 'Class I Incidents') {
      const incident = (this.class1IncidentSource) ? this.class1IncidentSource.find(item => item.ref_key === event) : '-';
      return (incident) ? incident.description : '-';
    } else if (incident_type === 'Class II Incidents') {
      const incident = (this.class2IncidentSource) ? this.class2IncidentSource.find(item => item.ref_key === event) : '-';
      return (incident) ? incident.description : '-';
    } else if (incident_type === 'Class III Incidents') {
      const incident = (this.class3IncidentSource) ? this.class3IncidentSource.find(item => item.ref_key === event) : '-';
      return (incident) ? incident.description : '-';
    }
    return '-';
  }

  manageIncident(incident, isView) {
    this.isViewIncident = (isView) ? true : false;
    this.isUpdate = true;
    this.getClassIncidentByType(incident.incident_type);
    this.locationSource = (incident.incident_location === '1') ? this.locationSource1$ : this.locationSource2$;
    this.precipitate = incident.precipitating_event;
    (<any>$('#add-incident')).modal('show');
    this.incidentForm.patchValue({
      incident_type: incident.incident_type,
      incident_date: incident.incident_date,
      incident_time: incident.incident_time,
      discovered_date: incident.discovered_date,
      discovered_time: incident.discovered_time,
      incident_location: incident.incident_location,
      incident_area: incident.incident_area,
      incident_level_of_supervision: incident.incident_level_of_supervision,
      incident_precipitating_event: (incident.incident_precipitating_event) ? incident.incident_precipitating_event.split(',') : [],
      precipitating_event: incident.precipitating_event,
      provider_uir_incident_detail_id: incident.provider_uir_incident_detail_id
    });

    if (isView) {
      this.incidentForm.disable();
    } else {
      this.incidentForm.enable();
    }
  }

  deleteIncident(incident, i) {
    // this.providerClassIncident.splice(i,1);
    // console.log(incident);
    this.getClassIncidentByType(incident.incident_type);
    this.locationSource = (incident.incident_location === '1') ? this.locationSource1$ : this.locationSource2$;
    this.precipitate = incident.precipitating_event;
    this.incidentForm.patchValue({
      incident_type: incident.incident_type,
      incident_date: incident.incident_date,
      incident_time: incident.incident_time,
      discovered_date: incident.discovered_date,
      discovered_time: incident.discovered_time,
      incident_location: incident.incident_location,
      incident_area: incident.incident_area,
      incident_level_of_supervision: incident.incident_level_of_supervision,
      incident_precipitating_event: (incident.incident_precipitating_event) ? incident.incident_precipitating_event.split(',') : [],
      precipitating_event: incident.precipitating_event,
      provider_uir_incident_detail_id: incident.provider_uir_incident_detail_id
    });
    (<any>$('#delete-incident')).modal('show');


  }
  addIncident() {
    this.incidentForm.enable();
    this.isUpdate = false;
    if (this.isClass3Incident) {
      this.incidentForm.get('incident_type').disable();
    }
    this.isViewIncident = false;
  }
  viewYouth(person) {
    console.log(person);
    this.isViewYouth = true;
    (<any>$('#add-youth')).modal('show');
    this.youthForm.patchValue({
      first_name: person.first_name,
      last_name: person.last_name,
      dob: person.dob,
      placing_agency: person.placing_agency,
      identifier_no: person.identifier_no,
      admitting_charge: person.admitting_charge,
    });
    this.youthForm.disable();
  }
  editYouth(person) {
    this.isViewYouth = false;
    this.isUpdate = true;
    (<any>$('#add-youth')).modal('show');
    this.youthForm.patchValue({
      first_name: person.first_name,
      last_name: person.last_name,
      placing_agency: person.placing_agency,
      dob: person.dob,
      identifier_no: person.identifier_no,
      admitting_charge: person.admitting_charge,
      provider_uir_actor_detail_id: person.provider_uir_actor_detail_id
    });
    this.youthForm.enable();
  }
  deleteYouth(person) {
    this.youthForm.patchValue({
      first_name: person.first_name,
      last_name: person.last_name,
      placing_agency: person.placing_agency,
      dob: person.dob,
      identifier_no: person.identifier_no,
      admitting_charge: person.admitting_charge,
      provider_uir_actor_detail_id: person.provider_uir_actor_detail_id,
      isdeletable: true
    });
    (<any>$('#delete-youth')).modal('show');
  }

  confirmDeleteYouth() {
    const youthInfo = this.youthForm.value;
    youthInfo.uir_actor_type = 'youth';
    youthInfo.uir_no = this._incidentService.uirid;
    youthInfo.object_id = this.providerId;
    if (this.providerUirId) {
      youthInfo.provider_uir_id = this.providerUirId;
    }


    this._providerIncident.saveYouthFosterDetails(youthInfo).subscribe(response => {
      if (response) {
        this._alertService.success(response);
        this.resetYouthForm();
        this.getYouthList();
      }
    });
    (<any>$('#delete-youth')).modal('hide');
  }

  resetNotificationForm() {
    this.isViewNotification = false;
    this.isUpdate = false;
    this.contactForm.enable();
    this.contactForm.reset();
  }

  viewNotification(contact) {
    console.log(contact);
    this.isViewNotification = true;
    (<any>$('#add-contact')).modal('show');
    this.contactForm.patchValue({
      first_name: contact.firstname,
      last_name: contact.lastname,
      email: contact.email,
      phone_no: contact.phonenumber,
    });
    this.contactForm.disable();
  }
  editNotification(contact) {
    console.log(contact);
    this.isViewNotification = false;
    this.isUpdate = true;
    (<any>$('#add-contact')).modal('show');
    this.contactForm.patchValue({
      first_name: contact.firstname,
      last_name: contact.lastname,
      email: contact.email,
      phone_no: contact.phonenumber,
      provider_uir_contact_detail_id: contact.provider_uir_contact_detail_id
    });
    this.contactForm.enable();
  }
  deleteNotification(contact) {
    (<any>$('#delete-contact')).modal('show');
    this.contactForm.patchValue({
      first_name: contact.firstname,
      last_name: contact.lastname,
      email: contact.email,
      phone_no: contact.phonenumber,
      isdeletable: true,
      provider_uir_contact_detail_id: contact.provider_uir_contact_detail_id
    });

  }

  confirmDeleteNotification() {
    const contactInfo = this.contactForm.value;
    contactInfo.uir_no = this._incidentService.uirid;

    if (this.providerUirId) {
      contactInfo.provider_uir_id = this.providerUirId;
    }

    this._providerIncident.saveContactDetails(contactInfo).subscribe(response => {
      if (response) {
        this._alertService.success(response);
        this.resetContactForm();
        this.getContactInformation();
      }

    });
    (<any>$('#delete-contact')).modal('hide');
  }

  resetLaw() {
    this.isViewLawEnforcement = false;
    this.isUpdate = false;
    this.lawEnforcementForm.enable();
    this.lawEnforcementForm.reset();
  }

  viewLaw(item) {
    console.log(item);
    this.isViewLawEnforcement = true;
    (<any>$('#add-law-enforcement')).modal('show');
    this.lawEnforcementForm.patchValue({
      report_number: item.report_number,
      date: item.date,
      time: item.time,
      contact_first_name: item.contact_first_name,
      contact_last_name: item.contact_last_name,
      phone_no: item.phone_no,
    });
    this.lawEnforcementForm.disable();
  }

  editLaw(item) {
    this.isViewLawEnforcement = false;
    this.isUpdate = true;
    (<any>$('#add-law-enforcement')).modal('show');
    this.lawEnforcementForm.patchValue({
      report_number: item.report_number,
      date: item.date,
      time: item.time,
      contact_first_name: item.contact_first_name,
      contact_last_name: item.contact_last_name,
      phone_no: item.phone_no,
      provider_uir_id: item.provider_uir_id
    });
    this.lawEnforcementForm.enable();
  }

  deleteLaw(item) {

    this.lawEnforcementForm.patchValue({
      report_number: item.report_number,
      date: item.date,
      time: item.time,
      contact_first_name: item.contact_first_name,
      contact_last_name: item.contact_last_name,
      phone_no: item.phone_no,
      isdeletable: true
    });
    (<any>$('#delete-law')).modal('show');
  }

  confirmDeleteLaw() {
    const lawEnforcementInfo = this.lawEnforcementForm.value;
    lawEnforcementInfo.uir_no = this._incidentService.uirid;

    if (this.providerUirId) {
      lawEnforcementInfo.provider_uir_id = this.providerUirId;
    }

    this._providerIncident.saveLawEnforcementDetails(lawEnforcementInfo).subscribe(response => {
      if (response) {
        this._alertService.success(response);
        this.resetLawEnforcementForm();
        this.getLawEnforcementInformation();
      }

    });
    (<any>$('#delete-law')).modal('hide');
  }

  viewStaff(person) {
    this.isViewStaff = true;
    (<any>$('#add-staff')).modal('show');
    this.staffForm.patchValue({
      first_nm: person.first_nm,
      last_nm: person.last_nm,
      affiliation_type: person.affiliation_type,
      job_title: person.job_title,
      employee_type: person.employee_type,
      behavioral_interventions_training: person.behavioral_interventions_training,
      start_date: person.start_date
      // behavioral_interventions_training :  person.behavioral_interventions_training,
    });
    this.staffForm.patchValue(person);
    this.staffForm.disable();
  }

  editStaff(person) {
    this.isViewStaff = false;
    this.isUpdate = true;
    (<any>$('#add-staff')).modal('show');
    this.staffForm.patchValue({
      first_nm: person.first_nm,
      last_nm: person.last_nm,
      affiliation_type: person.affiliation_type,
      job_title: person.job_title,
      employee_type: person.employee_type,
      behavioral_interventions_training: person.behavioral_interventions_training,
      // behavioral_interventions_training :  person.behavioral_interventions_training,
    });
    this.staffForm.enable();
  }

  deleteStaff(person) {

    this.staffForm.patchValue({
      first_nm: person.first_nm,
      last_nm: person.last_nm,
      affiliation_type: person.affiliation_type,
      job_title: person.job_title,
      employee_type: person.employee_type,
      isdeletable: true,
      behavioral_interventions_training: person.behavioral_interventions_training,
      // behavioral_interventions_training :  person.behavioral_interventions_training,
    });
    (<any>$('#delete-staff')).modal('show');
  }

  confirmDeleteStaff() {
    console.log(this.staffForm.value);
    this._commonHttpService.create(
      this.staffForm.value,
      'providerstaff'
    ).subscribe(
      (response) => {
        this._alertService.success(response);
        this.currentSelectedProviderStaffId = response.provider_staff_id;
      },
      (error) => {
        this._alertService.error('Unable to delete information');
      }
    );
  }
  resetFoster() {
    this.isviewFoster = false;
    this.isUpdate = false;
    this.fosterForm.enable();
    this.fosterForm.reset();
  }
  viewFoster(person) {
    this.isviewFoster = true;
    (<any>$('#add-foster')).modal('show');
    this.fosterForm.patchValue({
      last_name: person.last_name,
      first_name: person.first_name,
      address: person.address,
      phone_no: person.phone_no
    });
    this.fosterForm.disable();
  }
  editFoster(person) {
    this.isviewFoster = false;
    this.isUpdate = true;
    (<any>$('#add-foster')).modal('show');
    this.fosterForm.patchValue({
      last_name: person.last_name,
      first_name: person.first_name,
      address: person.address,
      phone_no: person.phone_no,
      provider_uir_actor_detail_id: person.provider_uir_actor_detail_id
    });
    this.fosterForm.enable();
  }
  deleteFoster(person) {

    this.fosterForm.patchValue({
      last_name: person.last_name,
      first_name: person.first_name,
      address: person.address,
      phone_no: person.phone_no,
      provider_uir_actor_detail_id: person.provider_uir_actor_detail_id,
      isdeletable: true
    });
    (<any>$('#delete-foster')).modal('show');
  }
  confirmDeleteFoster() {
    const fosterInfo = this.fosterForm.value;
    fosterInfo.uir_actor_type = 'foster';
    fosterInfo.uir_no = this._incidentService.uirid;
    if (this.providerUirId) {
      fosterInfo.provider_uir_id = this.providerUirId;
    }

    this._providerIncident.saveYouthFosterDetails(fosterInfo).subscribe(response => {
      // this.getYouthFosterInformation();
      // this.resetFosterForm();
      // this._alertService.success('Information saved successfully!');
      if (response) {
        (<any>$('#delete-foster')).modal('hide');
        this.resetFosterForm();
        this._alertService.success(response);
        this.getYouthFosterInformation();
      }
    });

  }
}


