import { Component, OnInit } from '@angular/core';
import { IncidentUirService } from '../incident-uir.service';
import { UirDetail } from '../_entities/uir-model';

@Component({
  selector: 'uir-header',
  templateUrl: './uir-header.component.html',
  styleUrls: ['./uir-header.component.scss']
})
export class UirHeaderComponent implements OnInit {

  uirid: string;
  created_by: string;
  uirDetail: UirDetail;
  constructor(private _service: IncidentUirService) {
    this.uirid = this._service.uirid;
    this.uirDetail = this._service.uirDetail;
  }

  ngOnInit() {
  }

}
