import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonHttpService, AlertService, DataStoreService, AuthService } from '../../../../@core/services';
import { IncidentUirService } from '../incident-uir.service';
import { ProviderIncidentReportService } from '../../provider-incident-report.service';
import { UirDetail, Supervisory } from '../_entities/uir-model';
import { AppUser } from '../../../../@core/entities/authDataModel';


@Component({
  selector: 'uir-supervisory',
  templateUrl: './uir-supervisory.component.html',
  styleUrls: ['./uir-supervisory.component.scss']
})
export class UirSupervisoryComponent implements OnInit, OnDestroy {

  supervisoryForm: FormGroup;
  providerUirId: string;
  uirNo: string;
  supervisoryInfo: Supervisory;
  user: AppUser;
  role: string;
  statusId: any;
  isDisabled: boolean;
  url: any;
  id: any;

  constructor(private formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _router: Router, private route: ActivatedRoute,
    private _incidentService: IncidentUirService,
    private _providerIncident: ProviderIncidentReportService,
    private _dataStore: DataStoreService,
    private _authService: AuthService) {
    this.id = this._incidentService.provideruirid;
    this.providerUirId = (this.id !== 'create') ? this.id : '';
    this.uirNo = this._incidentService.uirid;
    this.user = this._authService.getCurrentUser();
    this.role = this.user.role.name;
    this.statusId = this._dataStore.getData('uir_status_type_id');
    console.log(this.statusId + '|' + this.role);
    if ((this.role === 'Provider_Staff_Admin' && this.statusId === 49) ||
      (this.role !== 'Provider_Staff_Admin' && this.statusId !== 49)) {
      this.isDisabled = true;
    } else {
      this.isDisabled = false;
    }
    this.url = this._router.url;
    this.url = this.url.substring(0, this.url.lastIndexOf('/') + 1);
  }

  ngOnInit() {
    this.initSupervisoryForm();
    if (this.providerUirId) {
      this.getSupervisoryInformation();
    }
  }

  initSupervisoryForm() {
    this.supervisoryForm = this.formBuilder.group({
      supervisor_comments: [null],
      supervisor_name: [null],
      supervisor_signdate: [null],
      is_section_filledout: [null],
      is_supervisor_comments: [null],
      is_youth_witness: [null],
      is_notification: [null],
      is_nurses_reports: [null],
      is_signed_dates: [null],
      is_check_spelling: [null],
      is_comments_location: [null],
      is_addition_support: [null],
      is_incident_report: [null],
      other_section_filledout: [null],
      other_supervisor_comments: [null],
      other_youth_witness: [null],
      other_notification: [null],
      other_nurses_reports: [null],
      other_signed_dates: [null],
      other_check_spelling: [null],
      other_comments_location: [null],
      other_addition_support: [null],
      other_incident_report: [null],
      completed_by_name: [null],
      completed_by_date: [null]
    });

    if (this.isDisabled) {
      this.supervisoryForm.disable();
    } else {
      this.supervisoryForm.enable();
    }
  }

  patchSupervisory(event?) {
    console.log('Pacting values');
    const supervisoryData: Supervisory = this.supervisoryForm.value;
    supervisoryData.uir_no = this._incidentService.uirid;
    if (this.providerUirId) {
      supervisoryData.provider_uir_id = this.providerUirId;
    }

    if (this.supervisoryInfo && this.supervisoryInfo.provider_uir_supervisory_id) {
      supervisoryData.provider_uir_supervisory_id = this.supervisoryInfo.provider_uir_supervisory_id;
    }

    this._providerIncident.saveSupervisoryDetails(supervisoryData).subscribe(response => {
      if (response) {
        if (!this.providerUirId && response.provider_uir_id) {
          this.providerUirId = response.provider_uir_id;
          this._providerIncident.navigateTo(this.uirNo, this.providerUirId, 'supervisory');
        } else {
          this.getSupervisoryInformation();
        }
      }
      if (event === 'save') {
        if (this.id === 'create') {
          this.url = this.url.substr(0, this.url.length - 1);
          this.url = this.url.substring(0, this.url.lastIndexOf('/') + 1) + this.id + '/';
          this._alertService.warn('Please Fill the program tab First');
          setTimeout(() => {
            this._router.navigate([this.url + 'program']);
          }, 3000);

        } else {
          this._alertService.info('Information saved successfully!');
          // this._router.navigate([this.url + 'witness']);
        }
      } else {
        if (this.id === 'create') {
          this.url = this.url.substr(0, this.url.length - 1);
          this.url = this.url.substring(0, this.url.lastIndexOf('/') + 1) + this.id + '/';
          this._router.navigate([this.url + 'program']);
        }
      }
      // this._alertService.success('Information saved successfully!');
      // this._router.navigate([this.url + 'witness']);
    });
  }

  navigateTo() {
    const redirectUrl = '/provider-portal/incident-report/uir/' + this.uirNo + '/' + this.providerUirId + '/supervisory';
    this._router.navigate([redirectUrl]);
  }

  loadSupervisoryForm() {
    this.supervisoryForm.patchValue({
      supervisor_comments: this.supervisoryInfo.supervisor_comments,
      supervisor_name: this.supervisoryInfo.supervisor_name,
      supervisor_signdate: this.supervisoryInfo.supervisor_signdate,
      is_section_filledout: this.supervisoryInfo.is_section_filledout,
      is_supervisor_comments: this.supervisoryInfo.is_supervisor_comments,
      is_youth_witness: this.supervisoryInfo.is_youth_witness,
      is_notification: this.supervisoryInfo.is_notification,
      is_nurses_reports: this.supervisoryInfo.is_nurses_reports,
      is_signed_dates: this.supervisoryInfo.is_signed_dates,
      is_check_spelling: this.supervisoryInfo.is_check_spelling,
      is_comments_location: this.supervisoryInfo.is_comments_location,
      is_addition_support: this.supervisoryInfo.is_addition_support,
      is_incident_report: this.supervisoryInfo.is_incident_report,
      other_section_filledout: this.supervisoryInfo.other_section_filledout,
      other_supervisor_comments: this.supervisoryInfo.other_supervisor_comments,
      other_youth_witness: this.supervisoryInfo.other_youth_witness,
      other_notification: this.supervisoryInfo.other_notification,
      other_nurses_reports: this.supervisoryInfo.other_nurses_reports,
      other_signed_dates: this.supervisoryInfo.other_signed_dates,
      other_check_spelling: this.supervisoryInfo.other_check_spelling,
      other_comments_location: this.supervisoryInfo.other_comments_location,
      other_addition_support: this.supervisoryInfo.other_addition_support,
      other_incident_report: this.supervisoryInfo.other_incident_report,
      completed_by_name: this.supervisoryInfo.completed_by_name,
      completed_by_date: this.supervisoryInfo.completed_by_date
    });
  }

  getSupervisoryInformation() {
    const supervisoryParam = {
      where: { provider_uir_id: this.providerUirId },
      method: 'get',
      nolimit: true
    };
    this._providerIncident.getSupervisoryDetails(supervisoryParam).subscribe(response => {
      if (response) {
        this.supervisoryInfo = response[0];
        this.loadSupervisoryForm();
      }
    });
  }
  ngOnDestroy() {
    if (this.supervisoryForm.getRawValue().supervisor_comments ||
      this.supervisoryForm.getRawValue().supervisor_name ||
      this.supervisoryForm.getRawValue().supervisor_signdate ||
      this.supervisoryForm.getRawValue().is_section_filledout ||
      this.supervisoryForm.getRawValue().other_section_filledout ||
      this.supervisoryForm.getRawValue().is_supervisor_comments ||
      this.supervisoryForm.getRawValue().is_youth_witness ||
      this.supervisoryForm.getRawValue().other_youth_witness ||
      this.supervisoryForm.getRawValue().is_notification ||
      this.supervisoryForm.getRawValue().other_notification ||
      this.supervisoryForm.getRawValue().is_nurses_reports ||
      this.supervisoryForm.getRawValue().is_signed_dates ||
      this.supervisoryForm.getRawValue().other_signed_dates ||
      this.supervisoryForm.getRawValue().is_check_spelling ||
      this.supervisoryForm.getRawValue().other_check_spelling ||
      this.supervisoryForm.getRawValue().is_comments_location ||
      this.supervisoryForm.getRawValue().other_comments_location ||
      this.supervisoryForm.getRawValue().is_addition_support ||
      this.supervisoryForm.getRawValue().other_addition_support ||
      this.supervisoryForm.getRawValue().is_incident_report ||
      this.supervisoryForm.getRawValue().other_incident_report) {
      this.patchSupervisory();
    }

  }
}
