import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UirSupervisoryComponent } from './uir-supervisory.component';

const routes: Routes = [ 
  {
    path:'',
    component:UirSupervisoryComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UirSupervisoryRoutingModule { }
