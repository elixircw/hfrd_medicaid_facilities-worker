import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UirSupervisoryComponent } from './uir-supervisory.component';

describe('UirSupervisoryComponent', () => {
  let component: UirSupervisoryComponent;
  let fixture: ComponentFixture<UirSupervisoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UirSupervisoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UirSupervisoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
