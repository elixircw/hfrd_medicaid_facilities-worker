import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UirSupervisoryRoutingModule } from './uir-supervisory-routing.module';
import { UirSupervisoryComponent } from './uir-supervisory.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';

@NgModule({
  imports: [
    CommonModule,
    UirSupervisoryRoutingModule,
    FormMaterialModule
  ],
  declarations: [UirSupervisoryComponent]
})
export class UirSupervisoryModule { }
