import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UirYouthInvolvedComponent } from './uir-youth-involved.component';

describe('UirYouthInvolvedComponent', () => {
  let component: UirYouthInvolvedComponent;
  let fixture: ComponentFixture<UirYouthInvolvedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UirYouthInvolvedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UirYouthInvolvedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
