import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CommonHttpService, AlertService, DataStoreService, AuthService } from '../../../../@core/services';
import { IncidentUirService } from '../incident-uir.service';
import { ProviderIncidentReportService } from '../../provider-incident-report.service';
import { YouthInvolved } from '../../incident-uir/_entities/uir-model';
import { AppUser } from '../../../../@core/entities/authDataModel';

@Component({
  selector: 'uir-youth-involved',
  templateUrl: './uir-youth-involved.component.html',
  styleUrls: ['./uir-youth-involved.component.scss']
})
export class UirYouthInvolvedComponent implements OnInit {

  youthInvolvedForm: FormGroup;
  youthList = [];
  providerUirId: string;
  uirNo: string;
  restraintTypes = [];
  youthInvolvedData: YouthInvolved;
  youthInvolvedList = [];
  assignedStaff: any[];
  user: AppUser;
  role: string;
  statusId: any;
  isDisabled: boolean;
  durationPhysicalRestraint = [
                            {key: '01 - 15 Minutes', value: 15},
                            {key: '16 - 30 Minutes', value: 30},
                            {key: '31 - 45 Minutes', value: 45},
                            {key: '46 - 60 Minutes', value: 60},
                            {key: 'Greater than 60 minutes', value: 61}
                          ];
  durationMechanicalRestraint = [
                            {key: '01 – 15 Minutes', value: 15},
                            {key: '16 – 30 Minutes', value: 30},
                            {key: '31 – 45 Minutes', value: 45},
                            {key: '46 – 60 Minutes', value: 60},
                            {key: '61 – 75 Minutes', value: 75},
                            {key: '76 – 90 Minutes', value: 90},
                            {key: '91 – 105 Minutes', value: 105},
                            {key: '106 – 120 Minutes', value: 120},
                            {key: 'Greater than 120 minutes', value: 121}
                          ];
  durationSeclusion = [
                      {key: '01 - 30 Minutes', value: 30},
                      {key: '30 - 60 Minutes', value: 60},
                      {key: '1 – 2 Hours', value: 2},
                      {key: '2 – 4 Hours', value: 4},
                      {key: '4 – 6 Hours', value: 6},
                      {key: '6 – 8 Hours', value: 8}
                    ];
  constructor(private formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService,
    private _alertService: AlertService,
    private _dataStore: DataStoreService,
    private _incidentService: IncidentUirService,
    private _providerIncident: ProviderIncidentReportService,
    private _authService: AuthService
    ) {
      const id = this._incidentService.provideruirid;
      this.providerUirId = (id !== 'create') ? id : '';
      this.uirNo = this._incidentService.uirid;
      this.user = this._authService.getCurrentUser();
      this.role = this.user.role.name;
      this.statusId = this._dataStore.getData('uir_status_type_id');
      if ((this.role === 'Provider_Staff_Admin' && this.statusId === 49) ||
          (this.role !== 'Provider_Staff_Admin' && this.statusId !== 49)) {
        this.isDisabled = true;
      } else  {
        this.isDisabled = false;
      }
  }

  ngOnInit() {
    this.getRestraintType();
    this.loadYouthInvolvedForm();
    if (this.providerUirId) {
      this.getYouthInvolvedData();
      this.getYouthList();
    }
    this.getassignedstaff();
    // this.patchYouthInvolvedData();
  }

  getKey(val) {
    let tempKey = '';
    for (const key in this.durationPhysicalRestraint) {
      if (this.durationPhysicalRestraint[key].value === val) {
        tempKey =  this.durationPhysicalRestraint[key].key;
      }
    }
    return tempKey;
  }

  loadYouthInvolvedForm() {
    this.youthInvolvedForm = this.formBuilder.group({
      provider_uir_actor_detail_id: [null],
      provider_staff_id: [null],
      role_incident: [null],
      restraint_typekey: [null],
      duration_phiscial_restraint: [null],
      duration_phiscial_restraint_other: [null],
      is_intervention: [false],
      is_leaving_supervision: [false],
      is_prevention: [false],
      is_flexcuff: [false],
      is_handcuff: [false],
      is_legiron: [false],
      is_helmets: [false],
      is_handcuff_legiron: [false],
      is_protective_device: [false],
      duration_mechanical_restraint: [null],
      duration_mechanical_restraint_other: [null],
      is_de_escalation: [false],
      is_seen_medical: [false],
      is_injury_sustained: [false],
      injury_severity_rating: [false],
      is_injury_result: [false],
      is_seclusion: [false],
      duration_seclusion: [null],
      is_staff_assaulted: [false],
    });

    if (this.isDisabled) {
      this.youthInvolvedForm.disable();
    } else {
      this.youthInvolvedForm.enable();
    }
  }

  resetYouthInvolvedForm() {
    this.youthInvolvedForm.reset();
    this.loadYouthInvolvedForm();
  }

  patchYouthInvolvedData() {
    // if (this.youthInvolvedData) {
    //   this.youthInvolvedForm.patchValue({
    //     provider_uir_actor_detail_id: this.youthInvolvedData.provider_uir_actor_detail_id,
    //     provider_staff_id: this.youthInvolvedData.provider_staff_id,
    //     role_incident: this.youthInvolvedData.role_incident,
    //     restraint_typekey: this.youthInvolvedData.restraint_typekey,
    //     duration_phiscial_restraint: this.youthInvolvedData.duration_phiscial_restraint,
    //     duration_phiscial_restraint_other: this.youthInvolvedData.duration_phiscial_restraint_other,
    //     is_intervention: this.youthInvolvedData.is_intervention,
    //     is_leaving_supervision: this.youthInvolvedData.is_leaving_supervision,
    //     is_prevention: this.youthInvolvedData.is_prevention,
    //     is_flexcuff: this.youthInvolvedData.is_flexcuff,
    //     is_handcuff: this.youthInvolvedData.is_handcuff,
    //     is_legiron: this.youthInvolvedData.is_legiron,
    //     is_helmets: this.youthInvolvedData.is_helmets,
    //     is_handcuff_legiron: this.youthInvolvedData.is_handcuff_legiron,
    //     is_protective_device: this.youthInvolvedData.is_protective_device,
    //     duration_mechanical_restraint: this.youthInvolvedData.duration_mechanical_restraint,
    //     duration_mechanical_restraint_other: this.youthInvolvedData.duration_mechanical_restraint_other,
    //     is_de_escalation: this.youthInvolvedData.is_de_escalation,
    //     is_seen_medical: this.youthInvolvedData.is_seen_medical,
    //     is_injury_sustained: this.youthInvolvedData.is_injury_sustained,
    //     injury_severity_rating: this.youthInvolvedData.injury_severity_rating,
    //     is_injury_result: this.youthInvolvedData.is_injury_result,
    //     is_seclusion: this.youthInvolvedData.is_seclusion,
    //     duration_seclusion: this.youthInvolvedData.duration_seclusion,
    //     is_staff_assaulted: this.youthInvolvedData.is_staff_assaulted,
    //   });
    // }
  }

  submitYouthInvolvedDetails() {
    const youthData = this.youthInvolvedForm.value;
    youthData.uir_no = this._incidentService.uirid;
    if (this.providerUirId) {
      youthData.provider_uir_id = this.providerUirId;
    }

    if (this.youthInvolvedData) {
      youthData.provider_uir_actor_detail_id = this.youthInvolvedData.provider_uir_actor_detail_id;
    }

    this._providerIncident.saveYouthInvolvedDetails(youthData).subscribe(response => {
      if (response) {
        if (!this.providerUirId && response.provider_uir_id) {
          this.providerUirId = response.provider_uir_id;
          // this._providerIncident.navigateTo(this.uirNo, this.providerUirId, 'youth-involved');
        }
        this.getYouthInvolvedData();
      }
      (<any>$('#add-youthinvolved')).modal('hide');
      this._alertService.success('Information saved successfully!');
    });
  }

  getYouthInvolvedData() {
    const providerParam = {
      where: { provider_uir_id: this.providerUirId },
      method: 'get',
      count: -1 };
    this._providerIncident.getYouthInvolvedData(providerParam).subscribe(response => {
      if (response) {
        this.youthInvolvedList = response;
      }
    });
  }

  getYouthList() {
    const youthListParam = {
      where: { provider_uir_id: this.providerUirId },
      method: 'get',
      nolimit: true };
    this._providerIncident.getYouthFosterData(youthListParam).subscribe(response => {
      if (response) {
        this.youthList = (response) ? response.filter(person => person.uir_actor_type === 'youth') : [];
      }
    });
  }

  getRestraintType() {
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        where: {tablename: 'Restraint_Type', teamtypekey: 'OLM'},
      },
      'referencetype/gettypes?filter'
    ).subscribe(
      (response) => {
        this.restraintTypes = response;
      },
      (error) => {
        this._alertService.error('Unable to retrieve information');
      }
    );
  }

  getassignedstaff() {
    this._commonHttpService.create(
      {
        // where: { provider_applicant_id: this.applicantId },
        where: { provider_applicant_id: this._incidentService.uirid },
        method: 'post'
      },
      'providerstaff/getassignedstaff'

    ).subscribe(response => {
      this.assignedStaff = response;
      console.log(JSON.stringify('assigned staff', this.assignedStaff));
    },
      (error) => {
        this._alertService.error('Unable to get assigned staffs, please try again.');
        console.log('get contact Error', error);
        return false;
      }
    );
  }

}
