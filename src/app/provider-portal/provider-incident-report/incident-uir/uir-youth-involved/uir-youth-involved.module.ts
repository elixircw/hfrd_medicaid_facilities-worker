import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UirYouthInvolvedRoutingModule } from './uir-youth-involved-routing.module';
import { UirYouthInvolvedComponent } from './uir-youth-involved.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';


@NgModule({
  imports: [
    CommonModule,
    UirYouthInvolvedRoutingModule,
    FormMaterialModule
  ],
  declarations: [UirYouthInvolvedComponent]
})
export class UirYouthInvolvedModule { }
