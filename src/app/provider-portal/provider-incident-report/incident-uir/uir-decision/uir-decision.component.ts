import { Component, OnInit } from '@angular/core';
// import { ProviderAuthorizationService } from '.../../../provider-authorization.service';
// import { ComplaintDetailsService } from '../complaint-details.service';
// import { ComplaintDecisonService } from './complaint-decison.service';
// import { ComplaintDecison } from '../_entities/complaint-detail.model';
// import { AlertService } from '../../../../../@core/services';
// import { RouteUserService } from '../../../route-users/route-user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { ProviderAuthorizationService } from '../../../../pages/providers/provider-authorization.service';
import { AlertService, DataStoreService, AuthService } from '../../../../@core/services';
import { RouteUserService } from '../../../../pages/providers/route-users/route-user.service';
import { ProviderIncidentReportService } from '../../provider-incident-report.service';
import { IncidentUirService } from '../incident-uir.service';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { ComplaintDecison } from '../../../../pages/providers/complaint/complaint-detail/_entities/complaint-detail.model';
const OTHER_AGENCY = 'Refers to another licensing agency';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'uir-decision',
  templateUrl: './uir-decision.component.html',
  styleUrls: ['./uir-decision.component.scss']
})


export class UirDecisionComponent implements OnInit {

  invalidData = [];
  decisions = [];
  status: string;
  statusDescription: string;
  comments: string;
  agency: string;
  agencyEmail: string;
  notification: string;
  notificationIdentifier: boolean;
  decisionHistory = [];
  agencyList = ['DJS', 'DHS', 'MDH', 'Other'];
  onForwardToOther = false;
  // cmpltSavBtnTxt: string = "Sumbit";
  userAssingmentSubscription: Subscription;
  providerUirId: string;
  uirNo: string;
  user: AppUser;
  role: string;
  url: any;
  assignSecurityId: string;
  getUsersList: any[];
  constructor(private _router: Router,
    private _authorization: ProviderAuthorizationService,
    // private _service: ComplaintDecisonService,
    private _alertService: AlertService,
    private route: ActivatedRoute,
    private _routeUserService: RouteUserService,
    private _providerIncident: ProviderIncidentReportService,
    private _dataStore: DataStoreService,
    private _incidentService: IncidentUirService,
    private _authService: AuthService,
    private _providerIncidentReportService: ProviderIncidentReportService) {
     this.providerUirId = (this._dataStore.getData('provider_uir_id')) ? this._dataStore.getData('provider_uir_id') : null;

    this.decisions = [{key: 'Save as Draft', value: 'Review'},
                      {key: 'Submit', value: 'Submitted'},
                      {key: 'Withdraw', value: 'Closed'}];
    const id = this._incidentService.provideruirid;
    this.providerUirId = (id !== 'create') ? id : null;
    this.uirNo = this._incidentService.uirid;
    this.user = this._authService.getCurrentUser();
    this.role = this.user.role.name;
    const resource = this.user.resources;
    if (this.role !== 'Provider_Staff_Admin' && resource.find(data => data.name === 'Incident_Reporting')) {
      this.decisions = [{key: 'Return for Addition Information to Provider', value: 'Return'},
                        {key: 'Approved', value: 'Approved'},
                        {key: 'Close', value: 'Closed'}];
    }
    this.url = this._router.url;
    this.url = this.url.substring(0, this.url.lastIndexOf('/') + 1);
    this._providerIncidentReportService.setPersonData(this.providerUirId, this.uirNo);
  }

  ngOnInit() {
    this.loadList();
  }

  loadList() {
    // this._service.getList().subscribe(response => {
    //   console.log(response);
    //   if (response && response.length) {
    //     this.decisionHistory = response;
    //   }

    // });
  }

  onStatusChange(event) {
    // console.log('display name', data);
    // this.cmpltSavBtnTxt = (data.source.triggerValue == "Approve and Assign") ? "Assign" : "Submit";
    // if (data.source.triggerValue === OTHER_AGENCY) {
    //   this.onForwardToOther = true;
    // } else {
    //   this.onForwardToOther = false;
  console.log(event);
  if (event.value === 'Return to Provider') {
      this.notificationIdentifier = true;
  } else {
    this.notificationIdentifier = false;
  }
    // }

  }

  listenOnUserAssignment() {
    this.userAssingmentSubscription = this._routeUserService.selectedUserOnAssign$.subscribe(user => {
      if (user) {
        this.assignSecurityId = user.userid;
        this.saveDecision();
      }
    });
  }

  onSubmitDecison() {
    this.invalidData = [];
    if (!this.status || !this.comments) {
      this._alertService.error('Please fill required fields');
      return;
    }

    const validationData = this._providerIncidentReportService.getIncidentValidation();
    this.invalidData =validationData && validationData.length ? validationData.filter(data => { return data.isValid !== true; }) : [];
    if(this.invalidData && this.invalidData.length) {
      (<any>$('#add-decision')).modal('hide');
      (<any>$('#invalid-submit')).modal('show');
      return;
    }
    // if (this.status === 'submit') {
    //   (<any>$('#add-decision')).modal('hide');
    //   this.openRouteUser();
    // } else {
      this.saveDecision();
    // }
    // check status conditon
    // create on metho saVe dceions
    // if approve open route user
    //  on if(<any>$('#add-decision')).modal('hide');
    // create listner method
    // on listner call saveDecion method decion.assingnsecurityuserid = user.userid
    // on else call savdDecison
  }

  openRouteUser() {
    // this.listenOnUserAssignment();
    // this._router.navigate(['assign-user'], { relativeTo: this.route });
    (<any>$('#route-userassign')).modal('show');
    this.user = null;
    this._routeUserService.getRoutingUsersList('PRCM').subscribe(response => {
      this.getUsersList = response.data;
    });
  }

  selectPerson(user) {
    console.log(user);
    this.user = user;
  }

  saveDecision() {
    // if (this.providerUirId) {
    let notificationText = null;
    if (this.status === 'Return to Provider') {
      notificationText = this.notification;
    }
    const decision = {
      'status': this.status,
      'provider_uir_id': this.providerUirId,
      'notification': notificationText,
      'assingsecurityuserid': (this.status === 'Submitted' || this.status === 'Approved') ? '45e5f7b8-c327-412a-b814-cdf1062d2499' : null
    };

    this._providerIncident.saveDecision(decision).subscribe(response => {
      this.status = '';
      this.comments = '';
      console.log(response);
      this._alertService.success('Decision Submitted Successfully', true);
      const requestObj = {
        where: {
          uir_no: this._incidentService.uirid
        }
      };
      this._providerIncident.sendUIRNotification(requestObj).subscribe(callresp => {
        (<any>$('#add-decision')).modal('hide');
        this._alertService.success('Email notification sent Successfully', true);
        this._router.navigate(['/provider-portal/incident-report/dashboard']);
      });
    });
    // } else {
    //   this._alertService.error('UIR details not created yet', true);
    // }
  }

  resetDecision() {
    this.status = '';
    this.agency = '';
    this.agencyEmail = '';
    this.comments = '';
    this.notification = '';

  }
  // onChange(event){
  //   console.log(event);
  //   if (event.value === 1) {
  //     this.notificationIdentifier = true;
  // } else {
  //   this.notificationIdentifier = false;
  // }
}

