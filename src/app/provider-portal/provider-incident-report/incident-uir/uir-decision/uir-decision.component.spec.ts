import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UirDecisionComponent } from './uir-decision.component';

describe('UirDecisionComponent', () => {
  let component: UirDecisionComponent;
  let fixture: ComponentFixture<UirDecisionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UirDecisionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UirDecisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
