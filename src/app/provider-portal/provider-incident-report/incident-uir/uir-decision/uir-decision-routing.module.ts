import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UirDecisionComponent } from './uir-decision.component';

const routes: Routes = [
  {
    path: '',
    component: UirDecisionComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UirDecisionRoutingModule { }
