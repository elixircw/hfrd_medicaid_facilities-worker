import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UirDecisionRoutingModule } from './uir-decision-routing.module';
import { UirDecisionComponent } from './uir-decision.component';
import { FormMaterialModule } from '../../../../@core/form-material.module';
import { ProviderAuthorizationService } from '../../../../pages/providers/provider-authorization.service';
import { RouteUserService } from '../../../../pages/providers/route-users/route-user.service';


@NgModule({
  imports: [
    CommonModule,
    UirDecisionRoutingModule,
    FormMaterialModule
  ],
  declarations: [ UirDecisionComponent],
  providers: [ProviderAuthorizationService, RouteUserService]
})
export class UirDecisionModule { }
