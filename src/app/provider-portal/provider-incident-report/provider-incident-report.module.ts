import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProviderIncidentReportRoutingModule } from './provider-incident-report-routing.module';
import { ProviderIncidentReportComponent } from './provider-incident-report.component';
import { ProviderIncidentReportService } from './provider-incident-report.service';
import { IncidentUirService } from './incident-uir/incident-uir.service';

@NgModule({
  imports: [
    CommonModule,
    ProviderIncidentReportRoutingModule,
  ],
  declarations: [ProviderIncidentReportComponent],
  providers: [ProviderIncidentReportService, IncidentUirService]
})
export class ProviderIncidentReportModule { }
