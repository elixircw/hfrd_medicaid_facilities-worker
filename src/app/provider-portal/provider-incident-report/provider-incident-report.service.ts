import { Injectable } from '@angular/core';
import { ProviderPortalUrlConfig  } from '../provider-portal-url.config';
import { CommonHttpService } from '../../@core/services';
import { Router } from '@angular/router';
import { UirDetail, Supervisory, YouthInvolved } from './incident-uir/_entities/uir-model';
import { ProviderPortalService } from '../provider-portal.service';


@Injectable()
export class ProviderIncidentReportService {
  isClassIncidentSelected: boolean;
  isIncidentAvailable : boolean;
  isNotificationInfoAvailable: boolean;
  isLawEnforcementAvailable: Boolean;
  isStaffAvailable: Boolean;
  isYouthAvailable: Boolean;
  constructor(private _router: Router, private _providerPortalService: ProviderPortalService, private _commonHttpService: CommonHttpService) { }

  openIncidentReport() {
    this._commonHttpService.getArrayList({}, ProviderPortalUrlConfig.EndPoint.Uir.GetNextNumberUrl).subscribe(result => {
      const url = '/provider-portal/incident-report/uir/' + result['nextNumber'] + '/create/program';
      this._router.navigate([url]);
    });
  }

  selectClassIncident() {
    this.isClassIncidentSelected = true;
  }

  setPersonData(providerId, uirNo) {
    
    const classParam = {
      where: { provider_uir_id: providerId },
      method: 'get',
      count: -1 };
    this.getProgramDetails(providerId,uirNo).subscribe(response => {
      this.isClassIncidentSelected = response &&  response.length ? true : false;
     });
    this.getClass3IncidentDetails(classParam).subscribe( response =>{
      this.isIncidentAvailable = response && response.length ? true : false;
    });
    // this.getContactData(classParam).subscribe( response =>{
    //   this.isNotificationInfoAvailable = response && response.length && response[0] && response[0].provider_uir_contact_detail && response[0].provider_uir_contact_detail.length ? true : false;
    // });
    // this.getLawEnforcementData(classParam).subscribe( response =>{
    //   this.isLawEnforcementAvailable = response && response.length ? true : false;
    // });
    // this.getassignedstaff(uirNo).subscribe( response =>{
    //   this.isStaffAvailable = response && response.length ? true : false;
    // });

    this.getYouthList(uirNo).subscribe( response => { 
      this.isYouthAvailable = response && response.length ? true : false;
    });
  }


  getYouthList(uirNo) {
    return this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        where: { uir_no: uirNo }
      },
      ProviderPortalUrlConfig.EndPoint.Uir.GET_YOUTH_LIST
    );
  }

  getProgramDetails(providerId,uirid) {
    return this._commonHttpService.getArrayList(
      {
          method: 'post',
          nolimit: true,
          filter: {
            where:
            {
             provider_id: this._providerPortalService.getProviderId(),
             uir_no: uirid
            }
          }
      }, ProviderPortalUrlConfig.EndPoint.Uir.GET_PROGRAM_DETAILS );
  }

  getIncidentValidation() {
    const validationData =[ 
     { 'name':'Class3 Incident' , 'isValid' : this.isClassIncidentSelected},
     {'name':'Incident', 'isValid' : this.isIncidentAvailable},
    //  {'name':'Notification', 'isValid' : this.isNotificationInfoAvailable},
    //  {'name':'Law Enforcement', 'isValid': this.isLawEnforcementAvailable},
    //  {'name':'Staff Available', 'isValid': this.isStaffAvailable},
    /*  {'name':'Youth', 'isValid': this.isYouthAvailable} */
    ];
    return   validationData;
  }


  getassignedstaff(uirNo) {
    return this._commonHttpService.create(
      {
        // where: { provider_applicant_id: this.applicantId },
        where: { provider_applicant_id: uirNo },
        method: 'post'
        // applicant_id:this.applicantNumber
      },
      'providerstaff/getassignedstaff'
    );
  }


  saveUirDetails(uirDetails: UirDetail) {
    return this._commonHttpService.create(uirDetails, ProviderPortalUrlConfig.EndPoint.Uir.CREATE_UPDATE_URL);
  }

  saveIncidentUirDetails(uirDetails: UirDetail) {
    return this._commonHttpService.create(uirDetails, ProviderPortalUrlConfig.EndPoint.Uir.CREATE_UPDATE_INCIDENT);
  }

  saveWitnessDetails(witness) {
    return this._commonHttpService.create(witness, ProviderPortalUrlConfig.EndPoint.Uir.CREATE_UPDATE_WITNESS);
  }

  saveDecision(decision) {
    return this._commonHttpService.create(decision, ProviderPortalUrlConfig.EndPoint.Uir.CREATE_UPDATE_DECISION);
  }

  saveSupervisoryDetails(supervisory: Supervisory) {
    return this._commonHttpService.create(supervisory, ProviderPortalUrlConfig.EndPoint.Uir.CREATE_UPDATE_SUPERVISORY);
  }

  saveNotificationDetails(notification) {
    return this._commonHttpService.create(notification, ProviderPortalUrlConfig.EndPoint.Uir.CREATE_UPDATE_NOTIFICATION);
  }

  saveYouthInvolvedDetails(youthInvolvedDetail: YouthInvolved) {
    return this._commonHttpService.create(youthInvolvedDetail, ProviderPortalUrlConfig.EndPoint.Uir.CREATE_UPDATE_YOUTHINVOLVED);
  }

  saveYouthFosterDetails (person) {
    return this._commonHttpService.create(person, ProviderPortalUrlConfig.EndPoint.Uir.CREATE_UPDATE_YOUTHFOSTER);
  }

  saveClass3IncidentDetails(incident) {
    return this._commonHttpService.create(incident, ProviderPortalUrlConfig.EndPoint.Uir.CREATE_UPDATE_CLASS3_INCIDENT);
  }

  saveContactDetails (contact) {
    return this._commonHttpService.create(contact, ProviderPortalUrlConfig.EndPoint.Uir.CREATE_UPDATE_CONTACT);
  }

  saveLawEnforcementDetails (lawEnforcement) {
    return this._commonHttpService.create(lawEnforcement, ProviderPortalUrlConfig.EndPoint.Uir.CREATE_UPDATE_LAW_ENFORCEMENT);
  }


  getYouthInvolvedData(youthParam) {
    return this._commonHttpService.getSingle(youthParam, ProviderPortalUrlConfig.EndPoint.Uir.GET_YOUTH_INVOLVED_DETAILS);
  }

  getYouthFosterData(youthListParam) {
    return this._commonHttpService.getSingle(youthListParam, ProviderPortalUrlConfig.EndPoint.Uir.GET_YOUTH_FOSTER_LIST);
  }

  getContactData(contactListParam) {
    return this._commonHttpService.getSingle(contactListParam, ProviderPortalUrlConfig.EndPoint.Uir.GET_CONTACT_LIST);
  }

  getLawEnforcementData(lawEnforcementListParam) {
    return this._commonHttpService.getSingle(lawEnforcementListParam, ProviderPortalUrlConfig.EndPoint.Uir.GET_LAW_ENFORCEMENT_LIST);
  }

  getProviderUirDetails(providerDetail) {
    return this._commonHttpService.getSingle(providerDetail, ProviderPortalUrlConfig.EndPoint.Uir.GET_UIR_DETAILS);
  }

  getUirNotificationDetails(providerDetail) {
    return this._commonHttpService.getAllPaged(providerDetail, ProviderPortalUrlConfig.EndPoint.Uir.LIST_NOTIFICATION);
  }

  getSupervisoryDetails(supervisoryParam) {
    return this._commonHttpService.getSingle(supervisoryParam, ProviderPortalUrlConfig.EndPoint.Uir.GET_SUPERVISORY_DETAILS);
  }

  getNarrativeDetails(narrativeParam) {
    return this._commonHttpService.getSingle(narrativeParam, ProviderPortalUrlConfig.EndPoint.Uir.GET_NARRATIVE_DETAILS);
  }

  getClassIncidentDetails(classDetail) {
    return this._commonHttpService.getArrayList(classDetail, ProviderPortalUrlConfig.EndPoint.Uir.GET_CLASSINCIDENT_DETAILS);
  }

  getClass3IncidentDetails(classDetail) {
    return this._commonHttpService.getArrayList(classDetail, ProviderPortalUrlConfig.EndPoint.Uir.GET_CLASS3_INCIDENT_DETAILS);
  }

  getIncidentDropList(filter: any) {
    return this._commonHttpService.getAllFilter(filter, ProviderPortalUrlConfig.EndPoint.Uir.LIST_URL);
  }

  getIncidentRecordDashboard(filter: any) {
    return this._commonHttpService.getAllFilter(filter, ProviderPortalUrlConfig.EndPoint.Uir.LIST_URL);
  }

  navigateTo(uirNo, providerUirId, page) {
    const redirectUrl = '/provider-portal/incident-report/uir/' + uirNo + '/' + providerUirId + '/' + page;
    this._router.navigate([redirectUrl]);
  }

  sendUIRNotification(requestObj) {
    return this._commonHttpService.create(requestObj, 'Provider_uir/senduirnotification');
  }

}


