import { Injectable } from '@angular/core';
import { CommonHttpService, AuthService } from '../../../@core/services';
import { PaginationRequest } from '../../../@core/entities/common.entities';

@Injectable()
export class IncidentReportDashboardService {

  constructor(private _commonHttpService: CommonHttpService,
    private _authService: AuthService) { }

  getDashboardList(pageSize: number, pageNumber: number, searchCritera: any) {
    return this._commonHttpService.getPagedArrayList(
      new PaginationRequest({
        limit: pageSize,
        page: pageNumber,
        method: 'post',
        count: -1,
        where: searchCritera
      }),
      'provider_uir/listdashboard'
    ).map(response => {
      return response;
    });
  }

}
