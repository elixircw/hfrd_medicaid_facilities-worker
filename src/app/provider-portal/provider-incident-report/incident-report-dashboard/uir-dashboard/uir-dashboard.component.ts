import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { DynamicObject, PaginationInfo } from '../../../../@core/entities/common.entities';
import { IncidentUirService } from '../../incident-uir/incident-uir.service';
import { DataStoreService, CommonHttpService, AuthService } from '../../../../@core/services';
import { PaginationRequest } from '../../../../@core/entities/common.entities';
import { IncidentReportDashboardService } from '../incident-report-dashboard.service';
import { Router } from '@angular/router';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { MatTableDataSource } from '@angular/material';
import { ProviderPortalService } from '../../../provider-portal.service';


@Component({
  // tslint:disable-next-line:component-selector
  selector: 'uir-dashboard',
  templateUrl: './uir-dashboard.component.html',
  styleUrls: ['./uir-dashboard.component.scss']
})
export class UirDashboardComponent implements OnInit {

  totalRecords: number;
  paginationInfo: PaginationInfo = new PaginationInfo();
  dynamicObject: DynamicObject = {};
  private pageStream$ = new Subject<number>();
  private searchTermStream$ = new Subject<DynamicObject>();
  previousPage: number;
  uirList = [];
  tabs = [];
  status: string;
  url: any;
  user: AppUser;
  role: string;
  search_uir_no: any = '';
  dataSource = new MatTableDataSource<any>();
  displayedColumns: string[] = ['uir_no', 'provider_id', 'provider_name', 'program_name', 'licensing_agency', 'child_name' ,'insertedon', 'incident_type', 'statusdesc'];
  isLoading: Boolean = false;
  sortData: any;
  search_provider_id: any = '';
  search_provider_name: any = '';
  search_program_name: any = '';
  search_youth_identification_number: any = '';
  search_child_name: any = '';
  isProvider: any;
  search_type: any;
  constructor(private _service: IncidentReportDashboardService,
    private _incidentService: IncidentUirService,
    private _dataStore: DataStoreService,
    private _commonHttpService: CommonHttpService,
    private _router: Router,
    private _authService: AuthService,
    private _providerPortalService: ProviderPortalService,
    ) {
      this.url = this._router.url;
      this.url = this.url.substring(0, this.url.lastIndexOf('/') + 1);
      this.user = this._authService.getCurrentUser();
      this.role = this.user.role.name;
      console.log('role name--->', this.user.role.name);
      if(this.user && this.user.role) {
        this.isProvider = this.user.role.name === "Provider_Staff_Admin" ? true : false;
      }
    }

  ngOnInit() {
    this.paginationInfo.sortBy = 'desc';
    this.paginationInfo.sortColumn = 'updateddate';
    this.sortData = {
      active: 'uir_no',
      direction: 'desc'
    };
    this.pageStream$.subscribe(data => {
      this.paginationInfo.pageNumber = data;
      this.loadDashboardList(1, this.status);
  });
    this.status = this.isProvider ? 'pending' : 'pa-pending';
    this.loadDashboardList(1, this.status);
  }

  onNewIncidentReport() {
    this._incidentService.openIncidentReport();
    this._dataStore.setData('provider_uir_id', '');
    this._dataStore.setData('uir_status_type_id', '');
  }

  searchDashboard() {
    this.loadDashboardList(1, this.status);
  }

  

  setStatus(status) {
    this.status = this.isProvider ? status : 'pa-'+status;
    this.loadDashboardList(1, this.status);
  }

  loadDashboardList(selectPageNumber: number, status: string) {
    this.status = status;
    setTimeout(() => {
      this.isLoading = true;
    });
    const whereClause =  {
      status: this.status,
      uir_no: this.search_uir_no.trim() && this.search_uir_no.trim() !== '' ? this.search_uir_no.trim() : null,
      provider_id: Number(this.search_provider_id) ? Number(this.search_provider_id) : null,
      provider_name: this.search_provider_name.trim() && this.search_provider_name.trim() !== '' ? this.search_provider_name.trim() : null,
      program_name: this.search_program_name.trim() && this.search_program_name.trim() !== '' ? this.search_program_name.trim() : null,
      youth_identification_number: this.search_youth_identification_number.trim() && this.search_youth_identification_number.trim() !== '' ? this.search_youth_identification_number.trim() : null,
      child_name: this.search_child_name.trim() && this.search_child_name.trim() !== '' ? this.search_child_name.trim() : null,
      sortcolumn: this.sortData.active,
      sortorder: this.sortData.direction
    } ;
    this._service.getDashboardList(this.paginationInfo.pageSize, this.previousPage, whereClause).subscribe(result => {
      if (result && result.data) {
        this.uirList = result.data;
        this.dataSource.data = this.uirList;
        this.totalRecords = result.count;
        this.isLoading = false;
      }
    });
  }

  pageChanged(pageInfo: any) {
    console.log(pageInfo);
    this.paginationInfo.pageNumber = pageInfo.page;
    this.paginationInfo.pageSize = pageInfo.itemsPerPage;
    this.previousPage = this.paginationInfo.pageNumber;
    this.loadDashboardList(this.previousPage,this.status);
    //this.pageStream$.next(this.paginationInfo.pageNumber);
  }

  pageChanged1(pageInfo: any) {
    this.paginationInfo.pageNumber = pageInfo.page;
    this.paginationInfo.pageSize = pageInfo.itemsPerPage;
    this.pageStream$.next(this.paginationInfo.pageNumber);
  }

  getUirDetails(data) {
    this._providerPortalService.setAgencyProviderId(data.provider_id);
    this._dataStore.setData('uir_status_type_id', data.routingstatustypeid);
    this._router.navigate([this.url + 'uir/' + data.uir_no + '/' + data.provider_uir_id + '/program']);
  }

  customSort(event) {
    console.log('sort data custome', event);
    this.sortData = event;
    this.loadDashboardList(1, this.status);
    this.paginationInfo.pageNumber = 1;
  }

  onSearch(searchType: string, searchValue: any) {
    console.log(searchValue);
    this.sortData.active = searchType;
    // Preferably use switch
    if (searchType === 'uir_no') {
      this.search_uir_no = searchValue;
    }
    if (searchType === 'provider_id') {
      this.search_provider_id = searchValue;
    }
    if (searchType === 'provider_name') {
      this.search_provider_name = searchValue;
    }
    if (searchType === 'program_name') {
      this.search_program_name = searchValue;
    }
    if (searchType === 'youth_identification_number') {
      this.search_youth_identification_number = searchValue;
    }
    if (searchType === 'child_name') {
      this.search_child_name = searchValue;
    }
    this.paginationInfo.pageNumber = 1;
    this.loadDashboardList(1, this.status);
  }

  resetSearch() {
    this.search_type = null;
    // this.status = this.isProvider ? 'pending' : 'pa-pending';
    this.search_uir_no = '';
    this.search_provider_id = '';
    this.search_provider_name = '';
    this.search_program_name = '';
    this.search_youth_identification_number = '';
    this.search_child_name = '';
    this.loadDashboardList(1, this.status);
  }

}

