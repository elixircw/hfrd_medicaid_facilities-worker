import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UirDashboardComponent } from './uir-dashboard.component';

describe('UirDashboardComponent', () => {
  let component: UirDashboardComponent;
  let fixture: ComponentFixture<UirDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UirDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UirDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
