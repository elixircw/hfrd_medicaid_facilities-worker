import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IncidentReportDashboardComponent } from './incident-report-dashboard.component';


const routes: Routes = [
  {
    path : '',
    component: IncidentReportDashboardComponent
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IncidentReportDashboardRoutingModule { }
