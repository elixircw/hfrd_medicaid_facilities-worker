import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IncidentReportDashboardRoutingModule } from './incident-report-dashboard-routing.module';
import { IncidentReportDashboardComponent } from './incident-report-dashboard.component';
import { FormMaterialModule } from '../../../@core/form-material.module';
import { ProviderIncidentReportService } from '../provider-incident-report.service';
import { UirDashboardComponent } from './uir-dashboard/uir-dashboard.component';
import { IncidentReportDashboardService } from './incident-report-dashboard.service';
import { PaginationModule } from 'ngx-bootstrap';
import { MatSortModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    IncidentReportDashboardRoutingModule,
    FormMaterialModule,
    PaginationModule,
    MatSortModule
  ],
  declarations: [IncidentReportDashboardComponent, UirDashboardComponent],
  providers: [ProviderIncidentReportService, IncidentReportDashboardService]
})
export class IncidentReportDashboardModule { }
