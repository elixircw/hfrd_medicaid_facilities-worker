import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProviderIncidentReportComponent} from './provider-incident-report.component';

const routes: Routes = [
  {
    path: '',
    component: ProviderIncidentReportComponent
  },
  {
    path: 'dashboard',
    loadChildren: './incident-report-dashboard/incident-report-dashboard.module#IncidentReportDashboardModule'
  },
  {
    path: 'uir/:uirid/:provideruirid',
    loadChildren: './incident-uir/incident-uir.module#IncidentUirModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProviderIncidentReportRoutingModule { }
