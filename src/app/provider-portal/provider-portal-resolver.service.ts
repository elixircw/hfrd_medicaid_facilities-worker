import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ProviderPortalService } from './provider-portal.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ProviderPortalResolverService implements Resolve<any> {

  constructor(private _service: ProviderPortalService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {

    return this._service.getProviderConfiguration();
  }

}
