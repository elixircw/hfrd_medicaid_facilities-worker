import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FileError, NgxfUploaderService } from 'ngxf-uploader';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Subject } from 'rxjs/Subject';

import { AppUser } from '../../@core/entities/authDataModel';
import { DropdownModel } from '../../@core/entities/common.entities';
import { GLOBAL_MESSAGES } from '../../@core/entities/constants';
import { AlertService } from '../../@core/services/alert.service';
import { AuthService } from '../../@core/services/auth.service';
import { CommonHttpService } from '../../@core/services/common-http.service';
import { AppConfig } from '../../app.config';
import { Accomplishment, Education, School, Testing, Vocation, EducationDetails } from '../../pages/newintake/my-newintake/_entities/newintakeModel';
import { NewUrlConfig } from '../../pages/newintake/newintake-url.config';
import { CaseWorkerUrlConfig } from '../../pages/case-worker/case-worker-url.config';
import { CommonDropdownsService } from '../../@core/services/common-dropdowns.service';
import { of } from 'rxjs/observable/of';
import { CommonUrlConfig } from '../../@core/common/URLs/common-url.config';
import { config } from '../../../environments/config';
declare var $: any;
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'educational-profile',
    templateUrl: './educational-profile.component.html',
    styleUrls: ['./educational-profile.component.scss']
})
export class EducationalProfileComponent implements OnInit {
    @Input()
    addEducationSubject$ = new Subject<Education>();
    @Input()
    addEducationOutputSubject$ = new Subject<any>();
    @Input()
    educationFormReset$ = new Subject<boolean>();
    addEducation: Education;
    schoolForm: FormGroup;
    vocationForm: FormGroup;
    testingForm: FormGroup;
    accomplishmentForm: FormGroup;
    educationDetailsForm: FormGroup;
    vocationButton: boolean;
    school: School[] = [];
    vocation: Vocation[] = [];
    testing: Testing[] = [];
    accomplishment: Accomplishment[] = [];
    educationDetails: EducationDetails[] = [];
    vocationEditInd = -1;
    schoolEditInd = -1;
    testingEditInd = -1;
    accomplishEditInd = -1;
    edcnDetailInd = -1;
    isSpecialEducation = false;
    schoolTypeDropdownItems$: Observable<DropdownModel[]>;
    specialEducationDropdownItems$: Observable<DropdownModel[]>;
    stateDropdownItems$: Observable<DropdownModel[]>;
    lastGradeDropdownItems$: Observable<DropdownModel[]>;
    currentGradeDropdownItems$: Observable<DropdownModel[]>;
    testingTypeDropdownItems$: Observable<DropdownModel[]>;
    countyDropDownItems: DropdownModel[];
    schoolSetting$: Observable<DropdownModel[]>;
    splEdcnIntLevel$: Observable<DropdownModel[]>;
    edcnProgram$: Observable<DropdownModel[]>;
    edcnStatus$: Observable<DropdownModel[]>;
    gradeLevel$: Observable<DropdownModel[]>;
    edcnPerformance$: Observable<DropdownModel[]>;
    schlExitReason$: Observable<DropdownModel[]>;
    schoolExitDropdownItems$: Observable<DropdownModel[]>;
    edcnPerformanceDropdownItems$: Observable<DropdownModel[]>;
    classTypeDropdownItems$: Observable<DropdownModel[]>;
    funcGradeDropdownItems$: Observable<DropdownModel[]>;
    schoolSettingDropdownItems$: Observable<DropdownModel[]>;
    edcnTranspDropdownItems$: Observable<DropdownModel[]>;
    countyDropDownItems$: Observable<DropdownModel[]>;
    testingDescription: string;
    gradeDescription: string;
    schoolDescription: string;
    maxDate = new Date();
    uploadedFile: File;
    private token: AppUser;
    schoolSearchList$ = new Observable<any>();
    baseUrl: string;
    schoolid: string;
    isDjs = false;
    constructor(
        private formbulider: FormBuilder,
        private _uploadService: NgxfUploaderService,
        private _commonHttpService: CommonHttpService,
        private _alertSevice: AlertService,
        private _authService: AuthService,
        private http: HttpClient,
        private _commonDropdownsService: CommonDropdownsService
    ) {
        this.baseUrl = AppConfig.baseUrl;
     }

    ngOnInit() {
        this.isDjs = this._authService.isDJS();
        this.token = this._authService.getCurrentUser();
        this.schoolForm = this.formbulider.group({
            personeducationid: '',
            personid: '',
            educationname: '',
            educationtypekey: [null],
            schoolsetting: '',
            edcntransp: '',
            countyid: '',
            statecode: '',
            startdate: [null],
            enddate: [null],
            lastgradetypekey: [null],
            currentgradetypekey: [null],
            isspecialeducation: [false],
            specialeducation: '',
            specialeducationtypekey: [null],
            iepstartdate: [null],
            absentdate: [null],
            isreceived: [false],
            isverified: [false],
            isexcuesed: [false],
            reciveVeriExc: '',
            extracurricular: '',
            city: '',
            contactInfo: this.formbulider.group({
                contactName: '',
                workPhone: '',
                workPhoneExt: '',
                fax: ''
            }),
            schoolSchedule: '',
            schoolAdjustment: ''
        });

        this.testingForm = this.formbulider.group({
            personeducationtestingid: '',
            personid: '',
            testingtypekey: [null],
            readinglevel: '',
            readingtestdate: [null],
            mathlevel: '',
            mathtestdate: [null],
            testingprovider: ''
        });

        this.accomplishmentForm = this.formbulider.group({
            personaccomplishmentid: '',
            personid: '',
            highestgradetypekey: [null],
            accomplishmentdate: [null],
            isrecordreceived: '',
            receiveddate: [null]
        });

        this.vocationForm = this.formbulider.group({
            personeducationvocationid: '',
            personid: '',
            isvocationaltest: '',
            vocationinterest: '',
            vocationaptitude: '',
            certificatename: '',
            certificatepath: '',
            uploadFile: ''
        });
        this.educationDetailsForm = this.formbulider.group({
            typeOfClass: [''],
            lastgradetypekey: [''],
            currentgradetypekey: [''],
            splEdcLeastRestEnv: [''],
            funGradeLevel: [''],
            dateLastAttended: [''],
            edcnPerformance: this.formbulider.group({
                quater1: [''],
                quater2: [''],
                quater3: [''],
                quater4: [''],
            }),
            splEdcnNeeds: [''],
            schlExitReason: [''],
            schlExitDate: [''],
            strengths: [''],
            weakness: [''],
            edcnProgram: [''],
            homeHospEdcnServices: [''],
            edcnPrgmGoal: [''],
            extCurricularAct: [''],
            comments: [''],
            disabledQuestion: [''],
            diabilityNotes: [''],
            schlChngdPlcmnt: [''],
            reasonToChangeSchl: [''],

        });
        this.loadDropDown();
        this.addEducation = {
            school: [],
            testing: [],
            accomplishment: [],
            vocation: []
        };
        this.educationFormReset$.subscribe((res) => {
            if (res === true) {
                this.schoolForm.reset();
                this.testingForm.reset();
                this.accomplishmentForm.reset();
                this.vocationForm.reset();
            }
        });
        this.addEducationOutputSubject$.subscribe((education) => {
            this.school = education.personeducation ? education.personeducation : [];
            this.accomplishment = education.personaccomplishment ? education.personaccomplishment : [];
            this.testing = education.personeducationtesting ? education.personeducationtesting : [];
            this.vocation = education.personeducationvocation ? education.personeducationvocation : [];
        });
        this.addEducationSubject$.next(this.addEducation);
        this.loadSchoolDropdown();
    }
    schoolTypeDescription(model) {
        this.schoolDescription = '';
        this.schoolDescription = model.text;
    }
    addSchool(model: School) {
        if (this.schoolForm.value.absentdate && this.schoolForm.value.enddate != null && this.schoolForm.value.absentdate > this.schoolForm.value.enddate) {
            return this._alertSevice.error('Absence date should not be exceed end date');
        } else if (this.schoolForm.value.absentdate && this.schoolForm.value.startdate > this.schoolForm.value.absentdate) {
            return this._alertSevice.error('Absence date should not be less than start date');
        }
        if (this.schoolDescription && this.schoolDescription !== '') {
            model.schoolTypeDescription = this.schoolDescription;
        }
        model.isspecialeducation = model.specialeducation === 'true' ? true : false;
        const schoolInfo = Object.assign({
            educationname: model.educationname,
            educationtypekey: model.educationtypekey,
            statecode: model.statecode,
            startdate: model.startdate,
            enddate: model.enddate,
            countyid: model.countyid,
            lastgradetypekey: model.lastgradetypekey,
            currentgradetypekey: model.currentgradetypekey,
            isspecialeducation: model.isspecialeducation,
            specialeducationtypekey: model.specialeducationtypekey,
            absentdate: model.absentdate,
            iepstartdate: model.iepstartdate,
            isreceived: model.isreceived,
            isverified: model.isverified,
            isexcuesed: model.isexcuesed,
            schoolTypeDescription: this.schoolDescription,
            contactInfo: model.contactInfo,
            schoolAdjustment: model.schoolAdjustment,
            schoolSchedule: model.schoolSchedule,
            schoolid: this.schoolid
        });
        this.school.push(schoolInfo);
        this.addEducation.school = this.school;
        this.addEducationSubject$.next(this.addEducation);
        this.schoolForm.reset();
        this.isSpecialEducation = false;
        this.schoolDescription = '';
        this.schoolid = '';
        this._alertSevice.success('Address Added Successfully');
        $('#schoolAcdn').click();
    }
    updateSchool(model: School) {
        if (this.schoolForm.value.absentdate && this.schoolForm.value.enddate != null && this.schoolForm.value.absentdate > this.schoolForm.value.enddate) {
            return this._alertSevice.error('Absence date should not be exceed end date');
        } else if (this.schoolForm.value.absentdate && this.schoolForm.value.startdate > this.schoolForm.value.absentdate) {
            return this._alertSevice.error('Absence date should not be less than start date');
        }
        if (this.schoolDescription && this.schoolDescription !== '') {
            model.schoolTypeDescription = this.schoolDescription;
        }
        this.school[this.schoolEditInd].educationname = model.educationname;
        this.school[this.schoolEditInd].educationtypekey = model.educationtypekey;
        this.school[this.schoolEditInd].statecode = model.statecode;
        this.school[this.schoolEditInd].startdate = model.startdate;
        this.school[this.schoolEditInd].enddate = model.enddate;
        this.school[this.schoolEditInd].lastgradetypekey = model.lastgradetypekey;
        this.school[this.schoolEditInd].currentgradetypekey = model.currentgradetypekey;
        this.school[this.schoolEditInd].isspecialeducation = model.isspecialeducation;
        this.school[this.schoolEditInd].specialeducationtypekey = model.specialeducationtypekey;
        this.school[this.schoolEditInd].iepstartdate = model.iepstartdate;
        this.school[this.schoolEditInd].absentdate = model.absentdate;
        this.school[this.schoolEditInd].isreceived = model.isreceived;
        this.school[this.schoolEditInd].isverified = model.isverified;
        this.school[this.schoolEditInd].isexcuesed = model.isexcuesed;
        this.school[this.schoolEditInd].schoolTypeDescription = model.schoolTypeDescription;
        this.school[this.schoolEditInd].isspecialeducation = model.specialeducation === 'true' ? true : false;
        this.school[this.schoolEditInd].contactInfo = model.contactInfo;
        this.school[this.schoolEditInd].schoolAdjustment = model.schoolAdjustment;
        this.school[this.schoolEditInd].schoolSchedule = model.schoolSchedule;
        this.school[this.schoolEditInd].schoolid = this.schoolid;
        this.addEducation.school = this.school;
        this.addEducationSubject$.next(this.addEducation);
        this.schoolEditInd = -1;
        this.schoolForm.reset();
        this.schoolDescription = '';
        this._alertSevice.success('Address Updated Successfully');
        $('#schoolAcdn').click();
    }
    editSchool(model, index) {
        this.schoolEditInd = index;
        this.schoolForm.patchValue(model);
        this.specialEducation(model.specialeducation);
        this.schoolForm.enable();
        this.schoolid = model.schoolid;
        this.loadCounty(model.statecode, model.countyid);
        $('#schoolAcdn').click();
    }
    viewSchool(model) {
        this.schoolForm.patchValue(model);
        this.specialEducation(model.specialeducation);
        this.schoolForm.disable();
        this.schoolEditInd = -1;
        $('#schoolAcdn').click();
    }
    deleteSchool(index) {
        this.school.splice(index, 1);
        this._alertSevice.success('Deleted Successfully');
        this.addEducation.school = this.school;
        this.addEducationSubject$.next(this.addEducation);
        this.schoolForm.enable();
        this.schoolid = null;
        this.schoolForm.reset();
        this.schoolEditInd = -1;
    }
    selectTestingDescription(model) {
        this.testingDescription = '';
        this.testingDescription = model.text;
    }
    addTesting(model: Testing) {
        if (this.testingDescription && this.testingDescription !== '') {
            model.testdescription = this.testingDescription;
        }
        const testing = Object.assign({
            testingtypekey: model.testingtypekey,
            readinglevel: model.readinglevel,
            readingtestdate: model.readingtestdate,
            mathlevel: model.mathlevel,
            mathtestdate: model.mathtestdate,
            testingprovider: model.testingprovider,
            testdescription: this.testingDescription
        });
        this.testing.push(testing);
        this.testingForm.reset();
        this.addEducation.testing = this.testing;
        this.addEducationSubject$.next(this.addEducation);
        this.testingDescription = '';
    }
    updateTesting(model: Testing) {
        if (this.testingDescription && this.testingDescription !== '') {
            model.testdescription = this.testingDescription;
        }
        this.testing[this.testingEditInd].testingtypekey = model.testingtypekey;
        this.testing[this.testingEditInd].readinglevel = model.readinglevel;
        this.testing[this.testingEditInd].readingtestdate = model.readingtestdate;
        this.testing[this.testingEditInd].mathlevel = model.mathlevel;
        this.testing[this.testingEditInd].mathtestdate = model.mathtestdate;
        this.testing[this.testingEditInd].testingprovider = model.testingprovider;
        this.testing[this.testingEditInd].testdescription = model.testdescription;
        this.testingForm.reset();
        this.addEducation.testing = this.testing;
        this.addEducationSubject$.next(this.addEducation);
        this.testingEditInd = -1;
        this.testingDescription = '';
    }
    editTesting(model, index) {
        this.testingEditInd = index;
        this.testingForm.patchValue(model);
        this.testingForm.enable();
        $('#testingAcdn').click();
    }
    viewTesting(model) {
        this.testingForm.patchValue(model);
        this.testingForm.disable();
        this.testingEditInd = -1;
        $('#testingAcdn').click();
    }
    deleteTesting(index) {
        this.testing.splice(index, 1);
        this._alertSevice.success('Deleted Successfully');
        this.addEducation.testing = this.testing;
        this.addEducationSubject$.next(this.addEducation);
        this.testingForm.enable();
        this.testingForm.reset();
        this.testingEditInd = -1;
    }
    addEdcnDetails(model: EducationDetails) {

        this.educationDetails.push(model);
        this.educationDetailsForm.reset();
        this.addEducation.educationDetails = this.educationDetails;
        // this.addEducationSubject$.next(this.addEducation);
        // this._dataStoreService.setData(MyNewintakeConstants.Intake.PersonsInvolved.Educational, this.addEducation);
        // this.testingDescription = '';
    }

    updateEdcnDetails(model: EducationDetails) {

        this.educationDetails[this.edcnDetailInd] = model;
        // this.educationDetails[this.edcnDetailInd].isspecialeducation = model.specialeducation === 'true' ? true : false;
        this.addEducation.educationDetails = this.educationDetails;
        // this.addEducationSubject$.next(this.addEducation);
        // this._dataStoreService.setData(MyNewintakeConstants.Intake.PersonsInvolved.Educational, this.addEducation);
        this.edcnDetailInd = -1;
        this.educationDetailsForm.reset();
        // this.schoolDescription = '';
    }
    editEdcnDetails(model, index) {
        this.edcnDetailInd = index;
        this.educationDetailsForm.patchValue(model);
        // this.specialEducation(model.specialeducation);
        // this.schoolTypeDescription({ text: model.schoolTypeDescription, value: model.currentgradetypekey });
        this.educationDetailsForm.enable();
        $('#EdcnDetailsAcdn').click();
    }
    viewEdcnDetails(model) {
        this.educationDetailsForm.patchValue(model);
        //  this.specialEducation(model.specialeducation);
        this.educationDetailsForm.disable();
        this.edcnDetailInd = -1;
        $('#EdcnDetailsAcdn').click();
    }
    deleteEdcnDetails(index) {
        this.educationDetails.splice(index, 1);
        this._alertSevice.success('Deleted Successfully');
        this.addEducation.school = this.school;
        // this.addEducationSubject$.next(this.addEducation);
        //  this._dataStoreService.setData(MyNewintakeConstants.Intake.PersonsInvolved.Educational, this.addEducation);
        this.educationDetailsForm.enable();
        this.educationDetailsForm.reset();
        this.edcnDetailInd = -1;
    }
    highetGradeDescription(model) {
        this.gradeDescription = '';
        this.gradeDescription = model.text;
    }


    addAccomplishment(model: Accomplishment) {
        if (this.accomplishmentForm.valid) {
            if (this.gradeDescription && this.gradeDescription !== '') {
                model.gradedescription = this.gradeDescription;
            }
            const accomplishmentInfo = Object.assign({
                highestgradetypekey: model.highestgradetypekey,
                accomplishmentdate: model.accomplishmentdate,
                isrecordreceived: model.isrecordreceived,
                receiveddate: model.receiveddate,
                gradedescription: model.gradedescription
            });
            this.accomplishment.push(accomplishmentInfo);
            this.accomplishmentForm.reset();
            this.addEducation.accomplishment = this.accomplishment;
            this.addEducationSubject$.next(this.addEducation);
            this.gradeDescription = '';
        } else {
            this._alertSevice.warn('Please fill mandatory fields');
        }
    }
    setManditory(state: boolean, inputfield: string, manditory: string) {
        if (state === false && manditory === 'isManditory') {
            if (inputfield === 'receiveddate') {
                this.accomplishmentForm.get('isrecordreceived').valueChanges.subscribe((recordRecived: any) => {
                    if (recordRecived === 'Yes') {
                        this.accomplishmentForm.get('receiveddate').setValidators([Validators.required]);
                        this.accomplishmentForm.get('receiveddate').updateValueAndValidity();
                    } else {
                        this.accomplishmentForm.get('receiveddate').clearValidators();
                        this.accomplishmentForm.get('receiveddate').updateValueAndValidity();
                    }
                });
            }
        }
    }
    updateAccomplishment(model: Accomplishment) {
        if (this.gradeDescription && this.gradeDescription !== '') {
            model.gradedescription = this.gradeDescription;
        }
        if (this.accomplishmentForm.valid) {
            this.accomplishment[this.accomplishEditInd].highestgradetypekey = model.highestgradetypekey;
            this.accomplishment[this.accomplishEditInd].accomplishmentdate = model.accomplishmentdate;
            this.accomplishment[this.accomplishEditInd].isrecordreceived = model.isrecordreceived;
            this.accomplishment[this.accomplishEditInd].receiveddate = model.receiveddate;
            this.accomplishment[this.accomplishEditInd].gradedescription = model.gradedescription;
            this.accomplishmentForm.reset();
            this.addEducation.accomplishment = this.accomplishment;
            this.addEducationSubject$.next(this.addEducation);
            this.accomplishEditInd = -1;
            this.gradeDescription = '';
        } else {
            this._alertSevice.warn('Please fill mandatory fields');
        }
    }
    editAccomplishment(model, index) {
        this.accomplishEditInd = index;
        this.accomplishmentForm.patchValue(model);
        this.accomplishmentForm.enable();
        $('#accomplishmentsAcdn').click();
    }
    viewAccomplishment(model) {
        this.accomplishmentForm.patchValue(model);
        this.accomplishmentForm.disable();
        this.accomplishEditInd = -1;
    }
    deleteAccomplishment(index) {
        this.accomplishment.splice(index, 1);
        this._alertSevice.success('Deleted Successfully');
        this.addEducation.accomplishment = this.accomplishment;
        this.addEducationSubject$.next(this.addEducation);
        this.accomplishmentForm.enable();
        this.accomplishmentForm.reset();
        this.accomplishEditInd = -1;
    }
    addVocation(model: Vocation) {
        // upload attachment
        this._uploadService
            .upload({
                url: AppConfig.baseUrl + '/' + NewUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl + '?access_token=' + this.token.id,
                headers: new HttpHeaders().set('access_token', this.token.id).set('ctype', 'file'),
                filesKey: ['file'],
                files: this.uploadedFile,
                process: true
            })
            .subscribe(
                (response) => {
                    if (response.status) {
                        // this.progress.percentage = response.percent;
                    }
                    if (response.status === 1 && response.data) {
                        const vocation = Object.assign({
                            vocationinterest: model.vocationinterest,
                            vocationaptitude: model.vocationaptitude,
                            isvocationaltest: model.isvocationaltest,
                            certificatename: model.certificatename,
                            certificatepath: model.certificatepath
                        });
                        this.vocation.push(vocation);
                        this.vocation[this.vocation.length - 1].certificatename = response.data.originalfilename;
                        this.vocation[this.vocation.length - 1].certificatepath = response.data.s3bucketpathname;
                        this.vocationForm.reset();
                        this.addEducation.vocation = this.vocation;
                        this.addEducationSubject$.next(this.addEducation);
                        this._alertSevice.success('File Uploaded Succesfully!');
                    }
                },
                (err) => {
                    console.log(err);
                    this._alertSevice.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
                },
                () => {
                    console.log('complete');
                }
            );
        // end of attachment upload
    }
    updateVocation(model: Vocation) {
        this.vocation[this.vocationEditInd] = model;
        this.vocationForm.reset();
        this.addEducation.vocation = this.vocation;
        this.addEducationSubject$.next(this.addEducation);
        this.vocationEditInd = -1;
    }
    editVocation(model, index) {
        this.vocationEditInd = index;
        this.vocationForm.patchValue(model);
        this.vocationForm.enable();
        this.vocationButton = false;
        $('#voctionAcdn').click();
    }
    viewVocation(model, index) {
        this.vocationEditInd = index;
        this.vocationForm.patchValue(model);
        this.vocationForm.disable();
        this.vocationEditInd = -1;
        this.vocationButton = true;
        $('#voctionAcdn').click();
    }
    deleteVocation(index) {
        this.vocation.splice(index, 1);
        this._alertSevice.success('Deleted Successfully');
        this.addEducation.vocation = this.vocation;
        this.addEducationSubject$.next(this.addEducation);
        this.vocationForm.enable();
        this.vocationForm.reset();
        this.vocationEditInd = -1;
        this.vocationButton = false;
    }
    uploadFile(file: File | FileError): void {
        if (!(file instanceof File)) {
            // this.alertError(file);
            return;
        }
        this.uploadedFile = file;
        this.vocationForm.patchValue({ certificatename: file.name });
    }
    private loadDropDown() {
        const source = forkJoin([
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    where: { isspecialeducation: false },
                    order: 'typedescription ASC'
                },
                NewUrlConfig.EndPoint.Intake.EducationTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    where: { isspecialeducation: true },
                    order: 'typedescription ASC'
                },
                NewUrlConfig.EndPoint.Intake.EducationTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true
                },
                NewUrlConfig.EndPoint.Intake.StateListUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    where: { 'active_sw': 'Y', 'picklist_type_id': '91', 'delete_sw': 'N' },
                    order: 'sort_order_no asc'
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    where: { ishighergrade: true },
                    order: 'displayorder ASC'
                },
                NewUrlConfig.EndPoint.Intake.GradeTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true
                },
                NewUrlConfig.EndPoint.Intake.TestingTypeUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    order: 'countyname asc',
                },
                NewUrlConfig.EndPoint.Intake.CountryListUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    where: { referencetypeid: 142 }
                },
                'referencetype/gettypes' + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    where: { referencetypeid: 144 }
                },
                'referencetype/gettypes' + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    where: { referencetypeid: 145 }
                },
                'referencetype/gettypes' + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    where: { referencetypeid: 146 }
                },
                'referencetype/gettypes' + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    where: { 'active_sw': 'Y', 'picklist_type_id': '75', 'delete_sw': 'N' }
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    where: { 'active_sw': 'Y', 'picklist_type_id': '447', 'delete_sw': 'N' }
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    where: { 'active_sw': 'Y', 'picklist_type_id': '415', 'delete_sw': 'N' }
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    where: { 'active_sw': 'Y', 'picklist_type_id': '85', 'delete_sw': 'N' }
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    where: { 'active_sw': 'Y', 'picklist_type_id': '325', 'delete_sw': 'N' }
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
            ),
            this._commonHttpService.getArrayList(
                {
                    method: 'get',
                    nolimit: true,
                    where: { 'active_sw': 'Y', 'picklist_type_id': '373', 'delete_sw': 'N' }
                },
                CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
            )
        ])
            .map((result) => {
                return {
                    schoolType: result[0].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.educationtypekey
                            })
                    ),
                    specialEducation: result[1].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.educationtypekey
                            })
                    ),
                    states: result[2].map(
                        (res) =>
                            new DropdownModel({
                                text: res.statename,
                                value: res.stateabbr
                            })
                    ),
                    lastGrade: result[3].map(
                        (res) =>
                            new DropdownModel({
                                text: res.description_tx,
                                value: res.value_tx
                            })
                    ),
                    currentGrade: result[4].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.gradetypekey
                            })
                    ),
                    testingType: result[5].map(
                        (res) =>
                            new DropdownModel({
                                text: res.typedescription,
                                value: res.testingtypekey
                            })
                    ),
                    counties: result[6].map(
                        (res) =>
                            new DropdownModel({
                                text: res.countyname,
                                value: res.countyname
                            })
                    ),
                    schoolSetting: result[7].map(
                        (res) =>
                            new DropdownModel({
                                text: res.value_text,
                                value: res.ref_key
                            })
                    ),
                    splEdcnIntLevel: result[8].map(
                        (res) =>
                            new DropdownModel({
                                text: res.value_text,
                                value: res.ref_key
                            })
                    ),
                    edcnProgram: result[9].map(
                        (res) =>
                            new DropdownModel({
                                text: res.value_text,
                                value: res.ref_key
                            })
                    ),
                    edcnStatus: result[10].map(
                        (res) =>
                            new DropdownModel({
                                text: res.value_text,
                                value: res.ref_key
                            })
                    ),
                    exitReason: result[11].map(
                        (res) =>
                            new DropdownModel({
                                text: res.description_tx,
                                value: res.value_tx
                            })
                    ),
                    quarter: result[12].map(
                        (res) =>
                            new DropdownModel({
                                text: res.description_tx,
                                value: res.value_tx
                            })
                    ),
                    classtype: result[13].map(
                        (res) =>
                            new DropdownModel({
                                text: res.description_tx,
                                value: res.value_tx
                            })
                    ),
                    funcGrade: result[14].map(
                        (res) =>
                            new DropdownModel({
                                text: res.description_tx,
                                value: res.value_tx
                            })
                    ),
                    schoolStng: result[15].map(
                        (res) =>
                            new DropdownModel({
                                text: res.description_tx,
                                value: res.value_tx
                            })
                    ),
                    edcnTransportation: result[16].map(
                        (res) =>
                            new DropdownModel({
                                text: res.description_tx,
                                value: res.value_tx
                            })
                    )
                };
            })
            .share();
        this.schoolTypeDropdownItems$ = source.pluck('schoolType');
        this.specialEducationDropdownItems$ = source.pluck('specialEducation');
        this.stateDropdownItems$ = source.pluck('states');
        this.lastGradeDropdownItems$ = source.pluck('lastGrade');
        this.currentGradeDropdownItems$ = source.pluck('currentGrade');
        this.testingTypeDropdownItems$ = source.pluck('testingType');
        this.countyDropDownItems$ = source.pluck('counties');
        this.schoolSetting$ = source.pluck('schoolSetting');
        this.splEdcnIntLevel$ = source.pluck('splEdcnIntLevel');
        this.edcnProgram$ = source.pluck('edcnProgram');
        this.edcnStatus$ = source.pluck('edcnStatus');
        this.schoolExitDropdownItems$ = source.pluck('exitReason');
        this.edcnPerformanceDropdownItems$ = source.pluck('quarter');
        this.classTypeDropdownItems$ = source.pluck('classtype');
        this.funcGradeDropdownItems$ = source.pluck('funcGrade');
        this.schoolSettingDropdownItems$ = source.pluck('schoolStng');
        this.edcnTranspDropdownItems$ = source.pluck('edcnTransportation');
        // let t = [];
        // t.push(new DropdownModel({
        //     text: "Grade 1",
        //     value: 3370
        // }));
        // t.push(new DropdownModel({
        //     text: "Grade 2",
        //     value: 3372
        // }));
        this.gradeLevel$ = source.pluck('lastGrade');
        this.edcnPerformance$ = of(this.getEducationPerformance());
    }
    specialEducation(specialEdu) {
        if (specialEdu === 'true') {
            this.isSpecialEducation = true;
        } else {
            this.isSpecialEducation = false;
        }
    }

    getEducationPerformance() {
        const edcnAr = [];
        edcnAr.push(new DropdownModel({
            text: 'Excellent',
            value: 3954
        }));
        edcnAr.push(new DropdownModel({
            text: 'Fair',
            value: 3955
        }));

        return edcnAr;
    }

    getSchoolExitReason() {
        const schExitReason = [];
        schExitReason.push(new DropdownModel({
            text: 'At or near age level',
            value: 3958
        }));
        schExitReason.push(new DropdownModel({
            text: 'Behavioral Problems',
            value: 3959
        }));

        return schExitReason;
    }
    loadSchoolDropdown() {
        const searchkey = this.schoolForm.get('educationname').value;
        const headers = new HttpHeaders().set('no-loader', 'true');
        const workEnv = config.workEnvironment;
        let schoolUrl = '';
        if (workEnv === 'state') {
            schoolUrl = this.baseUrl + '/' + 'School/v1/' + CommonUrlConfig.EndPoint.PERSON.EDUCATION.SCHOOL.GetSchoolUrl;
        } else {
            schoolUrl =  this.baseUrl + '/' + CommonUrlConfig.EndPoint.PERSON.EDUCATION.SCHOOL.GetSchoolUrl;
        }
        this.schoolSearchList$ = this.http.get(schoolUrl + '?filter=' + JSON.stringify({
          where: {
            searchkey: searchkey
          },
          nolimit: true, method: 'get'
        }), { headers: headers }).map((result) => {
          return result;
        });
      }

      sourceSelected(school) {
        this.loadCounty(school.statekey, school.countyid);
        setTimeout(() => {
          this.schoolid = school.schoolid;
          this.schoolForm.patchValue({
            educationtypekey: school.schooltypekey,
            statecode: school.statekey,
            address: school.address,
            zip: school.zipcode
          });
        }, 1000);
      }

      private loadCounty(countystate, countyid) {
        this._commonDropdownsService.getCountyList(countystate).subscribe(result => {
          this.countyDropDownItems = result;
          this.schoolForm.patchValue({
            countyid: countyid
          });
        });
      }
}
