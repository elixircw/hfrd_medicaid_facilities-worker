import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Health, Physician } from '../../pages/newintake/my-newintake/_entities/newintakeModel';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AlertService } from '../../@core/services/alert.service';
import { InvolvedPerson } from '../../pages/case-worker/_entities/caseworker.data.model';
import { AppUser } from '../../@core/entities/authDataModel';
import { AuthService } from '../../@core/services/auth.service';
import { PersonHealthService } from './person-health.service';
import { DataStoreService } from '../../@core/services';
import { AppConstants } from '../../@core/common/constants';
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'person-health',
    templateUrl: './person-health.component.html',
    styleUrls: ['./person-health.component.scss']
})
export class PersonHealthComponent implements OnInit {
    @Input()
    addHealthSubject$ = new Subject<Health>();
    @Input()
    addHealthOutputSubject$ = new Subject<Health>();
    @Input()
    healthFormReset$ = new Subject<boolean>();
    addHealth: Health;
    physicianForm: FormGroup;
    physician: Physician[] = [];
    token: AppUser;
    healthSections: any[];
    djshealthSections = [
        { id: 'physician-information', name: 'Physician Information', isActive: true, securityKey: '' },
        { id: 'health-insurance-information', name: 'Health Insurance Information', isActive: false, securityKey: '' },
        { id: 'medication-including-psychotropic', name: 'Medication Including Psychotropic', isActive: false, securityKey: '' },
        { id: 'health-examination', name: 'Health Examination/Evaluations', isActive: false, securityKey: '' },
        { id: 'medical-conditions', name: 'Medical Conditions', isActive: false, securityKey: '' },
        { id: 'behavioral-health-info', name: 'Behavioral Health Info', isActive: false, securityKey: '' },
        { id: 'history-of-abuse', name: 'History of Abuse', isActive: false, securityKey: '' },
        { id: 'substance-abuse', name: 'Substance Abuse', isActive: false, securityKey: '' },
        // { id: '', name: 'Substance Use' },
        // { id: '', name: 'Substance Abuse Assessments' },
        // { id: '', name: 'Dental' },
        // { id: '', name: 'Vision' },
        // { id: '', name: 'Immunizations' },
        // { id: '', name: 'Behavioral/Mental Health Situation' },
        { id: 'log', name: 'Log', isActive: false, securityKey: '' }
    ];
    cwhealthSections = [
        { id: 'provider-dental-information-cw', name: 'Provider Information', isActive: true, securityKey: 'myintake-persons-involved-edit-health-provider-information' },
        { id: 'insurance-information-cw', name: 'Insurance Information', isActive: false, securityKey: 'myintake-persons-involved-edit-health-insurance-information' },
        { id: 'medication-including-psychotropic-cw', name: 'Medication Including Psychotropic', isActive: false, securityKey: 'myintake-persons-involved-edit-health-medication-including-psychotrophic' },
        { id: 'medical-conditions-cw', name: 'Medical Conditions', isActive: false,  securityKey: 'myintake-persons-involved-edit-health-medical-conditions' },
        { id: 'behavioral-health-info-cw', name: 'Behavioral Health Info', isActive: false, securityKey: 'myintake-persons-involved-edit-health-behavioral-health-info' },
        { id: 'substance-abuse', name: 'Substance Abuse', isActive: false, securityKey: 'myintake-persons-involved-edit-health-substance-abuse' },
        { id: 'allergies-cw', name: 'Allergies', isActive: false },
        { id: 'chronic-cw', name: 'Chronic', isActive: false },
        { id: 'person-disability-cw', name: 'Disabilities', isActive: false },
        { id: 'elimination-info-cw', name: 'Elimination', isActive: false },
        { id: 'examination-cw', name: 'Examination', isActive: false },
        { id: 'family-history-cw', name: 'Family History', isActive: false },
        { id: 'feeding-info', name: 'Feeding Information', isActive: false },
        { id: 'hospitalization-cw', name: 'Hospitalization', isActive: false },
        { id: 'immunization-cw', name: 'Immunization', isActive: false },
        { id: 'insurance-information-cw', name: 'Insurance Information', isActive: false },
        { id: 'medical-conditions-cw', name: 'Medical Conditions', isActive: false },
        { id: 'medication-including-psychotropic-cw', name: 'Medication Including Psychotropic', isActive: false },
        { id: 'mobility-speech', name: 'Mobility/Speech', isActive: false },
        { id: 'provider-dental-information-cw', name: 'Provider Information', isActive: false },
        { id: 'sexual-information-cw', name: 'Sexual Information', isActive: false },
        { id: 'sleeping-info-cw', name: 'Sleeping', isActive: false },
        { id: 'substance-abuse', name: 'Substance Abuse', isActive: false },
        { id: 'under-5years-cw', name: 'Under 5 Years', isActive: false },
    ];

    selectedHealthSection: any;
    physicianEditInd = -1;

    @Input()
    addedPersons: InvolvedPerson[] = [];

    constructor(private formbulider: FormBuilder,
        private _alertSevice: AlertService,
        private _authService: AuthService,
        private healthService: PersonHealthService,
        private _dataStoreService: DataStoreService) {
    }

    ngOnInit() {
        this.token = this._authService.getCurrentUser();
        const teamTypeKey = this.token.user.userprofile.teamtypekey;
        if (teamTypeKey === 'DJS') {
            this.healthSections = this.djshealthSections;
        } else if (teamTypeKey === 'CW') {
            this.healthSections = this.cwhealthSections;
        } else if (teamTypeKey === 'AS') {
            this.healthSections = this.djshealthSections;
        }

        this.selectedHealthSection = this.healthSections[0];

        // this.physicianForm = this.formbulider.group({
        //     'ismedicaidmedicare': true,
        //     'providertype': 1,
        //     'policyholdername': '',
        //   'address1': '',
        //   'address2': '',
        //   'city': '',
        //   'state': '',
        //   'county': '',
        //   'zip': '',
        //   'providerphone': '',
        //   'patientpolicyholderrelation': '',
        //   'policyname': '',
        //   'groupnumber': '',
        //   'startdate': '',
        //   'enddate': ''
        // });
        // this.addHealth = {
        //     physician: []
        // };
        this.healthFormReset$.subscribe((res) => {
            if (res === true) {
                // this.resetForm();
                const personId = this._dataStoreService.getData(AppConstants.GLOBAL_KEY.SELECTED_PERSON_ID);
                this.healthService.setPersonHealthInfo(personId);
                this.showHealthSection(this.healthSections[0]);
            }
        });
        // this.addHealthOutputSubject$.subscribe((health) => {
        //     this.physician = health.physician ? health.physician : [];
        //     this.viewPhysician(this.physician);
        // });
        // this.addHealthSubject$.next(this.addHealth);

        // this.physicianForm.valueChanges.subscribe(result => {
        //     const physician = this.physicianForm.getRawValue();
        //     this.addPhysician(physician);
        // });

        // console.log('Selected Tab', this.selectedHealthSection);
    }

    addPhysician(model: Physician) {

        this.physician.push(model);
        this.addHealth.physician = this.physician;
        this.addHealthSubject$.next(this.addHealth);
    }
    viewPhysician(model) {
        this.physicianForm.patchValue(model);
    }

    showHealthSection(healthSection) {
        this.healthSections.forEach(section => section.isActive = false);
        healthSection.isActive = true;
        this.selectedHealthSection = healthSection;
    }

    // private resetForm() {
    //      this.physicianForm.reset();
    // }
}

