export class HealthConstants {

  public static LIST_KEY = {
    'PERSON_BEHAVIOR': 'personBehaviour',
    'PERSON_BIRTH': 'personBirth',
    'PERSON_CHRONIC': 'personChronic',
    'PERSON_DISABILITY': 'personDisability',
    'PERSON_EXAMINATION': 'personExamination',
    'PERSON_FAMILY_HISTORY': 'personfamilyHistory',
    'PERSON_HOSPITALIZATION': 'personHospitalization',
    'PERSON_IMMUNIZATION': 'personImmunization',
    'PERSON_INSURANCE': 'personInsurance',
    'PERSON_SEXUAL_INFO': 'personSexualinfo',
    'PERSON_ABUSE': 'personAbuse',
    'PERSON_ALLERGIES': 'personAllergies'
  };
}
