import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BirthInformationCwComponent } from './birth-information-cw.component';

describe('BirthInformationCwComponent', () => {
  let component: BirthInformationCwComponent;
  let fixture: ComponentFixture<BirthInformationCwComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BirthInformationCwComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BirthInformationCwComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
