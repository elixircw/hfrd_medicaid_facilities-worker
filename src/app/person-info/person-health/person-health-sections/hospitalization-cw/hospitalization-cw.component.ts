import { Component, OnInit, Input } from '@angular/core';
import { DataStoreService } from '../../../../@core/services/data-store.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService, CommonHttpService, ValidationService } from '../../../../@core/services';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs';
import { DropdownModel } from '../../../../@core/entities/common.entities';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Subject } from 'rxjs';
import { Hospitalization, Health } from '../../../../pages/case-worker/dsds-action/involved-persons/_entities/involvedperson.data.model';
import { CaseWorkerUrlConfig } from '../../../../pages/case-worker/case-worker-url.config';
import { InvolvedPersonsConstants } from '../../../../pages/case-worker/dsds-action/involved-persons/_entities/involvedPersons.constants';

@Component({
  selector: 'hospitalization-cw',
  templateUrl: './hospitalization-cw.component.html',
  styleUrls: ['./hospitalization-cw.component.scss']
})
export class HospitalizationCwComponent implements OnInit {
  hosptializationForm: FormGroup;
  editMode: boolean;
  reportMode: string;
  minDate = new Date();
  maxDate = new Date();
  modalInt: number;
  hospitalcw: Hospitalization[] = [];
  health: Health = {};
  constants = InvolvedPersonsConstants.Intake.PersonsInvolved.Health;
  stateDropdownItems$: Observable<DropdownModel[]>;
  countyDropDownItems$: Observable<DropdownModel[]>;
  hosptializationType$: Observable<DropdownModel[]>;
  hosptializationReason$: Observable<DropdownModel[]>;

  constructor(
    private _formBuilder: FormBuilder,
    private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService) { }

    ngOnInit() {
      this.reportMode = 'add';
      this.loadDropDowns();
      this.initForm();
    }

  initForm() {
    this.hosptializationForm = this._formBuilder.group({
    'hosptializationType': '',
      'hosptializationReason' : '',
      'reasonOrDiagnosis' : '',
      'hospitalName' : '',
      'phoneNumber': '',
      'address1': '',
      'address2': '',
      'city': '',
      'state': '',
      'zip': '',
      'startdate': '',
      'enddate': ''
    });
  }

  private resetForm() {
    this.hosptializationForm.reset();
    this.editMode = false;
    this.reportMode = 'add';
    this.modalInt = -1;
    this.hosptializationForm.enable();
  }

  private view(modal) {
    this.reportMode = 'edit';
    this.patchForm(modal);
    this.editMode = false;
    this.hosptializationForm.disable();
  }


  private edit(modal, i) {
    this.reportMode = 'edit';
    this.editMode = true;
    this.modalInt = i;
    this.patchForm(modal);
    this.hosptializationForm.enable();
  }

  private delete(index) {
    this.hospitalcw.splice(index, 1);
    this._alertSevice.success('Deleted Successfully');
    this.resetForm();
  }

  private cancel() {
    this.resetForm();
  }

  private patchForm(modal: Hospitalization) {
    this.hosptializationForm.patchValue(modal);
  }

  private add() {
    this.hospitalcw.push(this.hosptializationForm.getRawValue());
    this.health = this._dataStoreService.getData(this.constants.Health);
    this.health.hospitalization = this.hospitalcw;
    this._dataStoreService.setData(this.constants.Health, this.health);
    this._alertSevice.success('Added Successfully');
    this.resetForm();
  }

  private update() {
    if (this.modalInt !== -1) {
      this.hospitalcw[this.modalInt] = this.hosptializationForm.getRawValue();
    }
    this.resetForm();
    this._alertSevice.success('Updated Successfully');
  }

  initializeHospitalization() {
    this.resetForm();
  }

  private loadDropDowns() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '230' , 'delete_sw' : 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '310' , 'delete_sw' : 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      )  ,
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.StateListUrl + '?filter'
      )
    ]).map((result) => {
      return {
        hosptializationTypeValues: result[0].map(
          (res) =>
            new DropdownModel({
              text: res.description_tx,
              value: res.value_tx
            })
        ),
        hosptializationReasonValues: result[1].map(
          (res) =>
            new DropdownModel({
              text: res.description_tx,
              value: res.value_tx
            })
        ),
        states: result[2].map(
          (res) =>
            new DropdownModel({
              text: res.statename,
              value: res.stateabbr
            })
        )
      };
    })
      .share();
      this.hosptializationReason$ = source.pluck('hosptializationReasonValues');
      this.stateDropdownItems$ = source.pluck('states');
      this.hosptializationType$ = source.pluck('hosptializationTypeValues');
  }
}
