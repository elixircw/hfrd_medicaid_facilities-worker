import { Component, OnInit } from '@angular/core';
import { DataStoreService } from '../../../../@core/services/data-store.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertService } from '../../../../@core/services';
import { Health } from '../../../../pages/newintake/my-newintake/_entities/newintakeModel';
import { MyNewintakeConstants } from '../../../../pages/newintake/my-newintake/my-newintake.constants';
import { PersonHealthService } from '../../person-health.service';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'substance-abuse',
  templateUrl: './substance-abuse.component.html',
  styleUrls: ['./substance-abuse.component.scss']
})
export class SubstanceAbuseComponent implements OnInit {

  substanceabuseForm: FormGroup;
  health: Health = {};
  constants = MyNewintakeConstants.Intake.PersonsInvolved.Health;
  alchoholSelectionEnabled: boolean;
  drugSelectionEnabled: boolean;
  drugorAlchoholSelectionEnabled: boolean;
  tobaccoSelectionEnabled: boolean;
  constructor(private formbulider: FormBuilder,
    private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService,
    private _healthService: PersonHealthService) { }

  ngOnInit() {
    this.substanceabuseForm = this.formbulider.group({
      isusetobacco: null,
      isusedrugoralcohol: null,
      isusedrug: null,
      drugfrequencydetails: '',
      drugageatfirstuse: '',
      isusealcohol: null,
      alcoholfrequencydetails: '',
      alcoholageatfirstuse: '',
      drugoralcoholproblems: '',
      tobaccofrequencydetails: '',
      tobaccoageatfirstuse: ''
    });

    this.health = this._dataStoreService.getData(this.constants.Health);
    this.alchoholSelectionEnabled = false;
    this.drugSelectionEnabled = false;
    this.drugorAlchoholSelectionEnabled = false;
    this.tobaccoSelectionEnabled = false;
    if (this.health && this.health.substanceAbuse) {
      this.substanceabuseForm.patchValue(this.health.substanceAbuse);
      this.health.substanceAbuse = this.substanceabuseForm.getRawValue();
      this.drugSelection(this.health.substanceAbuse.isusedrug);
      this.alchoholSelection(this.health.substanceAbuse.isusealcohol);
      this.tobaccoSelection(this.health.substanceAbuse.isusetobacco);
      // this.drugorAlcoholSelection(this.health.substanceAbuse.isusedrugoralcohol);
    }
    this.substanceabuseForm.valueChanges.subscribe(data => {
      // this.health = this._dataStoreService.getData(this.constants.Health);
      // this.health.substanceAbuse = this.substanceabuseForm.getRawValue();
      // this._dataStoreService.setData(this.constants.Health, this.health);
    });
  }

  // drugorAlcoholSelection(control) {
  //   if (control === '1') {
  //     this.drugorAlchoholSelectionEnabled = true;
  //   } else {
  //     this.drugorAlchoholSelectionEnabled = false;
  //     this.resetdrugalcoholfields();
  //     this.resetdrugfields();
  //     this.resetalcoholfields();
  //   }
  // }

  resetdrugalcoholfields() {
    this.substanceabuseForm.patchValue({ 'isusedrug': null });
    this.substanceabuseForm.patchValue({ 'isusealcohol': null });
  }

  resetdrugfields() {
    this.substanceabuseForm.patchValue({ 'drugfrequencydetails': null });
    this.substanceabuseForm.patchValue({ 'drugageatfirstuse': null });
  }

  resetalcoholfields() {
    this.substanceabuseForm.patchValue({ 'alcoholfrequencydetails': null });
    this.substanceabuseForm.patchValue({ 'alcoholageatfirstuse': null });
  }

  resettobaccofields() {
    this.substanceabuseForm.patchValue({ 'tobaccofrequencydetails': null });
    this.substanceabuseForm.patchValue({ 'tobaccoageatfirstuse': null });
  }

  drugSelection(control) {
    if (control) {
      this.drugSelectionEnabled = true;
      this.substanceabuseForm.get('drugfrequencydetails').enable();
      this.substanceabuseForm.get('drugfrequencydetails').setValidators([Validators.required]);
      this.substanceabuseForm.get('drugfrequencydetails').updateValueAndValidity();
      this.substanceabuseForm.get('drugageatfirstuse').enable();
      this.substanceabuseForm.get('drugageatfirstuse').setValidators([Validators.required]);
      this.substanceabuseForm.get('drugageatfirstuse').updateValueAndValidity();

    } else {
      this.resetdrugfields();
      this.drugSelectionEnabled = false;
      this.substanceabuseForm.get('drugfrequencydetails').disable();
      this.substanceabuseForm.get('drugfrequencydetails').clearValidators();
      this.substanceabuseForm.get('drugfrequencydetails').updateValueAndValidity();
      this.substanceabuseForm.get('drugageatfirstuse').disable();
      this.substanceabuseForm.get('drugageatfirstuse').clearValidators();
      this.substanceabuseForm.get('drugageatfirstuse').updateValueAndValidity();
    }
  }

  alchoholSelection(control) {
    if (control) {
      this.alchoholSelectionEnabled = true;
      this.substanceabuseForm.get('alcoholfrequencydetails').enable();
      this.substanceabuseForm.get('alcoholfrequencydetails').setValidators([Validators.required]);
      this.substanceabuseForm.get('alcoholfrequencydetails').updateValueAndValidity();
      this.substanceabuseForm.get('alcoholageatfirstuse').enable();
      this.substanceabuseForm.get('alcoholageatfirstuse').setValidators([Validators.required]);
      this.substanceabuseForm.get('alcoholageatfirstuse').updateValueAndValidity();
    } else {
      this.resetalcoholfields();
      this.alchoholSelectionEnabled = false;
      this.substanceabuseForm.get('alcoholfrequencydetails').disable();
      this.substanceabuseForm.get('alcoholfrequencydetails').clearValidators();
      this.substanceabuseForm.get('alcoholfrequencydetails').updateValueAndValidity();
      this.substanceabuseForm.get('alcoholageatfirstuse').disable();
      this.substanceabuseForm.get('alcoholageatfirstuse').clearValidators();
      this.substanceabuseForm.get('alcoholageatfirstuse').updateValueAndValidity();
    }
  }

  tobaccoSelection(control) {
    if (control) {
      this.tobaccoSelectionEnabled = true;
      this.substanceabuseForm.get('tobaccofrequencydetails').enable();
      this.substanceabuseForm.get('tobaccofrequencydetails').setValidators([Validators.required]);
      this.substanceabuseForm.get('tobaccofrequencydetails').updateValueAndValidity();
      this.substanceabuseForm.get('tobaccoageatfirstuse').enable();
      this.substanceabuseForm.get('tobaccoageatfirstuse').setValidators([Validators.required]);
      this.substanceabuseForm.get('tobaccoageatfirstuse').updateValueAndValidity();
    } else {
      this.resettobaccofields();
      this.tobaccoSelectionEnabled = false;
      this.substanceabuseForm.get('tobaccofrequencydetails').disable();
      this.substanceabuseForm.get('tobaccofrequencydetails').clearValidators();
      this.substanceabuseForm.get('tobaccofrequencydetails').updateValueAndValidity();
      this.substanceabuseForm.get('tobaccoageatfirstuse').disable();
      this.substanceabuseForm.get('tobaccoageatfirstuse').clearValidators();
      this.substanceabuseForm.get('tobaccoageatfirstuse').updateValueAndValidity();
    }
  }

  resetForm() {
    this.substanceabuseForm.reset();
    this.drugSelection(false);
    this.alchoholSelection(false);
    // this.drugorAlcoholSelection(false);
    this.tobaccoSelection(false);
  }
  save() {
    this.health = this._dataStoreService.getData(this.constants.Health);
    this.health.substanceAbuse = this.substanceabuseForm.getRawValue();
    this._dataStoreService.setData(this.constants.Health, this.health);
    const data = this.substanceabuseForm.getRawValue();
    this._healthService.saveHealth({ 'personHealthSubstanceAbuse': [data] }).subscribe(_ => {
      this._alertSevice.success('Saved Successfully');
  });
}
}
