import { Component, OnInit } from '@angular/core';
import { PersonChronic, Health } from '../../../../pages/case-worker/dsds-action/involved-persons/_entities/involvedperson.data.model';
import { InvolvedPersonsConstants } from '../../../../pages/case-worker/dsds-action/involved-persons/_entities/involvedPersons.constants';
import { CommonHttpService, AlertService, DataStoreService } from '../../../../@core/services';
import { FormGroup, FormBuilder } from '@angular/forms';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { CaseWorkerUrlConfig } from '../../../../pages/case-worker/case-worker-url.config';
import { DropdownModel } from '../../../../@core/entities/common.entities';
import { Observable } from 'rxjs';
import { PersonHealthService } from '../../person-health.service';
import { HealthConstants } from '../health-constants';

export class PersonElimination
{
  infoprovidedby: string;
  clientlist: string;
  collaterallist: string;
  infoprovidedname: string;
  relationship: string;
  eliminationinfounknown: boolean;
  currentstatus: string;
  toilettraining: string;
  bowelmovement: string;
  urination: string;
  toiletcomments: string;
  considerunknown: string;
  specialcomments:string;
}
@Component({
  selector: 'elimination',
  templateUrl: './elimination.component.html',
  styleUrls: ['./elimination.component.scss']
})
export class EliminationComponent implements OnInit {

  EliminationInfoForm: FormGroup;
  editMode: boolean;
  reportMode: string;
  modalInt: number;
  eliminationcw = [];
  health: Health = {};
  StatusList$: Observable<DropdownModel[]>;
  TrainingList$: Observable<DropdownModel[]>;

  constructor(
    private formbuilder: FormBuilder,
    private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService,
    private _healthService: PersonHealthService) {}

  ngOnInit() {
    this.reportMode = 'add';
    this.loadDropDowns();
    this.initForm();
   }



  private resetForm() {
    this.EliminationInfoForm.reset();
    this.editMode = false;
    this.reportMode = 'add';
    this.modalInt = -1;
    this.EliminationInfoForm.enable();
  }

  private view(modal) {
    this.reportMode = 'edit';
    this.patchForm(modal);
    this.editMode = false;
    this.EliminationInfoForm.disable();
  }

  private edit(modal, i) {
    this.reportMode = 'edit';
    this.editMode = true;
    this.modalInt = i;
    this.patchForm(modal);
    this.EliminationInfoForm.enable();
  }

  private delete(index) {
    this.eliminationcw.splice(index, 1);
    this._alertSevice.success('Deleted Successfully');
    this.resetForm();
  }

  private cancel() {
    this.resetForm();
  }

  private patchForm(modal: PersonElimination) {
    this.EliminationInfoForm.patchValue(modal);
  }

  add() {
    const eliminateinfo = this.EliminationInfoForm.getRawValue();
    this.eliminationcw.push(eliminateinfo);
    // this.health = this._dataStoreService.getData(this.constants.Health);
    // this.health.personChronic = this.eliminationcw;
    // this._dataStoreService.setData(this.constants.Health, this.health);
    this._healthService.saveHealth({ 'personElimination': [eliminateinfo] }).subscribe(response => {
      this._alertSevice.success('Added Successfully');
    });
  }

    initForm() {
    this.reportMode = 'add';
    this.EliminationInfoForm = this.formbuilder.group({
      infoprovidedby: '',
      clientlist: '',
      collaterallist: '',
      infoprovidedname: '',
      relationship: '',
      eliminationinfounknown: false,
      currentstatus: '',
      toilettraining: '',
      bowelmovement: '',
      urination: '',
      toiletcomments: '',
      considerunknown: '',
      specialcomments: ''
    });
  }

  private loadDropDowns() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '59', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '342', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      )])
      .map((result) => {
        return {
          StatusList: result[0].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.value_tx
              })
          ),
          TrainingList: result[1].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.value_tx
              })
          )
        };
      })
      .share();
    this.StatusList$ = source.pluck('StatusList');
    this.TrainingList$ = source.pluck('TrainingList');
  }
}
