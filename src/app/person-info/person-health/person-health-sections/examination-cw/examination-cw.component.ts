import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { CommonHttpService, AlertService, DataStoreService } from '../../../../@core/services';
import { CaseWorkerUrlConfig } from '../../../../pages/case-worker/case-worker-url.config';
import { DropdownModel } from '../../../../@core/entities/common.entities';
import { PersonExamination, Health } from '../../../../pages/case-worker/dsds-action/involved-persons/_entities/involvedperson.data.model';
import { InvolvedPersonsConstants } from '../../../../pages/case-worker/dsds-action/involved-persons/_entities/involvedPersons.constants';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs';
import { PersonHealthService } from '../../person-health.service';
import { HealthConstants } from '../health-constants';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'examination-cw',
  templateUrl: './examination-cw.component.html',
  styleUrls: ['./examination-cw.component.scss']
})
export class ExaminationCwComponent implements OnInit {

  examinationInfoForm: FormGroup;
  // examTypeList: any[];
  // specialityExamTypeList: any[];
  // labTestList: any[];
  editMode: boolean;
  reportMode: string;
  modalInt: number;
  examinationcw: PersonExamination[] = [];
  health: Health = {};
  constants = InvolvedPersonsConstants.Intake.PersonsInvolved.Health;
  examTypeDropdownItems$: Observable<DropdownModel[]>;
  specialityExamTypeDropdownItems$: Observable<DropdownModel[]>;
  labTestDropdownItems$: Observable<DropdownModel[]>;
  stateDropdownItems$: Observable<DropdownModel[]>;
  countyDropDownItems$: Observable<DropdownModel[]>;
 

  constructor(private formbulider: FormBuilder,
    private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService,
    private _healthService: PersonHealthService
  ) {

  }

  ngOnInit() {
    this.reportMode = 'add';
    this.loadDropDown();
    this.examinationInfoForm = this.formbulider.group({
       apptDate: '',
       nextApptDate: '',
       natureofexamkey: ['', Validators.required],
       labtestkey: '',
       specialityexamkey: '',
       apptkept: '',
       hivtestreceived: '',
       speciality: '',
       affilication: '',
       physicianName: '',
       recommendations: '',
       comments: '',
       address1: '',
       address2: '',
       city: '',
       state: '',
       county: '',
       zip: '',
       phone: '',
       email: ''

    });
    const list = this._healthService.getHealthInfoWithKey(HealthConstants.LIST_KEY.PERSON_EXAMINATION);
    if (list && Array.isArray(list) && list.length) {
      this.examinationcw = list;
    }
  }

  private resetForm() {

    this.examinationInfoForm.reset();
    this.modalInt = -1;
    this.editMode = false;
    this.reportMode = 'add';
    this.examinationInfoForm.enable();

  }


  private view(modal) {
    this.reportMode = 'edit';
    this.patchForm(modal);
    this.editMode = false;
    this.examinationInfoForm.disable();
  }

  private edit(modal, i) {
    this.reportMode = 'edit';
    this.editMode = true;
    this.modalInt = i;
    this.patchForm(modal);
    this.examinationInfoForm.enable();
  }

  private delete(index) {
    this.examinationcw.splice(index, 1);
    this._alertSevice.success('Deleted Examination Successfully');
    this.resetForm();
  }

  private cancel() {
    this.resetForm();
  }

  private add() {

     if (this.examinationInfoForm.value.natureofexamkey) {
      this.examinationcw.push(this.examinationInfoForm.getRawValue());
      const data = this.examinationInfoForm.getRawValue();
      this.health = this._dataStoreService.getData(this.constants.Health);
      this.health.personExamination = this.examinationcw;
      this._dataStoreService.setData(this.constants.Health, this.health);
      this._healthService.saveHealth({ 'personExamination': [data] }).subscribe(_ => {
      this._alertSevice.success('Added Examination Successfully');
      this.resetForm();
      });
    } else {
      this._alertSevice.error('Please enter Nature of Exam');
     }
  }

  private update() {
    if (this.modalInt !== -1) {
      this.examinationcw[this.modalInt] = this.examinationInfoForm.getRawValue();
    }
    this.resetForm();
    this._alertSevice.success('Updated Examination Successfully');
    // this._alertSevice.error('Error in Examination Edit. Please enter Nature of Exam');
  }

  private loadDropDowns() {
    // this.getExamTypeList();
    // this.getSpecialityExamTypeList();
    // this.getLabTestList();
    // const source = forkJoin([
    //   this._commonHttpService.getArrayList(
    //     {
    //       where: { activeflag: 1 },
    //       method: 'get',
    //       nolimit: true
    //     },
    //     CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
    //   ),
    //   this._commonHttpService.getArrayList(
    //     {
    //       method: 'get',
    //       nolimit: true
    //     },
    //     CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
    //   ),
    //   this._commonHttpService.getArrayList(
    //     {
    //       method: 'get',
    //       nolimit: true,
    //       order: 'description ASC'
    //     },
    //     CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
    //   ),
    // ])
    //   .map((result) => {
    //     result[2].forEach(type => {
    //       // this.typeofservice[type.personservicetypekey] = type.description;
    //     });
    //     return {
    //       ethinicities: result[0].map(
    //         (res) =>
    //           new DropdownModel({
    //             text: res.typedescription,
    //             value: res.ethnicgrouptypekey
    //           })
    //       ),
    //       states: result[1].map(
    //         (res) =>
    //           new DropdownModel({
    //             text: res.statename,
    //             value: res.stateabbr
    //           })
    //       ),
    //       typeofservice: result[2].map(
    //         (res) =>
    //           new DropdownModel({
    //             text: res.description,
    //             value: res.personservicetypekey
    //           })
    //       ),
    //     };
    //   })
    //   .share();
    // this.countyDropDownItems$ = source.pluck('counties');
    // this.ethinicityDropdownItems$ = source.pluck('ethinicities');
    // this.stateDropdownItems$ = source.pluck('states');
    // this.typeofservice$ = source.pluck('typeofservice');
  }

  private patchForm(modal: PersonExamination) {
    this.examinationInfoForm.patchValue(modal);

    // if (modal.startdate) {
    //   this.examinationInfoForm.patchValue({startdate: new Date(modal.startdate)});
    // }
    // if (modal.enddate) {
    //   this.examinationInfoForm.patchValue({enddate: new Date(modal.enddate)});
    // }
  }


  // getExamTypeList() {
  //   this._commonHttpService.getArrayList(
  //     {
  //       where: { activeflag: 1, 'picklist_type_id': '320' },
  //       method: 'get',
  //       nolimit: true
  //     },
  //     CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter').subscribe(result => {
  //       this.examTypeList = result;
  //   });
  // }

  // getSpecialityExamTypeList() {
  //   this._commonHttpService.getArrayList(
  //     {
  //       where: { activeflag: 1, 'picklist_type_id': '318' },
  //       method: 'get',
  //       nolimit: true
  //     },
  //     CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter').subscribe(result => {
  //       this.specialityExamTypeList = result;
  //   });
  // }

  // getLabTestList() {
  //   this._commonHttpService.getArrayList(
  //     {
  //       where: { activeflag: 1, 'picklist_type_id': '319' },
  //       method: 'get',
  //       nolimit: true
  //     },
  //     CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter').subscribe(result => {
  //       this.labTestList = result;
  //   });
  // }

  private loadDropDown() {
    const source = forkJoin([
        this._commonHttpService.getArrayList(
            {
                method: 'get',
                nolimit: true,
                where: { 'active_sw': 'Y', 'delete_sw' : 'N', 'picklist_type_id': '320' }
            },
            CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
        ),
        this._commonHttpService.getArrayList(
            {
                method: 'get',
                nolimit: true,
                where: { 'active_sw': 'Y', 'delete_sw' : 'N', 'picklist_type_id': '318' }
            },
            CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
        ),
        this._commonHttpService.getArrayList(
            {
                method: 'get',
                nolimit: true,
                where: { 'active_sw': 'Y', 'delete_sw' : 'N', 'picklist_type_id': '319' }
            },
            CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
        ),
        this._commonHttpService.getArrayList(
          {
            method: 'get',
            nolimit: true
          },
          CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.StateListUrl + '?filter'
        ),
        this._commonHttpService.getArrayList(
          {
            method: 'get',
            nolimit: true,
            order: 'countyname asc'
          },
          CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.CountyList + '?filter'
        )
    ])
        .map((result) => {
            return {
              examTypeList: result[0].map(
                    (res) =>
                        new DropdownModel({
                            text: res.description_tx,
                            value: res.value_tx
                        })
                ),
                specialityExamTypeList: result[1].map(
                    (res) =>
                        new DropdownModel({
                          text: res.description_tx,
                          value: res.value_tx
                        })
                ),
                labTestList: result[2].map(
                    (res) =>
                        new DropdownModel({
                          text: res.description_tx,
                          value: res.value_tx
                        })
                ),
                states: result[3].map(
                  (res) =>
                    new DropdownModel({
                      text: res.statename,
                      value: res.stateabbr
                    })
                ),
                counties: result[4].map(
                  (res) =>
                    new DropdownModel({
                      text: res.countyname,
                      value: res.countyname
                    })
                )
            };
        })
        .share();
    this.examTypeDropdownItems$ = source.pluck('examTypeList');
    this.specialityExamTypeDropdownItems$ = source.pluck('specialityExamTypeList');
    this.labTestDropdownItems$ = source.pluck('labTestList');
    this.stateDropdownItems$ = source.pluck('states');
    this.countyDropDownItems$ = source.pluck('counties');
}

}
