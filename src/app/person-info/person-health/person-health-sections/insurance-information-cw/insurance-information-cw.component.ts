import { Component, OnInit } from '@angular/core';
import { DataStoreService } from '../../../../@core/services/data-store.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertService, CommonHttpService } from '../../../../@core/services';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs';
import { DropdownModel } from '../../../../@core/entities/common.entities';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Health, HealthInsuranceInformation } from '../../../../pages/case-worker/dsds-action/involved-persons/_entities/involvedperson.data.model';
import { InvolvedPersonsConstants } from '../../../../pages/case-worker/dsds-action/involved-persons/_entities/involvedPersons.constants';
import { CaseWorkerUrlConfig } from '../../../../pages/case-worker/case-worker-url.config';
import { PersonHealthService } from '../../person-health.service';
import { HealthConstants } from '../health-constants';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'insurance-information-cw',
  templateUrl: './insurance-information-cw.component.html',
  styleUrls: ['./insurance-information-cw.component.scss']
})
export class InsuranceInformationCwComponent implements OnInit {
  healthinsuranceForm: FormGroup;
  modalInt: number;
  isCustomInsuranceProvider: boolean;
  editMode: boolean;
  reportMode: string;
  minDate = new Date();
  maxDate = new Date();
  ethinicityDropdownItems$: Observable<DropdownModel[]>;
  stateDropdownItems$: Observable<DropdownModel[]>;
  countyDropDownItems$: Observable<DropdownModel[]>;
  healthinsurance: HealthInsuranceInformation[] = [];
  health: Health;
  isPrimaryExists: boolean;
  isPrimaryInsurance: boolean;
  isInsuranceAvailableforPerson: boolean;
  insuranceList: any[];
  insuranceTypeList: any[];
  // constants = InvolvedPersonsConstants.Intake.PersonsInvolved.Health;

  medAssistance: boolean;
  constructor(private formbulider: FormBuilder,
    private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService,
    private _healthService: PersonHealthService) {
  }

  ngOnInit() {
    this.loadDropDowns();
    this.initForm();
    this.medAssistance = false;
    this.editMode = false;
    this.reportMode = 'add';
    this.isPrimaryExists = false;
    this.isPrimaryInsurance = true;
    this.isInsuranceAvailableforPerson = false;
    this.modalInt = -1;
    this.isCustomInsuranceProvider = false;
    const list = this._healthService.getHealthInfoWithKey(HealthConstants.LIST_KEY.PERSON_INSURANCE);
    if (list && Array.isArray(list) && list.length) {
      this.healthinsurance = list;
    }
  }

  initForm() {
    this.healthinsuranceForm = this.formbulider.group({
      insurancetype: '',
      medicalinsuranceprovider: '',
      providertype: '',
      policyholdername: '',
      isinsuranceavailable: [''],
      customprovidertype: '',
      address1: '',
      address2: '',
      city: '',
      state: '',
      county: '',
      zip: '',
      providerphone: '',
      patientpolicyholderrelation: '',
      policynumber: '',
      groupnumber: '',
      startdate: '',
      enddate: '',
      updateddate: ''
    });

    const Index = this.healthinsurance.findIndex(c => c.providertype === 'Primary');
      if(Index !== -1) {
        this.isPrimaryExists = true;
      } else {
        this.isPrimaryExists = false;
      }
  }
        // console.log('health', this.health);

        // if (this.health.healthInsurance) {
        //   this.healthinsuranceForm.patchValue(this.health.healthInsurance);
        // }

        // this.healthinsuranceForm.valueChanges.subscribe(physician => {
        //   this._dataStoreService.setData('health_insurance', this.healthinsuranceForm.getRawValue());
        // });


        //   <mat-option value="Medicaid"> Medicaid</mat-option>
        // <mat-option value="Medicare"> Medicare</mat-option>
        // <mat-option value="Private"> Private</mat-option>

  setInsurance(option) {
    switch (option.value) {
      case 'Medicaid': this.isPrimaryInsurance = true;
        this.healthinsuranceForm.patchValue({ 'providertype': 'Primary' });
        break;
      case 'Medicare': this.isPrimaryInsurance = true;
        this.healthinsuranceForm.patchValue({ 'providertype': 'Primary' });
        break;
      case 'Private': this.isPrimaryInsurance = false;
        this.healthinsuranceForm.patchValue({ 'providertype': 'Primary' });
        break;
      default: this.isPrimaryInsurance = false;
    }
  }
  setInsuranceType(option) {
    switch (option) {
      case 'Primary': this.isPrimaryExists = true;
        break;
    }
  }
  resetInsuranceType(option) {
    switch (option) {
      case 'Primary': this.isPrimaryExists = false;
        break;
    }
  }
  modifyInsuranceType(option) {
    switch (option) {
      case 'Secondary': this.isPrimaryExists = false;
        break;
    }
  }
    private setProviderType(option) {
    if (option.value === 'other' || option === 'other') {
      this.isCustomInsuranceProvider = true;
    } else {
      this.isCustomInsuranceProvider = false;
    }
  }

    private loadDropDowns() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          where: { activeflag: 1 },
          method: 'get',
          nolimit: true
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.EthnicGroupTypeUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.StateListUrl + '?filter'
      )
    ])
      .map((result) => {
        return {
          ethinicities: result[0].map(
            (res) =>
              new DropdownModel({
                text: res.typedescription,
                value: res.ethnicgrouptypekey
              })
          ),
          states: result[1].map(
            (res) =>
              new DropdownModel({
                text: res.statename,
                value: res.stateabbr
              })
          )
        };
      })
      .share();
    // this.countyDropDownItems$ = source.pluck('counties');
    this.ethinicityDropdownItems$ = source.pluck('ethinicities');
    this.stateDropdownItems$ = source.pluck('states');
  }

  loadCounty() {
    const state = this.healthinsuranceForm.get('state').value;
    const source = this._commonHttpService.getArrayList(
      {
        where: { state: state },
        order: 'countyname asc',
        method: 'get',
        nolimit: true
      },
      CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.CountyList + '?filter'
    ).map((result) => {
      return {
        counties: result.map(
          (res) =>
            new DropdownModel({
              text: res.countyname,
              value: res.countyid
            })
        )
      };
    }).share();

    this.countyDropDownItems$ = source.pluck('counties');
  }

  selectmedicalassistance(control) {
    this.medAssistance = control;
  }
    private add() {
    const currDate = new Date();
    this.healthinsuranceForm.patchValue({ 'updateddate': currDate });
    let insuranceForm = this.healthinsuranceForm.getRawValue();
    // insuranceForm["insurancetype"] = insuranceForm["insurancetype"];
    if (insuranceForm['insurancetype'] === 'Medicare' || insuranceForm['insurancetype'] === ' Medicaid') {
      insuranceForm['insurancetype'] = true;
    }
    else {
      insuranceForm['insurancetype'] = false;
    }

    // if()
    this.setInsuranceType(insuranceForm.providertype);
    this.healthinsurance.push(insuranceForm);
    //this.healthinsurance.push(insuranceForm);
    //this.health = this._dataStoreService.getData(this.constants.Health);
    //this.health.healthInsurance = this.healthinsurance;
    //this._dataStoreService.setData(this.constants.Health, this.health);
    this._healthService.saveHealth({ 'insuranceInfo': [this.healthinsurance] }).subscribe(response => {
      this._alertSevice.success('Added Successfully');
    });
    this.resetForm();
  }

    private enableorDisableField(field, opt) {
    if (opt) {
      this.healthinsuranceForm.get(field).enable();
      // this.healthinsuranceForm.get(field).setValidators([Validators.required]);
      this.healthinsuranceForm.get(field).updateValueAndValidity();
    } else {
      this.healthinsuranceForm.get(field).disable();
      this.healthinsuranceForm.get(field).clearValidators();
      this.healthinsuranceForm.get(field).updateValueAndValidity();
    }
  }

  isInsuranceExsists(opt) {

    this.enableorDisableField('insurancetype', opt);
    this.enableorDisableField('medicalinsuranceprovider', opt);
    this.enableorDisableField('customprovidertype', opt);
    this.enableorDisableField('providertype', opt);
    this.enableorDisableField('policynumber', opt);
    this.enableorDisableField('startdate', opt);
    this.enableorDisableField('groupnumber', opt);
    this.isInsuranceAvailableforPerson = opt;
    this.healthinsuranceForm.reset();
    this.healthinsuranceForm.patchValue({ 'isinsuranceavailable': opt });
  }

    private resetForm() {
    this.healthinsuranceForm.reset();
    this.isInsuranceAvailableforPerson = false;
    this.modalInt = -1;
    this.editMode = false;
    this.reportMode = 'add';
    this.healthinsuranceForm.enable();
    this.isCustomInsuranceProvider = false;
    this.medAssistance = false;
  }

    private update() {
    if (this.modalInt !== -1) {
      const currDate = new Date();
      this.healthinsuranceForm.patchValue({ 'updateddate': currDate });
      const insuranceForm = this.healthinsuranceForm.getRawValue();
      this.setInsuranceType(insuranceForm.providertype);
      this.healthinsurance[this.modalInt] = this.healthinsuranceForm.getRawValue();
    }
    this.resetForm();
    this._alertSevice.success('Updated Successfully');
  }

    private view(modal) {
    const isInsurance = modal.isinsuranceavailable === 'true' ? true : false;
    this.isInsuranceExsists(isInsurance);
    this.setProviderType(modal.medicalinsuranceprovider);
    this.resetInsuranceType(modal.providertype);
    this.medAssistance = modal.medicalassistance;
    this.reportMode = 'edit';
    this.editMode = false;
    this.patchForm(modal);
    this.healthinsuranceForm.disable();
  }

    private edit(modal, i) {
    const isInsurance = modal.isinsuranceavailable === 'true' ? true : false;
    this.isInsuranceExsists(isInsurance);
    this.resetInsuranceType(modal.providertype);
    this.setProviderType(modal.medicalinsuranceprovider);
    this.medAssistance = modal.medicalassistance;
    this.reportMode = 'edit';
    this.editMode = true;
    this.modalInt = i;
    this.patchForm(modal);
    this.healthinsuranceForm.enable();
    this.enableorDisableField('isinsuranceavailable', false);
  }

    private delete (index) {
    const insuranceForm = this.healthinsurance[index];
    this.resetInsuranceType(insuranceForm.providertype);
    this.healthinsurance.splice(index, 1);
    this._alertSevice.success('Deleted Successfully');
    this.resetForm();
  }

    private cancel() {
    this.resetForm();
  }


    private startDateChanged() {
    this.healthinsuranceForm.patchValue({ enddate: '' });
    const empForm = this.healthinsuranceForm.getRawValue();
    this.maxDate = new Date(empForm.enddate);
  }
    private endDateChanged() {
    this.healthinsuranceForm.patchValue({ startdate: '' });
    const empForm = this.healthinsuranceForm.getRawValue();
    this.minDate = new Date(empForm.startdate);
  }

    private patchForm(modal: HealthInsuranceInformation) {
    this.healthinsuranceForm.patchValue(modal);
  }
}
