
export class ImmunizationConstants {
    public static headerInfo = [{
        'title': 'Vaccine',
        'ageType': 'BIRTH_TO_15'
    },
    {
        'title': 'At Birth',
        'ageType': 'BIRTH_TO_15'
    },
    {
        'title': '1 Mo',
        'ageType': 'BIRTH_TO_15'
    },
    {
        'title': '2 Mos',
        'ageType': 'BIRTH_TO_15'
    },
    {
        'title': '3 Mos',
        'ageType': 'BIRTH_TO_15'
    },
    {
        'title': '6 Mos',
        'ageType': 'BIRTH_TO_15'
    },
    {
        'title': '9 Mos',
        'ageType': 'BIRTH_TO_15'
    },
    {
        'title': '12 Mos',
        'ageType': 'BIRTH_TO_15'
    },
    {
        'title': '15 Mos',
        'ageType': 'BIRTH_TO_15'
    },
    {
        'title': 'Vaccine',
        'ageType': 'MONTH_18_TO_YEAR_18'
    },
    {
        'title': '18 Mos',
        'ageType': 'MONTH_18_TO_YEAR_18'
    },
    {
        'title': '19-23 Mos',
        'ageType': 'MONTH_18_TO_YEAR_18'
    },
    {
        'title': '2-3 yrs',
        'ageType': 'MONTH_18_TO_YEAR_18'
    },
    {
        'title': '4-6 yrs',
        'ageType': 'MONTH_18_TO_YEAR_18'
    },
    {
        'title': '7-10 yrs',
        'ageType': 'MONTH_18_TO_YEAR_18'
    },
    {
        'title': '11-12 yrs',
        'ageType': 'MONTH_18_TO_YEAR_18'
    },
    {
        'title': '13-15 yrs',
        'ageType': 'MONTH_18_TO_YEAR_18'
    },
    {
        'title': '16 yrs',
        'ageType': 'MONTH_18_TO_YEAR_18'
    },
    {
        'title': '17-18 yrs',
        'ageType': 'MONTH_18_TO_YEAR_18'
    }];
}