import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CommonHttpService, AlertService, DataStoreService } from '../../../../@core/services';
import { PersonImmunization, Health } from '../../../../pages/case-worker/dsds-action/involved-persons/_entities/involvedperson.data.model';
import { InvolvedPersonsConstants } from '../../../../pages/case-worker/dsds-action/involved-persons/_entities/involvedPersons.constants';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { CaseWorkerUrlConfig } from '../../../../pages/case-worker/case-worker-url.config';
import { DropdownModel } from '../../../../@core/entities/common.entities';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs';
import { ImmunizationConstants } from './config';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'immunization-cw',
  templateUrl: './immunization-cw.component.html',
  styleUrls: ['./immunization-cw.component.scss']
})
export class ImmunizationCwComponent implements OnInit {
  immunizationInfoForm: FormGroup;
  isImmunizedforPerson: boolean;
  immunizationHeaderStructure = [];
  immunizationDataStructure = [];
  immunizationTypes = [];
  ageType = 'BIRTH_TO_15';


  constructor(
    private _formBuilder: FormBuilder,
    private _commonHttpService: CommonHttpService) { }

  ngOnInit() {
    this.isImmunizedforPerson = false;
    this.processHeader();
    this.loadDropDowns();
    this.initForm();
  }

  processHeader() {
    this.immunizationHeaderStructure = ImmunizationConstants.headerInfo.filter(header => header.ageType === this.ageType);
    console.log('headerInfo', this.immunizationHeaderStructure);

  }
  updateData() {
    this.processHeader();
    this.processImmunizationDataStructure({});
  }

  processImmunizationDataStructure(data) {
    this.immunizationDataStructure = [];
    this.immunizationTypes.forEach(immunizationType => {
      const immunizationTypeRow = this.getImmunizationTypeRow(immunizationType);
      this.immunizationDataStructure.push(immunizationTypeRow);
    });
    console.log('data structure', this.immunizationDataStructure);
  }

  getImmunizationTypeRow(immunizationType) {
    const columns = [];


    this.immunizationHeaderStructure.forEach(header => {
      const column = {
        template: '',
        value: ''
      };
      if (header.title === 'Vaccine') {
        column.template = 'vaccine-type';
        column.value = immunizationType;
      } else {
        column.template = 'type1';
        column.value = header.title + 'value';
      }

      columns.push(column);
    });
    return columns;
  }

  initForm() {
    this.immunizationInfoForm = this._formBuilder.group({
      isimmunized: [''],
      immunizationkey: '',
      date: '',
      duedate: '',
      comments: ''
    });
  }

  isImmunizedExists(opt) {

    this.enableorDisableField('immunizationkey', opt);
    this.enableorDisableField('date', opt);
    this.enableorDisableField('duedate', opt);
    this.enableorDisableField('comments', opt);
    this.isImmunizedforPerson = opt;
    this.immunizationInfoForm.reset();
    this.immunizationInfoForm.patchValue({ 'isinsuranceavailable': opt });
  }
  private enableorDisableField(field, opt) {
    if (opt) {
      this.immunizationInfoForm.get(field).enable();
    }
  }

  private loadDropDowns() {
    this._commonHttpService.getArrayList(
      {
        method: 'get',
        nolimit: true,
        where: { 'active_sw': 'Y', 'picklist_type_id': '231', 'delete_sw': 'N' }
      },
      CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
    ).subscribe(immunizationTypes => {
      if (immunizationTypes && Array.isArray(immunizationTypes)) {
        this.immunizationTypes = immunizationTypes;
        console.log(this.immunizationTypes);
        this.processImmunizationDataStructure({});
      }

    });
  }
}
