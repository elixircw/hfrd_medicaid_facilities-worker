import { Component, OnInit, OnDestroy } from '@angular/core';
import { PersonDisabilityService } from '../../../../pages/shared-pages/person-disability/person-disability.service';
import { DataStoreService, AlertService } from '../../../../@core/services';
import { GUARDIANSHIP } from '../../../../pages/case-worker/dsds-action/involved-persons/guardianship/_entities/guardianship.const';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'person-health-disability',
  templateUrl: './person-health-disability.component.html',
  styleUrls: ['./person-health-disability.component.scss']
})
export class PersonHealthDisabilityComponent implements OnInit, OnDestroy {


  personDisabilities = [];
  personId: any;
  subscription: Subscription;
  personDisabilityForm: FormGroup;
  dateStart: boolean;
  disabilityTypes: any = [];
  disabilityConditions: any = [];
  disablityFlag = false;
  hasDisability = false;
  constructor(private _service: PersonDisabilityService,
    private _dataStoreService: DataStoreService,
    private _router: Router,
    private route: ActivatedRoute,
    private _alertService: AlertService,
    private _formBuilder: FormBuilder) { }

  ngOnInit() {
    this.personId = this._dataStoreService.getData(GUARDIANSHIP.PersonId);
    this.loadPersonDisabilityList();
    this.listenForPersonDisability();
    this.initForm();
    this.loadDropDowns();
  }

  initForm() {
    this.personDisabilityForm = this._formBuilder.group({
      personid: [this.personId],
      disabilityconditiontypekey: [null, Validators.required],
      diagnoiseddisabilitynotes: [null],
      startdate: [null, Validators.required],
      enddate: [null],
      disabilityflag: [null],
      evaluationdate: [null],
      evaluatorname: [null],
      disabilitytypekey: [null, Validators.required],
      comments: [null]
    });
  }

  loadDropDowns() {
    this._service.getDisabilityTypes().subscribe(response => {
      if (response && Array.isArray(response)) {
        this.disabilityTypes = response;
        this.setDisabiltyFields();
      }
    });
    this.disabilityConditions = [{
      'value_text': 'Yes',
      'description': 'Yes'
    },
    {
      'value_text' : 'No',
      'description': 'No'
    },
    {
      'value_text' : 'Unknown',
      'description': 'Unknown'
    }];
   /*  this._service.getDisabilityConditions().subscribe(response => {
      if (response && Array.isArray(response)) {
        this.disabilityConditions = response;
      }
    }); */
  }



  clearStartdate(event: any) {
    if (event) {
      this.personDisabilityForm.get('startdate').clearValidators();
      this.personDisabilityForm.get('startdate').updateValueAndValidity();
      this.dateStart = true;
    } else {
      this.personDisabilityForm.get('startdate').setValidators([Validators.required]);
      this.personDisabilityForm.get('startdate').updateValueAndValidity();
      this.dateStart = false;
    }
  }


  saveDisability() {
    console.log('df', this.personDisabilityForm.getRawValue());
    if (this.personDisabilityForm.invalid) {
      this._alertService.error('please fill required fields');
      return;
    }
    const data = this.personDisabilityForm.getRawValue();
    this.clearStartdate(this.personId);
    data.personid = this.personId;
    this._service.createDisability(data).subscribe(response => {
      this.reset();
      this._alertService.success('Disability added successfully', true);
      this.loadPersonDisabilityList();
    });
  }

  loadPersonDisabilityList() {
    this._service.getDisabilityList(this.personId).subscribe(response => {
      if (response && Array.isArray(response) && response.length) {
        this.personDisabilities = response;
        this.setDisabiltyFields();
        if (this.personDisabilities.length > 0) {
          this.hasDisability = true;
        }
      } else {
        this.personDisabilities = [];
        this.setDisabiltyFields();
      }
    });
  }

  openDisabilityForm() {
    this._router.navigate(['disability/' + this.personId + '/create'], { relativeTo: this.route });
  }

  listenForPersonDisability() {
    this.subscription = this._dataStoreService.currentStore.subscribe(store => {
      if (store.SUBSCRIPTION_TARGET === 'DISABILITY') {
        this.loadPersonDisabilityList();
      }
    });
  }

  deleteDisability(disability) {
    this._service.deleteDisability(disability.persondisabilityid).subscribe(response => {
      this._alertService.success('Disability deleted successfully');
      this.loadPersonDisabilityList();
    });
  }

  // ngOnDestroy(): void {
  //   this.subscription.unsubscribe();
  // }

  addDisability(item, disabilitycondition) {
    this.personDisabilityForm.patchValue({ disabilitytypekey: item.ref_key });
    this.personDisabilityForm.patchValue({ disabilityconditiontypekey: disabilitycondition });
    if (disabilitycondition === 'Yes') {
      (<any>$('#add-disability')).modal('show');
    }
  }


  closeDisabilityModal() {
    (<any>$('#add-disability')).modal('hide');
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  setDisabiltyFields() {
    if (this.disabilityTypes && this.disabilityTypes.length) {
      const personDisabilities = this.personDisabilities;
      this.disabilityTypes.forEach(element => {
        element.conditionType = null;
        if (personDisabilities && personDisabilities.length) {
          const isDisabilityAvailable = personDisabilities.find(child => child.disabilitytypekey === element.ref_key);
          if (isDisabilityAvailable) {
            element.conditionType = isDisabilityAvailable.disabilityconditiontypekey;
            element.disabled = true;
          } else {
            element.disabled = false;
          }
        } else {
          element.conditionType = null;
          element.disabled = false;
        }
      });
    }
  }

  reset() {
    this.personDisabilityForm.reset();
    this.closeDisabilityModal();
  }


  onDisabiltyChange(isSelected) {
    if (!isSelected) {
      (<any>$('#remove-disability')).modal('show');
    }
  }

  closeRemoveDisabilityModal() {
    (<any>$('#remove-disability')).modal('hide');
  }

  removeAllDisability() {
  if (this.personDisabilities && this.personDisabilities.length) {
    this.personDisabilities.forEach( (disability, index) => {
        this._service.deleteDisability( disability.persondisabilityid).subscribe(response => {
          if (index === this.personDisabilities.length - 1 ) {
            this.loadPersonDisabilityList();
          }
      });
     });
   }
   (<any>$('#remove-disability')).modal('hide');
  }

  resetDisability() {
    this.hasDisability = true;
    this.closeRemoveDisabilityModal();
  }
}
