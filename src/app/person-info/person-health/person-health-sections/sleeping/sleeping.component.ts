import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { CommonHttpService, AlertService, DataStoreService } from '../../../../@core/services';
import { PersonHealthService } from '../../person-health.service';
import { CaseWorkerUrlConfig } from '../../../../pages/case-worker/case-worker-url.config';
import { DropdownModel } from '../../../../@core/entities/common.entities';
import { Observable } from 'rxjs';
export class Sleeping {
  infoprovidedby: string;
  clientlist: string;
  collaterallist: string;
  infoprovidedname: string;
  relationship: string;
  sleepinginfounknown: boolean;
  sleepingenvironment: string;
  sleepingproblems: string;
  sleepingposition: string;
  naptime: string;
  bedtime: string;
  sleepcomments: string;
}
@Component({
  selector: 'sleeping',
  templateUrl: './sleeping.component.html',
  styleUrls: ['./sleeping.component.scss']
})

export class SleepingComponent implements OnInit {
  SleepingInfoForm : FormGroup;
  SleepEnvironment$: Observable<DropdownModel[]>;
  SleepPositiont$: Observable<DropdownModel[]>;
  SleepProblem$:Observable<DropdownModel[]>;
  reportMode: string;
  editMode: boolean;
  modalInt: number;
  sleepcw: any;

  constructor( private formbuilder: FormBuilder,
    private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService,
    private _healthService: PersonHealthService) { }

  ngOnInit() {
    this.reportMode = 'add';
    this.SleepingInfoForm = this.formbuilder.group({
      infoprovidedby: '',
      clientlist: '',
      collaterallist: '',
      infoprovidedname: '',
      relationship: '',
      sleepinginfounknown: false,
      sleepingenvironment: '',
      sleepingproblems: '',
      sleepingposition: '',
      naptime: '',
      bedtime: '',
      sleepcomments: ''
    });
    this.loadDropDowns();
  }

  private loadDropDowns() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '200', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '201', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '152', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      )])
      .map((result) => {
        return {
          SleepEnvironmentList: result[0].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.value_tx
              })
          ),
          SleepPositionList: result[1].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.value_tx
              })
          ),
          SleepProblemList: result[2].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.value_tx
              })
          )
            };
          })
          .share();
      this.SleepEnvironment$ = source.pluck('SleepEnvironmentList');
      this.SleepPositiont$ = source.pluck('SleepPositionList');
      this.SleepProblem$ = source.pluck('SleepProblemList');
  }

  private resetForm() {
    this.SleepingInfoForm.reset();
    this.editMode = false;
    this.reportMode = 'add';
    this.modalInt = -1;
    this.SleepingInfoForm.enable();
  }

  private view(modal) {
    this.reportMode = 'edit';
    this.patchForm(modal);
    this.editMode = false;
    this.SleepingInfoForm.disable();
  }

  private edit(modal, i) {
    this.reportMode = 'edit';
    this.editMode = true;
    this.modalInt = i;
    this.patchForm(modal);
    this.SleepingInfoForm.enable();
  }

  private delete(index) {
     this.sleepcw.splice(index, 1);
    this._alertSevice.success('Deleted Successfully');
    this.resetForm();
  }

  private cancel() {
    this.resetForm();
  }

  private patchForm(modal: Sleeping) {
    this.SleepingInfoForm.patchValue(modal);
  }

  private add() {
    const sleepinginfo = this.SleepingInfoForm.getRawValue();
    this.sleepcw.push(sleepinginfo);
    this._healthService.saveHealth({ 'sleepinginfo': this.sleepcw }).subscribe(response => {
      this._alertSevice.success('Added Successfully');
    });
    this.resetForm();
  }

  private update() {
    if (this.modalInt !== -1) {
      this.sleepcw[this.modalInt] = this.SleepingInfoForm.getRawValue();
    }
    this.resetForm();
    this._alertSevice.success('Updated Successfully');
  }

}
