import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { AlertService, DataStoreService, CommonHttpService } from '../../../../@core/services';
import { PersonHealthService } from '../../person-health.service';
import { CaseWorkerUrlConfig } from '../../../../pages/case-worker/case-worker-url.config';
import { DropdownModel } from '../../../../@core/entities/common.entities';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'feeding-info-cw',
  templateUrl: './feeding-info-cw.component.html',
  styleUrls: ['./feeding-info-cw.component.scss']
})
export class FeedingInfoCwComponent implements OnInit {

  FeedingInfoForm: FormGroup;
  FeedingPositionList$:  Observable<DropdownModel[]>;
  DietTypeList$: Observable<DropdownModel[]>;
  EaterTypeList$: Observable<DropdownModel[]>;
  LiquidList$: Observable<DropdownModel[]>;
  SolidFood$: Observable<DropdownModel[]>;
  OtherNeedsList$:  Observable<DropdownModel[]>;
  constructor(private formbuilder: FormBuilder,private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService,
    private _healthService: PersonHealthService) { }

  ngOnInit() {
    this.FeedingInfoForm = this.formbuilder.group({
      infoprovidedby: '',
      clientlist: '',
      Collaterallist: '',
      infoprovidedname: '',
      relationship: '',
      feedinginfounknown: '',
      diettype: '',
      eatertype: '',
      liquids: '',
      tpeofformula: '',
      amtperfeeding: '',
      schedule: '',
      solidfood: '',
      feedingposition: '',
      otherneeds: '',
      comments: ''
    });
    this.loadDropDowns();
  }

  private loadDropDowns() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '81', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '229', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '219', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '114', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '202', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '130', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      )])
      .map((result) => {
        return {
          FeedingPositionList: result[0].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.value_tx
              })
          ),
          DietTypeList: result[1].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.value_tx
              })
          ),
          EaterTypeList: result[2].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.value_tx
              })
          ),
          LiquidList: result[3].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.value_tx
              })
          ),
          SolidFood: result[4].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.value_tx
              })
          ),
          OtherNeedsList: result[5].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.value_tx
              })
          ),
            };
          })
            .share();
     this.FeedingPositionList$ = source.pluck('FeedingPositionList');
     this.DietTypeList$ = source.pluck('DietTypeList');
     this.EaterTypeList$ = source.pluck ('EaterTypeList');
     this.LiquidList$ = source.pluck ('LiquidList');
     this.SolidFood$ = source.pluck('SolidFood');
     this.OtherNeedsList$ =source.pluck ('OtherNeedsList');
  }
}
