import { Component, OnInit, Input } from '@angular/core';
import { DataStoreService } from '../../../../@core/services/data-store.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService, CommonHttpService, ValidationService } from '../../../../@core/services';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs';
import { DropdownModel } from '../../../../@core/entities/common.entities';
import { forkJoin } from 'rxjs/observable/forkJoin';
// tslint:disable-next-line:import-blacklist
import { Subject } from 'rxjs';
import { PersonSexual, Health } from '../../../../pages/case-worker/dsds-action/involved-persons/_entities/involvedperson.data.model';
import { CaseWorkerUrlConfig } from '../../../../pages/case-worker/case-worker-url.config';
//import { InvolvedPersonsConstants } from '../../../../pages/case-worker/dsds-action/involved-persons/_entities/involvedPersons.constants';
import { PersonHealthService } from '../../person-health.service';
import { HealthConstants } from '../health-constants';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'sexual-information-cw',
  templateUrl: './sexual-information-cw.component.html',
  styleUrls: ['./sexual-information-cw.component.scss']
})
export class SexualInformationCwComponent implements OnInit {
  sexualInfoForm: FormGroup;
  editMode: boolean;
  reportMode: string;
  modalInt: number;
  sexualcw: PersonSexual[] = [];
  health: Health = {};
  //constants = InvolvedPersonsConstants.Intake.PersonsInvolved.Health;
  sexuallyTransmittedDiseases$: Observable<DropdownModel[]>;
  birthControlMethods$: Observable<DropdownModel[]>;

  constructor(
    private _formBuilder: FormBuilder,
    private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService,
    private _healthService: PersonHealthService) { }

  ngOnInit() {
    this.reportMode = 'add';
    this.loadDropDowns();
    this.initForm();
  }

  initForm() {
    this.sexualInfoForm = this._formBuilder.group({
      numberOfLivingChildren: '',
      numberOfPregnancies: '',
      stdkey: '',
      birthControlMethod: '',
      stdspecify: '',
      bcspecify: '',
      specify: '',
      comments: '',
      sexuallyActive: [false]
    });
  }

  private resetForm() {
    this.sexualInfoForm.reset();
    this.editMode = false;
    this.reportMode = 'add';
    this.modalInt = -1;
    this.sexualInfoForm.enable();
  }

  private view(modal) {
    this.reportMode = 'edit';
    this.patchForm(modal);
    this.editMode = false;
    this.sexualInfoForm.disable();
  }

  private edit(modal, i) {
    this.reportMode = 'edit';
    this.editMode = true;
    this.modalInt = i;
    this.patchForm(modal);
    this.sexualInfoForm.enable();
  }

  private delete(index) {
    this.sexualcw.splice(index, 1);
    this._alertSevice.success('Deleted Successfully');
    this.resetForm();
  }

  private cancel() {
    this.resetForm();
  }

  private patchForm(modal: PersonSexual) {
    this.sexualInfoForm.patchValue(modal);
  }

  private add() {
    let sexualinfo= this.sexualInfoForm.getRawValue();
    this.sexualcw.push(sexualinfo);
    this._healthService.saveHealth({ 'sexualinfo': this.sexualcw }).subscribe(response => {
      this._alertSevice.success('Added Successfully');
    });
    this.resetForm();
  }

  private update() {
    if (this.modalInt !== -1) {
      this.sexualcw[this.modalInt] = this.sexualInfoForm.getRawValue();
    }
    this.resetForm();
    this._alertSevice.success('Updated Successfully');
  }

  private loadDropDowns() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '344', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '23', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      )
    ]).map((result) => {
      return {
        sexuallyTransmittedDiseasesValues: result[0].map(
          (res) =>
            new DropdownModel({
              text: res.description_tx,
              value: res.value_tx
            })
        ),
        birthControlMethodValues: result[1].map(
          (res) =>
            new DropdownModel({
              text: res.description_tx,
              value: res.value_tx
            })
        )
      };
    })
      .share();
    this.sexuallyTransmittedDiseases$ = source.pluck('sexuallyTransmittedDiseasesValues');
    this.birthControlMethods$ = source.pluck('birthControlMethodValues');
  }
}
