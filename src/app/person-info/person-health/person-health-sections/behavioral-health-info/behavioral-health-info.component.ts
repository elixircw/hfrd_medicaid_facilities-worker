import { Component, OnInit } from '@angular/core';

// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs';
import { DropdownModel } from '../../../../@core/entities/common.entities';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { NewUrlConfig } from '../../../../pages/newintake/newintake-url.config';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertService, DataStoreService, CommonHttpService, ValidationService, AuthService } from '../../../../@core/services';
import { Health, BehaviouralHealthInfo } from '../../../../pages/newintake/my-newintake/_entities/newintakeModel';
import { MyNewintakeConstants } from '../../../../pages/newintake/my-newintake/my-newintake.constants';
import { FileError, NgxfUploaderService } from 'ngxf-uploader';
import { AppConfig } from '../../../../app.config';
import { AppUser } from '../../../../@core/entities/authDataModel';
import { HttpHeaders } from '@angular/common/http';
import { GLOBAL_MESSAGES } from '../../../../@core/entities/constants';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'behavioral-health-info',
  templateUrl: './behavioral-health-info.component.html',
  styleUrls: ['./behavioral-health-info.component.scss']
})
export class BehavioralHealthInfoComponent implements OnInit {

  behaviouralhealthinfoForm: FormGroup;
  modalInt: number;
  editMode: boolean;
  reportMode: string;
  ethinicityDropdownItems$: Observable<DropdownModel[]>;
  stateDropdownItems$: Observable<DropdownModel[]>;
  countyDropDownItems$: Observable<DropdownModel[]>;
  behaviouralhealthinfo: BehaviouralHealthInfo[] = [];
  health: Health = {};
  constants = MyNewintakeConstants.Intake.PersonsInvolved.Health;
  private token: AppUser;
  uploadedFile: File;

  constructor(private formbulider: FormBuilder,
    private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService,
    private _uploadService: NgxfUploaderService,
    private _authService: AuthService) {
    this.token = this._authService.getCurrentUser();
  }

  ngOnInit() {
    this.loadDropDowns();
    this.editMode = false;
    this.reportMode = 'add';
    this.modalInt = -1;
    this.behaviouralhealthinfoForm = this.formbulider.group({
      clinicianname: '',
      address1: '',
      address2: '',
      city: '',
      state: '',
      county: '',
      zip: '',
      phone: '',
      currentdiagnoses: '',
      reportname: [''],
      reportpath: ''
    });

    this.health = this._dataStoreService.getData(this.constants.Health);

    if (this.health && this.health.behaviouralhealthinfo) {
      this.behaviouralhealthinfo = this.health.behaviouralhealthinfo;
    }

    // this.behaviouralhealthinfoForm.valueChanges.subscribe( physician => {
    //   this._dataStoreService.setData('behavioural_health_info', this.behaviouralhealthinfoForm.getRawValue());
    // });
  }


  uploadFile(file: File | FileError): void {
    if (!(file instanceof File)) {
      // this.alertError(file);
      return;
    }

    this.uploadedFile = file;
    this.behaviouralhealthinfoForm.patchValue({ reportname: file.name });
  }

  private loadDropDowns() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          where: { activeflag: 1 },
          method: 'get',
          nolimit: true
        },
        NewUrlConfig.EndPoint.Intake.EthnicGroupTypeUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true
        },
        NewUrlConfig.EndPoint.Intake.StateListUrl + '?filter'
      )
    ])
      .map((result) => {
        return {
          ethinicities: result[0].map(
            (res) =>
              new DropdownModel({
                text: res.typedescription,
                value: res.ethnicgrouptypekey
              })
          ),
          states: result[1].map(
            (res) =>
              new DropdownModel({
                text: res.statename,
                value: res.stateabbr
              })
          )
        };
      })
      .share();
    // this.countyDropDownItems$ = source.pluck('counties');
    this.ethinicityDropdownItems$ = source.pluck('ethinicities');
    this.stateDropdownItems$ = source.pluck('states');
  }

  loadCounty() {
    const state = this.behaviouralhealthinfoForm.get('state').value;
    const source = this._commonHttpService.getArrayList(
      {
        where: { state: state },
        order: 'countyname asc',
        method: 'get',
        nolimit: true
      },
      NewUrlConfig.EndPoint.Intake.CountryListUrl + '?filter'
    ).map((result) => {
      return {
        counties: result.map(
          (res) =>
            new DropdownModel({
              text: res.countyname,
              value: res.countyname
            })
        )
      };
    }).share();

    this.countyDropDownItems$ = source.pluck('counties');
  }

  private add() {
    this.behaviouralhealthinfo.push(this.behaviouralhealthinfoForm.getRawValue());
    this.health = this._dataStoreService.getData(this.constants.Health);
    this.health.behaviouralhealthinfo = this.behaviouralhealthinfo;
    this._dataStoreService.setData(this.constants.Health, this.health);
    this._alertSevice.success('Added Successfully');
    this.resetForm();

  }
  private oldadd() {
    this._uploadService
      .upload({
        url: AppConfig.baseUrl + '/' + NewUrlConfig.EndPoint.DSDSAction.Attachment.UploadAttachmentUrl + '?access_token=' + this.token.id,
        headers: new HttpHeaders().set('access_token', this.token.id).set('ctype', 'file'),
        filesKey: ['file'],
        files: this.uploadedFile,
        process: true
      })
      .subscribe(
        (response) => {
          if (response.status) {
            // this.progress.percentage = response.percent;
          }
          if (response.status === 1 && response.data) {
            this.behaviouralhealthinfo.push(this.behaviouralhealthinfoForm.getRawValue());
            this.behaviouralhealthinfo[this.behaviouralhealthinfo.length - 1].reportname = response.data.originalfilename;
            this.behaviouralhealthinfo[this.behaviouralhealthinfo.length - 1].reportpath = response.data.s3bucketpathname;
            this.health = this._dataStoreService.getData(this.constants.Health);
            this.health.behaviouralhealthinfo = this.behaviouralhealthinfo;
            this._dataStoreService.setData(this.constants.Health, this.health);
            this._alertSevice.success('Added Successfully');
            this.resetForm();
            this._alertSevice.success('File Uploaded Succesfully!');
          }
        },
        (err) => {
          console.log(err);
          this._alertSevice.error(GLOBAL_MESSAGES.ERROR_MESSAGE);
        },
        () => {
          // console.log('complete');
        }
      );
  }

  private resetForm() {
    this.behaviouralhealthinfoForm.reset();
    this.modalInt = -1;
    this.editMode = false;
    this.reportMode = 'add';
    this.behaviouralhealthinfoForm.enable();
  }

  private update() {
    if (this.modalInt !== -1) {
      this.behaviouralhealthinfo[this.modalInt] = this.behaviouralhealthinfoForm.getRawValue();
    }
    this.resetForm();
    this._alertSevice.success('Updated Successfully');
  }

  private view(modal) {
    this.reportMode = 'edit';
    this.patchForm(modal);
    this.editMode = false;
    this.behaviouralhealthinfoForm.disable();
  }

  private edit(modal, i) {
    this.reportMode = 'edit';
    this.editMode = true;
    this.modalInt = i;
    this.patchForm(modal);
    this.behaviouralhealthinfoForm.enable();
  }

  private delete(index) {
    this.behaviouralhealthinfo.splice(index, 1);
    this._alertSevice.success('Deleted Successfully');
    this.resetForm();
  }

  private cancel() {
    this.resetForm();
  }

  private patchForm(modal: BehaviouralHealthInfo) {
    this.behaviouralhealthinfoForm.patchValue(modal);
  }
}
