import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BehavioralHealthInfoComponent } from './behavioral-health-info.component';

describe('BehavioralHealthInfoComponent', () => {
  let component: BehavioralHealthInfoComponent;
  let fixture: ComponentFixture<BehavioralHealthInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BehavioralHealthInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BehavioralHealthInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
