import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CommonHttpService, AlertService, DataStoreService } from '../../../../@core/services';
import { PersonChronic, Health } from '../../../../pages/case-worker/dsds-action/involved-persons/_entities/involvedperson.data.model';
import { InvolvedPersonsConstants } from '../../../../pages/case-worker/dsds-action/involved-persons/_entities/involvedPersons.constants';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { CaseWorkerUrlConfig } from '../../../../pages/case-worker/case-worker-url.config';
import { DropdownModel } from '../../../../@core/entities/common.entities';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs';
import { PersonHealthSectionLogComponent } from '../../../../pages/provider-referral/new-referral/intake-persons-involved/person-health/person-health-section-log/person-health-section-log.component';
import { PersonHealthService } from '../../person-health.service';
import { HealthConstants } from '../health-constants';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'chronic-cw',
  templateUrl: './chronic-cw.component.html',
  styleUrls: ['./chronic-cw.component.scss']
})
export class ChronicCwComponent implements OnInit {
  chronicInfoForm: FormGroup;
  editMode: boolean;
  reportMode: string;
  modalInt: number;
  chroniccw: PersonChronic[] = [];
  health: Health = {};
  constants = InvolvedPersonsConstants.Intake.PersonsInvolved.Health;
  physicalTypeDropdownItems$: Observable<DropdownModel[]>;
  mentalTypeDropdownItems$: Observable<DropdownModel[]>;

  constructor(
    private _formBuilder: FormBuilder,
    private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService,
    private _healthService: PersonHealthService) {}

  ngOnInit() {
    this.reportMode = 'add';
    this.loadDropDowns();
    this.initForm();
    const list = this._healthService.getHealthInfoWithKey(HealthConstants.LIST_KEY.PERSON_CHRONIC);
    if (list && Array.isArray(list) && list.length) {
      this.chroniccw = list;
    }
  }

  initForm() {
    this.chronicInfoForm = this._formBuilder.group({
      noknownchronic: '',
      highriskmentaldisease: '',
      physicalkey: '',
      physicalcomments: '',
      mentalkey: '',
      mentalcomments: '',
      comments: ''
    });
  }

  private resetForm() {
    this.chronicInfoForm.reset();
    this.editMode = false;
    this.reportMode = 'add';
    this.modalInt = -1;
    this.chronicInfoForm.enable();
  }

  private view(modal) {
    this.reportMode = 'edit';
    this.patchForm(modal);
    this.editMode = false;
    this.chronicInfoForm.disable();
  }

  private edit(modal, i) {
    this.reportMode = 'edit';
    this.editMode = true;
    this.modalInt = i;
    this.patchForm(modal);
    this.chronicInfoForm.enable();
  }

  private delete(index) {
    this.chroniccw.splice(index, 1);
    this._alertSevice.success('Deleted Successfully');
    this.resetForm();
  }

  private cancel() {
    this.resetForm();
  }

  private patchForm(modal: PersonChronic) {
    this.chronicInfoForm.patchValue(modal);
  }

  add() {
    const chronicData = this.chronicInfoForm.getRawValue();
    this.chroniccw.push(chronicData);
    this.health = this._dataStoreService.getData(this.constants.Health);
    this.health.personChronic = this.chroniccw;
    this._dataStoreService.setData(this.constants.Health, this.health);
    this._healthService.saveHealth({ 'personChronic': [chronicData] }).subscribe(response => {
      this._alertSevice.success('Added Successfully');
    });

    this.resetForm();
  }

  private update() {
    if (this.modalInt !== -1) {
      this.chroniccw[this.modalInt] = this.chronicInfoForm.getRawValue();
    }
    this.resetForm();
    this._alertSevice.success('Updated Successfully');
  }

  private loadDropDowns() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '141', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '120', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      )
    ]).map((result) => {
      return {
        physicalTypeList: result[0].map(
          (res) =>
            new DropdownModel({
              text: res.description_tx,
              value: res.value_tx
            })
        ),
        mentalTypeList: result[1].map(
          (res) =>
            new DropdownModel({
              text: res.description_tx,
              value: res.value_tx
            })
        )
      };
    })
      .share();
    this.physicalTypeDropdownItems$ = source.pluck('physicalTypeList');
    this.mentalTypeDropdownItems$ = source.pluck('mentalTypeList');
  }
}
