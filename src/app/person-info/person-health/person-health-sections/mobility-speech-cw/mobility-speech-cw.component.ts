import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { AlertService, DataStoreService, CommonHttpService } from '../../../../@core/services';
import { PersonHealthService } from '../../person-health.service';
import { CaseWorkerUrlConfig } from '../../../../pages/case-worker/case-worker-url.config';
import { DropdownModel } from '../../../../@core/entities/common.entities';
import { Observable } from 'rxjs/Observable';

export class MobilitySpeech
{
  infoprovidedby: string;
  clientlist: string;
  Collaterallist: string;
  infoprovidedname: string;
  relationship: string;
  mobspeechinfounknown: boolean;
  speechvalue: string;
  mobilityvalue: string;
  childsatup: string;
  childwalked: string;
  childtalked: string;
  comments: string;
}
@Component({
  selector: 'mobility-speech-cw',
  templateUrl: './mobility-speech-cw.component.html',
  styleUrls: ['./mobility-speech-cw.component.scss']
})
export class MobilitySpeechCwComponent implements OnInit {

  mobilitySpeechInfoForm: FormGroup;
  SpeechList$: Observable<DropdownModel[]>;
  MobilityList$: Observable<DropdownModel[]>;
  editMode: boolean;
  reportMode: string;
  modalInt: number;
  speechcw: any;

  constructor(private formbuilder: FormBuilder, private _alertSevice: AlertService,
    private _dataStoreService: DataStoreService,
    private _commonHttpService: CommonHttpService,
    private _healthService: PersonHealthService) { }

  ngOnInit() {
    this.mobilitySpeechInfoForm = this.formbuilder.group({
      infoprovidedby: '',
      clientlist: '',
      Collaterallist: '',
      infoprovidedname: '',
      relationship: '',
      mobspeechinfounknown: false,
      speechvalue: '',
      mobilityvalue: '',
      childsatup: '',
      childwalked: '',
      childtalked: '',
      comments: ''
    });
    this.loadDropDowns();
  }
  private loadDropDowns() {
    const source = forkJoin([
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '341', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      ),
      this._commonHttpService.getArrayList(
        {
          method: 'get',
          nolimit: true,
          where: { 'active_sw': 'Y', 'picklist_type_id': '340', 'delete_sw': 'N' }
        },
        CaseWorkerUrlConfig.EndPoint.DSDSAction.InvolvedPerson.pickListUrl + '?filter'
      )])
      .map((result) => {
        return {
          SpeechList: result[0].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.value_tx
              })
          ),
          MobilityList: result[1].map(
            (res) =>
              new DropdownModel({
                text: res.description_tx,
                value: res.value_tx
              })
          )
        };
      })
      .share();
    this.SpeechList$ = source.pluck('SpeechList');
    this.MobilityList$ = source.pluck('MobilityList');
  }
  private resetForm() {
    this.mobilitySpeechInfoForm.reset();
    this.editMode = false;
    this.reportMode = 'add';
    this.modalInt = -1;
    this.mobilitySpeechInfoForm.enable();
  }

  private view(modal) {
    this.reportMode = 'edit';
    this.patchForm(modal);
    this.editMode = false;
    this.mobilitySpeechInfoForm.disable();
  }

  private edit(modal, i) {
    this.reportMode = 'edit';
    this.editMode = true;
    this.modalInt = i;
    this.patchForm(modal);
    this.mobilitySpeechInfoForm.enable();
  }

  private delete(index) {
     this.speechcw.splice(index, 1);
    this._alertSevice.success('Deleted Successfully');
    this.resetForm();
  }

  private cancel() {
    this.resetForm();
  }

  private patchForm(modal: MobilitySpeech) {
    this.mobilitySpeechInfoForm.patchValue(modal);
  }

  private add() {
    const speechinfo = this.mobilitySpeechInfoForm.getRawValue();
    this.speechcw.push(speechinfo);
    this._healthService.saveHealth({ 'speechinfo': this.speechcw }).subscribe(response => {
      this._alertSevice.success('Added Successfully');
    });
    this.resetForm();
  }

  private update() {
    if (this.modalInt !== -1) {
      this.speechcw[this.modalInt] = this.mobilitySpeechInfoForm.getRawValue();
    }
    this.resetForm();
    this._alertSevice.success('Updated Successfully');
  }

}
