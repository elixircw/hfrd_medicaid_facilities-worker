import { Injectable } from '@angular/core';
import { CommonHttpService, DataStoreService } from '../../@core/services';
import { GUARDIANSHIP } from '../../pages/case-worker/dsds-action/involved-persons/guardianship/_entities/guardianship.const';
import { AppConstants } from '../../@core/common/constants';
import { PaginationRequest } from '../../@core/entities/common.entities';

@Injectable()
export class PersonHealthService {

  constructor(private _commonHttpService: CommonHttpService,
    private _dataStoreService: DataStoreService) {

  }

  saveHealth(data) {
    const personId = this._dataStoreService.getData(AppConstants.GLOBAL_KEY.SELECTED_PERSON_ID);
    const saveObject = { 'health': data, 'pid': personId };
    return this._commonHttpService
      .create(saveObject, 'People/personhealthaddupdate');
  }

  getPersonHealthInfo(personId: string) {

    return this._commonHttpService
      .getPagedArrayList(
        new PaginationRequest({
          method: 'get',
          where: { personid: personId }
        }),
        'People/listpersonhealth?filter'
      );
  }

  setPersonHealthInfo(personId) {
    this.getPersonHealthInfo(personId).subscribe(healthInfo => {
      console.log('setting healthInfo', healthInfo);
      if (healthInfo && Array.isArray(healthInfo) && healthInfo.length) {
        this._dataStoreService.setData(AppConstants.GLOBAL_KEY.PERSON_HEALTH_INFO, healthInfo[0]);
      }
    });
  }

  getHealthInfoWithKey(keyName: string) {
    const healthInfo = this._dataStoreService.getData(AppConstants.GLOBAL_KEY.PERSON_HEALTH_INFO);
    console.log('getting healthInfo', healthInfo);
    if (healthInfo && healthInfo.hasOwnProperty(keyName)) {
      return healthInfo[keyName];
    } else {
      return null;
    }

  }

}
