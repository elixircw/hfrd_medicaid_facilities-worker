import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormMaterialModule } from '../../@core/form-material.module';
import { ChronicCwComponent } from './person-health-sections/chronic-cw/chronic-cw.component';
import { PersonHealthService } from './person-health.service';
import { BehavioralHealthInfoCwComponent } from './person-health-sections/behavioral-health-info-cw/behavioral-health-info-cw.component';
import { BirthInformationCwComponent } from './person-health-sections/birth-information-cw/birth-information-cw.component';
import { FamilyHistoryCwComponent } from './person-health-sections/family-history-cw/family-history-cw.component';
import { AllergiesCwComponent } from './person-health-sections/allergies-cw/allergies-cw.component';
import { EliminationComponent } from './person-health-sections/elimination/elimination.component';
import { SleepingComponent } from './person-health-sections/sleeping/sleeping.component';
import { MobilitySpeechCwComponent } from './person-health-sections/mobility-speech-cw/mobility-speech-cw.component';
import { FeedingInfoCwComponent } from './person-health-sections/feeding-info-cw/feeding-info-cw.component';
import { InsuranceInformationCwComponent } from './person-health-sections/insurance-information-cw/insurance-information-cw.component';
import { NgxfUploaderModule } from 'ngxf-uploader';
import { NgxMaskModule } from 'ngx-mask';
import { ImmunizationCwComponent } from './person-health-sections/immunization-cw/immunization-cw.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {A2Edatetimepicker} from 'ng2-eonasdan-datetimepicker';

@NgModule({
  imports: [
    CommonModule,
    FormMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    NgxfUploaderModule.forRoot(),
    NgxMaskModule.forRoot(),
    A2Edatetimepicker
  ],
  declarations: [AllergiesCwComponent,
    ChronicCwComponent,
    BehavioralHealthInfoCwComponent,
    BirthInformationCwComponent,
    FamilyHistoryCwComponent,
    EliminationComponent,
    SleepingComponent,
    MobilitySpeechCwComponent,
    FeedingInfoCwComponent,
    InsuranceInformationCwComponent,
    ImmunizationCwComponent
  ],
  exports: [AllergiesCwComponent,
    ChronicCwComponent,
    BehavioralHealthInfoCwComponent,
    BirthInformationCwComponent,
    FamilyHistoryCwComponent,
    EliminationComponent,
    SleepingComponent,
    MobilitySpeechCwComponent,
    FeedingInfoCwComponent,
    InsuranceInformationCwComponent,
    ImmunizationCwComponent],
  providers: [PersonHealthService]
})
export class PersonHealthModule { }
