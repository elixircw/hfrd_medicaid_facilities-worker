import {Component, Input} from '@angular/core';

@Component({
    selector: 'ui-modal',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.scss']
})
export class ModalComponent {

	@Input() modalId : string;

	@Input() title : string;
	
	close() {
		let id =  '#' + this.modalId;
		(<any>$(id)).modal('hide');
	}
}