import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
    selector: 'ui-modal-button',
    templateUrl: './modalButton.component.html',
    styleUrls: ['./modalButton.component.scss']
})
export class ModalButtonComponent {

	@Input() modalId : string;

	@Input() label : string;

	@Input() data : any;

  	@Output() didClick = new EventEmitter<any>();
	
	onClick() {
		this.didClick.emit(this.data);
		this.open();
	}

	open() {
		let id =  '#' + this.modalId;
		(<any>$(id)).modal('show');
	}
}