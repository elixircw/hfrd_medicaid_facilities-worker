import {Component, Input} from '@angular/core';

@Component({
    selector: 'program-participation-table',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.scss']
})
export class TableComponent {

	@Input() isLoading : boolean = true;

	@Input() programs : any[] = [];
	
	columns: string[] = [
		'id', 
		'source', 
		'local', 
		'program', 
		'sub-program', 
		'status',
		'start', 
		'end', 
		'worker', 
		'supervisor'
	];
}