import {Component} from '@angular/core';
import {ProgramParticipationService} from './../programParticipation.service';

@Component({
    selector: 'program-participation',
    templateUrl: './programParticipation.component.html',
    styleUrls: ['./programParticipation.component.scss']
})
export class ProgramParticipationComponent {

	person: any;

	constructor(private programParticipationService: ProgramParticipationService) { }
	
	ngOnInit() {
		this.person = {
			mdmId: "MDT-139406431"
		}
	}
}