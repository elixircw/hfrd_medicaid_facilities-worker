
export interface Program {
	irnId: string
	source: string
	program: string
	subProgram: string
	status: string
	start: string
	end: string
	worker: string
	supervisor: string
}