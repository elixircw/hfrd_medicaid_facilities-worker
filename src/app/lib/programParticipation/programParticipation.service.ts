import {Injectable} from '@angular/core';
import {CommonHttpService} from './../../@core/services/common-http.service';
import {Observable} from 'rxjs/Observable';

import {Program} from './program.model';

@Injectable()
export class ProgramParticipationService {

	selectedPerson: any;
	loadedPrograms: Program[] = [];
	loadingPrograms: boolean = false;

    constructor(private _commonHttpService: CommonHttpService) { }

	onSelectPerson($event) {
    	this.selectedPerson = $event;

    	if(this.selectedPerson == undefined) {
    		this.loadedPrograms = undefined;
    		return;
    	}

    	this.loadPrograms(this.selectedPerson.id);
	}

	// load() { this.loadReports(); }
	loadPrograms(id) {
		this.loadingPrograms = true;
		this.getPrograms(id).subscribe( response => {
				this.loadedPrograms = response;
			},
			err => { console.error(err); },
			() => { this.loadingPrograms = false; }
		);
	}

	getPrograms(id) : Observable<Program[]> {
    	return this._commonHttpService.getAll(`external/persons/${id}/programs`);
    }
}