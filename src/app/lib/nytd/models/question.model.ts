
export interface Question {
    id: string;
    description: string;
    value: string;
    choices?: any;
}
