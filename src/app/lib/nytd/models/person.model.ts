
export interface Person {
    firstName: string;
    lastName: string;
    id: string;
    detailsId?: string;
}
