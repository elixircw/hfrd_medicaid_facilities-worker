import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {
    MatButtonModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatSelectModule,
    MatTableModule,
    MatTabsModule
} from '@angular/material';

import { CommonHttpService } from './../../@core/services/common-http.service';
import { DetailsTableComponent } from './components/detailsTable/detailsTable.component';
import { NytdComponent } from './components/nytd.component';
import { NytdRoutingModule } from './nytd-routing.module';
import { NytdService } from './nytd.service';
import { SelectTableComponent } from './components/selectTable/selectTable.component';
import { SurveyModule } from './components/survey/survey.module';
import { UIModule } from '../ui/ui.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatIconModule,
        MatInputModule,
        MatListModule,
        MatProgressSpinnerModule,
        MatRadioModule,
        MatSelectModule,
        MatTableModule,
        MatTabsModule,
        NytdRoutingModule,
        SurveyModule,
        UIModule
    ],
    declarations: [
        DetailsTableComponent,
        NytdComponent,
        SelectTableComponent
    ],
    providers: [
        NytdService,
        CommonHttpService
    ]
})

export class NytdModule { }
