import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';

@Component({
    selector: 'details-table',
    templateUrl: './detailsTable.component.html',
    styleUrls: ['./detailsTable.component.scss']
})
export class DetailsTableComponent {

	@Input() rows: any[];

	columns: string[] = ['id', 'description', 'value'];
}
