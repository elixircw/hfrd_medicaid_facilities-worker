import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {
    MatButtonModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatInputModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatSelectModule,
    MatTableModule,
    MatTabsModule
} from '@angular/material';

import { SurveyComponent } from './survey.component';
import { RadioQuestionComponent } from './question/radioQuestion/radioQuestion.component';
import { SelectQuestionComponent } from './question/selectQuestion/selectQuestion.component';
import { UIModule } from '../../../ui/ui.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatInputModule,
        MatListModule,
        MatProgressSpinnerModule,
        MatRadioModule,
        MatSelectModule,
        MatTableModule,
        MatTabsModule,
        UIModule
    ],
    exports: [
        SurveyComponent
    ],
    declarations: [
        SurveyComponent,
        RadioQuestionComponent,
        SelectQuestionComponent,
    ]
})

export class SurveyModule {
    //
}
