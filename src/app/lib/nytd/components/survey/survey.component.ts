import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';

@Component({
    selector: 'nytd-survey',
    templateUrl: './survey.component.html'
})
export class SurveyComponent implements OnChanges {

    @Input() data: any;

    @Output() didClickSave = new EventEmitter<any>();

    answers: any[] = [];

    prefix = 'question_';

    survey: any;

    onClickSave($event) {
        // Remove prefix
        const answers = Object.keys(this.answers).reduce((acc, key) => {
            acc[key.substring(this.prefix.length)] = this.answers[key];
            return acc;
        }, {});

        this.didClickSave.emit(answers);
    }

    ngOnChanges(changes: SimpleChanges) {
        if (this.data) {
            // morph for usage
            this.survey = this.transformToSurvey(this.data);
            // extract to set form
            this.answers = this.extractAnswers(this.data);
        }
    }

    extractAnswers(data) {
        return data.filter(q => q.id > 33).reduce((acc, question) => {
            acc[this.prefix + question.id] = question.value;
            return acc;
        }, {});
    }

    transformToSurvey(data) {

        const survey = {
            validated: false,
            participation: {},
            questions: []
        };

        if (data) {
            survey.participation = data.find(e => e.id == 34);
            survey.questions = data.filter(e => e.id > 36);
        }

        return survey;
    }

    didChangeParticipation($event) {
        this.updateAnswer({
            id: 34,
            value: $event.value
        });
    }

    didChangeDate($event) {
        this.updateAnswer({
            id: 35,
            value: $event.value
        });
    }

    didChangeStatus($event) {
        this.updateAnswer({
            id: 36,
            value: $event.checked
        });
    }

    updateAnswer($event) {
        const value = $event.value;
        const element = this.prefix + $event.id;

        this.answers[element] = value;
    }
}
