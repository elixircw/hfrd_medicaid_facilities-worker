import {Component, OnChanges, SimpleChanges} from '@angular/core';
import {QuestionComponent} from '../question.component';

@Component({
    selector: 'select-question',
    templateUrl: './selectQuestion.component.html'
})
export class SelectQuestionComponent extends QuestionComponent implements OnChanges {

	ngOnChanges(changes: SimpleChanges) {
		super.ngOnChanges(changes);
	}
}