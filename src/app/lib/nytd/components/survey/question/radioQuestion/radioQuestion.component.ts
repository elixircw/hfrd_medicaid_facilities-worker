import {Component, OnChanges, SimpleChanges} from '@angular/core';
import {QuestionComponent} from '../question.component';

@Component({
    selector: 'radio-question',
    templateUrl: './radioQuestion.component.html',
    styleUrls: ['./radioQuestion.component.scss']
})
export class RadioQuestionComponent extends QuestionComponent implements OnChanges {

	ngOnChanges(changes: SimpleChanges) {
		super.ngOnChanges(changes);
	}
}