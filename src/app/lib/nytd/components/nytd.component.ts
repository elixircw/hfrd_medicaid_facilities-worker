import {Component, OnInit} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {Observable} from 'rxjs/Observable';
import {NytdService} from './../nytd.service';
import {saveAs} from "file-saver";

@Component({
    selector: 'app-nytd',
    templateUrl: './nytd.component.html',
    styleUrls: ['./nytd.component.scss']
})
export class NytdComponent implements OnInit {

    constructor(private service: NytdService) { }

    validated = false;

    saveSuccessful = false;

    reportSelectorColumns: any = {
        name: (row) => row.period
    };

    personSelectorColumns: any = {
        name: (row) => row.firstName + ' ' + row.lastName,
        id: (row) => row.id
    };

    ngOnInit() {
        this.service.load();

        this.service.didSuccessfullySubmit.subscribe(
            response => {
                this.saveSuccessful = true;
            },
            err => { console.error(err); },
            () => {}
        );
    }

    onClickDownload($event) {
        this.service.downloadAsXml().subscribe(
            response => {
                saveAs(response, 'nytd-' + this.service.loadedReport.period + '.xml');
            },
            err => { console.error(err); },
            () => {}
        );
    }

    onClose($event) {
        this.closeAndResetModal();
    }

    closeAndResetModal() {
        /**
         * @todo Dont use hardcoded modal id here.
         * @author jhester@dminc.com
         * This hardcodes the id of the modal to this functionality.
         * This is not ideal, but it works for now. This should be rectified
         * later through an open event flow with the modal component.
         */
        (<any>$('#survey-save-modal')).modal('hide');

        // reset data ...
        setTimeout(() => {
            this.saveSuccessful = false;
        }, 1000);
    }

    get saveButtonText() {
        if (this.service.isSubmitting) {
            return 'Saving Survey...';
        }
        return (this.validated) ? 'Save Validated' : 'Save Unvalidated';
    }

    onClickSave($event) {
        this.service.saveAnswers($event);
    }

    onClickSubmit($event) {
        this.service.onSubmit({ validated: this.validated});
    }
}
