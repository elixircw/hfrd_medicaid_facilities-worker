export enum AppErrorConfig {
    SUCCESS= '10001',
    ERROR= '10002',
    MISSING_DATA= '10003',
    PROCEDURE_ERROR= '10004'
}
