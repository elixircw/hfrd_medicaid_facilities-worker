// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.
export const environment = {
    contextPath:"/api",
    production: false,
    apiHost: 'https://d38mozb1nh16qd.cloudfront.net/api',
	formBuilderHost: 'https://dev.djs.cjams.mdthink.maryland.gov/formbuilder',
    reportHost: 'https://10.88.40.141/',
    reports: 'https://dev2.analytics.mdthink.maryland.gov/qliksense/hub/',
    fakeHttpResponse: false,
    envName: 'dev1',
    formBuilderUserId: 'formsuser@cjams.com',
    formBuilderPassword: 'cjams@123',
    googleMapApi: 'AIzaSyA8WcRN7QOypVMNyky_vPQgsq2DMCz0bzw',
    logoutDHSURL: 'https://dev1.access.mdthink.maryland.gov/openam/UI/Logout?realm=/DJS&goto=https://dev.djs.cjams.mdthink.maryland.gov/',
    IdleTimeOut: 780,
    PopupTimeOut: 60,
    cookieName: 'iPlanetDirectoryPro'
};
